<?php

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/faq', 'FAQController@index')->name('faq.index');

Route::get('/contact', 'ContactController@index')->name('contact.index');

Route::get('/terms', 'TermsOfServiceController@index')->name('terms-of-service.index');

Route::group(['prefix' => 'knowledge'], function() {
    Route::get('/', 'KnowledgeController@index')->name('knowledge.index');
    Route::get('materials', 'KnowledgeController@materials')->name('knowledge.materials');
    Route::get('technologies', 'KnowledgeController@technologies')->name('knowledge.technologies');
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function() {

    Route::group(['prefix' => 'register'], function() {
        Route::get('/', 'RegistrationController@create')->name('registration.create');
        Route::post('/', 'RegistrationController@store')->name('registration.store');
    });

    Route::group(['prefix' => 'login'], function() {
        Route::get('/', 'SessionsController@create')->name('sessions.create');
        Route::post('/', 'SessionsController@store')->name('sessions.store');
    });

    Route::get('logout', 'SessionsController@destroy')->name('sessions.destroy');

    Route::group(['prefix' => 'email/verify'], function() {
        Route::get('/', 'EmailVerificationController@create')->name('email-verification.create');
        Route::post('/', 'EmailVerificationController@store')->name('email-verification.store');
        Route::get('{token}', 'EmailVerificationController@verify')->name('email-verification.verify');
    });

    Route::group(['prefix' => 'password/reset'], function() {
        Route::get('/', 'PasswordResetController@create')->name('password-reset.create');
        Route::post('/', 'PasswordResetController@store')->name('password-reset.store');
        Route::get('{token}', 'PasswordResetController@edit')->name('password-reset.edit');
        Route::post('{token}', 'PasswordResetController@update')->name('password-reset.update');
    });

});

Route::group(['prefix' => 'account', 'namespace' => 'Account', 'middleware' => 'auth'], function() {

    Route::get('/', 'HomeController@index')->name('account.index');

    Route::group(['namespace' => 'Settings', 'prefix' => 'settings'], function() {

        Route::group(['prefix' => 'profile'], function() {
            Route::get('/', 'ProfileController@edit')->name('account.profile.edit');
            Route::post('/', 'ProfileController@update')->name('account.profile.update');
        });

        Route::group(['prefix' => 'shipping'], function() {
            Route::get('/', 'ShippingController@edit')->name('account.shipping.edit');
            Route::post('/', 'ShippingController@update')->name('account.shipping.update');
        });

        Route::group(['prefix' => 'change_password'], function() {
            Route::get('/', 'ChangePasswordController@edit')->name('account.change-password.edit');
            Route::post('/', 'ChangePasswordController@update')->name('account.change-password.update');
        });

    });

    Route::group(['namespace' => 'History', 'prefix' => 'history'], function() {
        Route::get('quotes', 'QuotesController@index')->name('history.quotes');
        Route::get('manual_quotes', 'ManualQuotesController@index')->name('history.manual-quotes');
        Route::get('orders', 'OrdersController@index')->name('history.orders');
    });

});

Route::group(['namespace' => 'Quotes', 'prefix' => 'quotes', 'middleware' => 'auth'], function() {
    Route::get('create', 'NewQuoteController@store')->name('quotes.create');
    Route::get('{quoteId}/upload', 'UploadController@index')->name('quotes.upload.show');
    Route::post('{quoteId}/upload', 'UploadController@upload')->name('quotes.upload');

    Route::get('{quoteId}/parts/streamics/status', 'StatusController@checkStreamicsStatus')->name('quotes.parts.streamics-status');
    Route::get('{quoteId}/parts/spreadsheet/status', 'StatusController@checkSpreadsheetStatus')->name('quotes.parts.spreadsheet-status');

    Route::get('{quoteId}/parts', 'PartsController@fetch')->name('quotes.parts');
    Route::put('{quoteId}/parts', 'PartsController@save')->name('quotes.update-parts');

    Route::delete('{quoteId}/parts/{partId}', 'UploadController@drop')->name('quotes.file.drop');

    Route::post('{quoteId}/review/begin', 'ReviewController@beginReview')->name('quotes.review.begin');
    Route::get('{quoteId}/review', 'ReviewController@show')->name('quotes.review.show');
    Route::get('{quoteId}/details', 'DetailsController@show')->name('quotes.details');

    Route::get('{quoteId}/report/download', 'ReportController@download')->name('quotes.report.download');

    Route::put('{quoteId}/comments', 'CommentsController@save')->name('quotes.comments.save');
    Route::post('{quoteId}/manual', 'ManualController@save')->name('quotes.set-manual');
});

Route::group(['namespace' => 'Orders', 'prefix' => 'orders', 'middleware' => 'auth'], function() {
    Route::get('{id}/details', 'DetailsController@show')->name('orders.details');
});

Route::group(['namespace' => 'Manufacturing', 'prefix' => 'manufacturing', 'middleware' => 'auth'], function() {
    Route::get('processes', 'ProcessesController@fetch')->name('manufacturing.get-processes');
    Route::get('processes/{processId}/materials', 'MaterialsController@fetch')->name('manufacturing.get-materials');
});