const mix = require('laravel-mix');

mix.sass('resources/assets/sass/common.scss', 'public/css');
mix.sass('resources/assets/sass/home.scss', 'public/css');
mix.sass('resources/assets/sass/contact.scss', 'public/css');
mix.sass('resources/assets/sass/faq.scss', 'public/css');
mix.sass('resources/assets/sass/knowledge.scss', 'public/css');
mix.sass('resources/assets/sass/knowledge-materials.scss', 'public/css');
mix.sass('resources/assets/sass/knowledge-technologies.scss', 'public/css');
mix.sass('resources/assets/sass/terms-of-service.scss', 'public/css');
mix.sass('resources/assets/sass/account.scss', 'public/css');
mix.sass('resources/assets/sass/account-profile.scss', 'public/css');
mix.sass('resources/assets/sass/account-shipping.scss', 'public/css');
mix.sass('resources/assets/sass/upload.scss', 'public/css');
mix.sass('resources/assets/sass/quote-details.scss', 'public/css');
mix.sass('resources/assets/sass/order-details.scss', 'public/css');

mix.js('resources/assets/js/common/index.js', 'public/js/common.js');
mix.js('resources/assets/js/registration/index.js', 'public/js/registration.js');

mix.js('resources/assets/js/upload/index.js', 'public/js/upload.js')
mix.js('resources/assets/js/quote-details/index.js', 'public/js/quote-details.js')
mix.js('resources/assets/js/quote-review/index.js', 'public/js/quote-review.js')
mix.js('resources/assets/js/order-details/index.js', 'public/js/order-details.js')

mix.copyDirectory('resources/assets/images', 'public/images');

mix.disableNotifications();