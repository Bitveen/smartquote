import Vue from 'vue';

import App from './App.vue';
import VModal from 'vue-js-modal';
import VTooltip from 'v-tooltip';
import Toasted from 'vue-toasted';

Vue.use(Toasted);
Vue.use(VTooltip);
Vue.use(VModal);

new Vue({
  el: '#app',
  components: {
    App
  }
});