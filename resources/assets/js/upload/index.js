import Vue from 'vue';
import axios from 'axios';
import uuid from 'uuid';
import VModal from 'vue-js-modal';

import SmartQuoteFile from './components/SmartQuoteFile.vue';
import GetQuoteModal from './components/GetQuoteModal.vue';

Vue.use(VModal);

const eventBus = new Vue({});

new Vue({
  el: '#app',
  data: {
    cadFiles: [],
    quoteId: ''
  },
  created() {
    eventBus.$on('filesDropped', (files) => {
      this.handleUploadFiles(files);
    });
    const quoteId = location.pathname.split('/')[2];
    this.quoteId = parseInt(quoteId);
  },
  methods: {
    handleUploadFiles(files) {
      const filesToUpload = [];

      for (let i = 0; i < files.length; i++) {
        filesToUpload.push(files[i]);
      }

      const uploaders = filesToUpload.map((file) => {
        return this.uploadFile(file);
      });

      axios.all(uploaders);
    },
    uploadFiles(event) {
      const files = event.target.files;
      if (!files.length) return;
      this.handleUploadFiles(files);
    },
    uploadFile(file) {
      const formData = new FormData();
      formData.append('cad', file);

      const cadFile = {
        id: uuid(),
        name: file.name,
        loaded: 0,
        total: 0,
        uploadError: false,
        file: file,
        isUploaded: false,
        quoteId: this.quoteId,
        partId: ''
      };

      this.cadFiles.push(cadFile);

      return axios.post(`/quotes/${this.quoteId}/upload`, formData, {
        onUploadProgress: (event) => {
          const { total, loaded } = event;
          cadFile.total = total;
          cadFile.loaded = loaded;
        }
      }).then((response) => {
        cadFile.loaded = cadFile.total;
        cadFile.isUploaded = true;
        if (response.data instanceof Array) {
          cadFile.partId = response.data.map((item) => {
            return item.id;
          });
        } else {
          if (response.data.id) {
            cadFile.partId = response.data.id;
          }
        }
        return cadFile;
      }, (err) => {
        cadFile.isUploaded = false;
        cadFile.uploadError = true;
      });
    },
    deleteCad(cadId, fileId) {
      if (!cadId) {
        this.cadFiles = this.cadFiles.filter((file) => {
          return file.id !== fileId;
        });
        return;
      }
      if (cadId instanceof Array) {
        const tasks = cadId.map((id) => {
          return axios.delete(`/quotes/${this.quoteId}/parts/${id}`);
        });
        axios.all(tasks).then(() => {
          this.cadFiles = this.cadFiles.filter((file) => {
            return file.id !== fileId;
          });
        }, (err) => {
          console.log('Error while deleting multiple files');
        });
      } else {
        axios.delete(`/quotes/${this.quoteId}/parts/${cadId}`).then((response) => {
          if (response.data.status === 'deleted') {
            this.cadFiles = this.cadFiles.filter((file) => {
              return file.partId !== cadId;
            });
          }
        }, (err) => {
          console.log('Error while deleting part');
        });
      }
    },
    getQuote() {
      axios.get(`/quotes/${this.quoteId}/parts/streamics/status`).then((response) => {
        const totalFiles = response.data.total;
        const processedFiles = response.data.processed;
        if (totalFiles) {
          if (totalFiles === processedFiles) {
            return location.assign(`/quotes/${this.quoteId}/details`);
          }
        }
        this.$modal.show('getQuote', { totalFiles, processedFiles });
      });
    }
  },
  computed: {
    filesUploaded() {
      let uploaded = true;

      if (!this.cadFiles.length) return false;

      this.cadFiles.forEach((file) => {
        if (!file.isUploaded) {
          uploaded = false;
          return;
        }
      });

      return uploaded;
    }
  },
  directives: {
    droppable: {
      bind: (el, binding) => {

        if (!window.FileReader) {
          return;
        }

        el.addEventListener('dragover', (event) => {
          event.stopPropagation();
          event.preventDefault();

          if (!el.classList.contains('hover')) {
            el.classList.add('hover');
          }
        }, false);

        el.addEventListener('dragleave', (event) => {
          event.stopPropagation();
          event.preventDefault();

          if (el.classList.contains('hover')) {
            el.classList.remove('hover');
          }
        }, false);

        el.addEventListener('drop', (event) => {
          event.stopPropagation();
          event.preventDefault();

          if (el.classList.contains('hover')) {
            el.classList.remove('hover');
          }

          const files = event.target.files || event.dataTransfer.files;

          eventBus.$emit('filesDropped', files);

        }, false);
      }
    }
  },
  components: {
    cadFile: SmartQuoteFile,
    getQuoteModal: GetQuoteModal
  }
});