$(document).ready(() => {
  $('[data-carrier="fedex"]').css({ display: 'none' });
  $('[data-carrier="ups"]').css({ display: 'none' });

  $('#pickup').on('change', (event) => {
    if (event.target.checked) {
      $('#pickup_form').css({ display: 'block' });
      $('.shipping-information').css({ display: 'none' });
      $('.shipping-carier').css({ display: 'none' });
    } else {
      $('#pickup_form').css({ display: 'none' });
      $('.shipping-information').css({ display: 'block' });
      $('.shipping-carier').css({ display: 'block' });
    }
  });
  $('#same_as_billing').on('change', (event) => {
    if (event.target.checked) {
      $('.shipping-information-inputs').css({ display: 'none' });
    } else {
      $('.shipping-information-inputs').css({ display: 'block' });
    }
  });
  $('#carrier_enable').on('change', (event) => {
    if (event.target.checked) {
      $('#shipping-preference').css({ display: 'none' });
      $('#own-shipping-carier').css({ display: 'block' });
    } else {
      $('#shipping-preference').css({ display: 'block' });
      $('#own-shipping-carier').css({ display: 'none' });
    }
  });
  $('#shipping_account_carrier').on('change', (event) => {
    const selectedOption = event.target.options[event.target.options.selectedIndex];
    $('#delivery_preference option').removeAttr('selected');
    const firstOption = $('#delivery_preference option')[0];
    $(firstOption).attr('selected', 'selected');
    if (!selectedOption.value) {
      $('[data-carrier="fedex"]').css({ display: 'none' });
      $('[data-carrier="ups"]').css({ display: 'none' });
      return;
    }
    if (selectedOption.value === 'fedex') {
      $('[data-carrier="fedex"]').css({ display: 'block' });
      $('[data-carrier="ups"]').css({ display: 'none' });
    } else {
      $('[data-carrier="fedex"]').css({ display: 'none' });
      $('[data-carrier="ups"]').css({ display: 'block' });
    }
  });
});