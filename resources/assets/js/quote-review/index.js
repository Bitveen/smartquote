import axios from 'axios';
import Vue from 'vue';
import VModal from 'vue-js-modal';

import RequestManualQuoteModal from './RequestManualQuoteModal.vue';

Vue.use(VModal);

$(document).ready(() => {
  const $comment = $('#comment');
  const $checkout = $('#checkout');
  const quoteId = $('#quoteId').val();

  $comment.attr('placeholder', 'Please use this message box for comments, questions, or special application ' +
    'requirements. Thank you for your order — we will follow-up with you shortly.'
  );

  $checkout.on('click', (event) => {
    event.preventDefault();
    const message = $comment.val().trim();
    const payload = { message };
    axios.put(`/quotes/${quoteId}/comments`, payload).then((response) => {
      location.assign(`/orders/${quoteId}/details`);
    });
  });
});




new Vue({
  el: '#app',
  components: {
    RequestManualQuoteModal
  }
});