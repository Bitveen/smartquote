const assistlyWidget = $('.assistly-widget');

$(document).scroll(() => {
  const height = $(window).scrollTop();
  if (!assistlyWidget) return;
  if (height > 35) {
    assistlyWidget.css('position', 'fixed');
    assistlyWidget.css('top', '0px');
  } else {
    assistlyWidget.css('position', 'absolute');
    assistlyWidget.css('top', '35px');
  }
});

$(document).ready(() => {
  const widget = $('.a-desk-widget');
  const styleText = widget.attr('style');
  if (styleText) {
    const strVas = styleText.search('email');
    if (strVas != -1) {
      widget.addClass('a-desk-widget-email');
    } else {
      widget.addClass('a-desk-widget-chat');
    }
  }


  /* Init liveagent */
  if (!window._laq) {
    window._laq = [];
  }
  window._laq.push(() => {
    liveagent.showWhenOnline('57380000000PBiu', document.getElementById('liveagent_button_online_57380000000PBiu'));
    liveagent.showWhenOffline('57380000000PBiu', document.getElementById('liveagent_button_offline_57380000000PBiu'));
  });

  liveagent.init('https://d.la4-c1-chi.salesforceliveagent.com/chat', '57280000000PBhr', '00D80000000LahY');

  $('#liveagent_button_online_57380000000PBiu').click((event) => {
    event.preventDefault();
    if (liveagent) {
      liveagent.startChat('57380000000PBiu');
    }
  });

});


