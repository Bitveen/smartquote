import { RegistrationValidator } from './validator';

$(document).ready(() => {
  $('#register-form').on('submit', (event) => {
    const validator = new RegistrationValidator();
    const isFormValid = validator.validateAll();
    if (!isFormValid) {
      return event.preventDefault();
    }
  });
});