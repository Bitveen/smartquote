export class RegistrationValidator {

  validateAll() {
    const validators = [];

    validators.push(this.validateFirstName());
    validators.push(this.validateLastName());
    validators.push(this.validateEmailAddress());
    validators.push(this.validatePassword());
    validators.push(this.validateCompany());
    validators.push(this.validatePhoneNumber());
    validators.push(this.validateZipCode());
    validators.push(this.validateTermsAgreement());

    let valid = true;
    validators.forEach((v) => {
      if (!v) {
        valid = false;
        return;
      }
    });

    return valid;
  }

  validateFirstName() {
    const $firstName = $('#first_name');
    const firstName = $firstName.val();
    let valid = true;
    if (!firstName.trim()) {
      valid = false;
      $firstName.parent().find('.help-inline').css('display', 'block');
      $firstName.parent().parent().addClass('error');
    } else {
      valid = true;
      $firstName.parent().find('.help-inline').css('display', 'none');
      $firstName.parent().parent().removeClass('error');
    }
    return valid;
  }

  validateLastName() {
    const $lastName = $('#last_name');
    const lastName = $lastName.val();
    let valid = true;
    if (!lastName.trim()) {
      valid = false;
      $lastName.parent().find('.help-inline').css('display', 'block');
      $lastName.parent().parent().addClass('error');
    } else {
      valid = true;
      $lastName.parent().find('.help-inline').css('display', 'none');
      $lastName.parent().parent().removeClass('error');
    }
    return valid;
  }

  validateEmailAddress() {
    const $email = $('#email');
    const $emailConfirmation = $('#email_confirmation');
    const email = $email.val();
    const emailConfirmation = $emailConfirmation.val();
    let valid = true;
    if (!validator.isEmail(email)) {
      valid = false;
      $email.parent().find('.help-inline').css('display', 'block');
      $email.parent().parent().addClass('error');
    } else {
      valid = true;
      $email.parent().find('.help-inline').css('display', 'none');
      $email.parent().parent().removeClass('error');
    }

    if (email && email !== emailConfirmation) {
      valid = false;
      $emailConfirmation.parent().find('.help-inline').css('display', 'block');
      $emailConfirmation.parent().parent().addClass('error');
    } else {
      valid = true;
      $emailConfirmation.parent().find('.help-inline').css('display', 'none');
      $emailConfirmation.parent().parent().removeClass('error');
    }

    return valid;
  }

  validatePassword() {
    const $password = $('#password');
    const $passwordConfirmation = $('#password_confirmation');
    const password = $password.val();
    const passwordConfirmation = $passwordConfirmation.val();
    let valid = true;
    if (!password.trim()) {
      valid = false;
      $password.parent().find('.help-inline').css('display', 'block');
      $password.parent().parent().addClass('error');
    } else {
      valid = true;
      $password.parent().find('.help-inline').css('display', 'none');
      $password.parent().parent().removeClass('error');
    }

    if (password && password !== passwordConfirmation) {
      valid = false;
      $passwordConfirmation.parent().find('.help-inline').css('display', 'block');
      $passwordConfirmation.parent().parent().addClass('error');
    } else {
      valid = true;
      $passwordConfirmation.parent().find('.help-inline').css('display', 'none');
      $passwordConfirmation.parent().parent().removeClass('error');
    }

    return valid;
  }

  validateCompany() {
    const $company = $('#company');
    const company = $company.val();
    let valid = true;
    if (!company.trim()) {
      valid = false;
      $company.parent().find('.help-inline').css('display', 'block');
      $company.parent().parent().addClass('error');
    } else {
      valid = true;
      $company.parent().find('.help-inline').css('display', 'none');
      $company.parent().parent().removeClass('error');
    }
    return valid;
  }

  validatePhoneNumber() {
    const $phoneNumber = $('#phone_number');
    const phoneNumber = $phoneNumber.val();
    let valid = true;
    if (!validator.isMobilePhone(phoneNumber.trim(), 'en-US')) {
      valid = false;
      $phoneNumber.parent().find('.help-inline').css('display', 'block');
      $phoneNumber.parent().parent().addClass('error');
    } else {
      valid = true;
      $phoneNumber.parent().find('.help-inline').css('display', 'none');
      $phoneNumber.parent().parent().removeClass('error');
    }
    return valid;
  }

  validateZipCode() {
    const $zipCode = $('#zip_code');
    const zipCode = $zipCode.val();
    let valid = true;
    if (zipCode && !validator.isPostalCode(zipCode.trim(), 'US')) {
      valid = false;
      $zipCode.parent().find('.help-inline').css('display', 'block');
      $zipCode.parent().parent().addClass('error');
    } else {
      valid = true;
      $zipCode.parent().find('.help-inline').css('display', 'none');
      $zipCode.parent().parent().removeClass('error');
    }
    return valid;
  }

  validateTermsAgreement() {
    const $isTermsAgree = $('#is_terms_agree');
    const isTermsAgree = $isTermsAgree.is(':checked');
    let valid = true;
    if (!isTermsAgree) {
      valid = false;
      $isTermsAgree.parent().parent().find('.help-inline').css('display', 'block');
      $isTermsAgree.parent().parent().parent().addClass('error');
    } else {
      valid = true;
      $isTermsAgree.parent().parent().find('.help-inline').css('display', 'none');
      $isTermsAgree.parent().parent().parent().removeClass('error');
    }

    return valid;
  }

}