<style>
    b {
        text-transform:uppercase;
    }
    table {
        width:100%;
        font-size:9pt;
        font-family:Arial;
    }
    p {
        font-family:Arial;
        font-size:6pt;
        margin-top:0;
        margin-bottom:1px;
    }
    .items td, .fees td {
        padding-top:7px;
        padding-bottom:0px;
    }
    .report-type {
        text-align: right;
        font-size:18pt;
        font-weight:bold;
    }
    .order-total td {
        padding-top:20px;
        font-weight:bold;
        text-align:right;
        font-size:10pt;
        padding-bottom:20px;
    }
    .customer-right {
        text-align: right;
    }
    .item-heading th {
        font-size:9pt;
        padding-top: 7px;
        padding-bottom: 7px;
        border-top:1px solid #000;
        border-left:1px solid #000;
        border-bottom:1px solid #000;
    }
    .item-heading th.last {
        border-right:1px solid #000;
    }
    .part-number {
        width: 32%;
        text-align: center;
    }
    .part-process {
        width: 15%;
        text-align: center;
    }
    .part-material {
        width: 17%;
        text-align: center;
    }
    .part-price {
        width: 12%;
        text-align: center;
    }
    .part-quantity {
        width: 8%;
        text-align: center;
    }
    .part-total {
        width: 20%;
        text-align: center;
    }
    .report-info td {
        border: 1px solid #000;
        padding: 5px 5px;
    }
    .report-info td.report-info__dest {
        border-right: none;
    }
    .report-info td.order-number {
        text-align: right;
        border-left: none;
    }
    .spacer {
        height: 10px;
    }
    .heading td {
        padding-bottom: 15px !important;
    }
    .customer-info td {
        padding-top: 15px;
        padding-bottom: 15px;
    }
</style>
<table cellspacing="0" cellpadding="2">
    <tr class="heading">
        <td colspan="3">
            <img style="height:80px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAABkCAYAAABkW8nwAAAACXBIWXMAAAsTAAALEwEAmpwYAAABZGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iPgogICAgICAgICA8eG1wOkNyZWF0b3JUb29sPkFkb2JlIEltYWdlUmVhZHk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Chvleg4AABVFSURBVHic7Z13nFXVtce/axjKAIoDWFDQEEs0ipoodp8+gl2jxGhif1hiSQyWZ4khaCyPiIo+e4sx0U+UPMuL5WH32RBBTOwIKhZEVEQ6DAyz8sfal9n3zDnnnnPLnDt4f5/P+cDss8s6e6+799prrb22qCo1tIWI9AVOATYAWrxXdcBs4Algqqp+kwF5VQ+pMVY4ROQq4KyYLE3ADGAK8CLwCvCBqi5oB/KqHjXGioCI/A04LEWRJcCHwGTgJWAS8KGqLq4AeVWPGmNFQETuAX5eQhULMUabiDHaq8AMVV1WBvKqHjXGioCI/BU4ooxVzgM+AO4EblLVlWWsu+pQnzUB1QYR6Qd0ArqXueq1gO2ATYC/A5+Wuf6qQo2xPIjIWcAIbOfXOyJbEzATKHaqfxb4qsiyHQY1xnIQEQH2BzYskPUd4CBgeZFNzV3dl0GoMdYqqKqKyO8xHVVnbEbqCgwFenpZm4EvVXVF+1PZcVAT3mMgIj0xPdVmXvKrwO7flt1dsajLmoAqh2RNQEdFbSn0ICJrALtgS2ALsCb5y2ANCVFjLAcnvF8OnFogawu2M6whBjXGcnDCe98EWTcCrhORtDs7wZjycVUdn5rADoaa8O5BRDYADgUaMHPOthVoZhawnarOrkDdVYPajOVBVT8DrgUQkUFUhrHeAFZ7w3SNsaLRqQx1KPAJ8DbmVjMReEVVF5ah7qpGjbHKi5WYj9abGCO9DExb3Ze9MHwrGEtEOgMDMXPKnDJWnXP2ewOYgPlgTS9zGx0Sqz1jicgA4DZge2CeiFwL3KKqpaoMXgJ+AXxec09ui9Va8+5mqrHAPkAfYGPgv4H7RaRUwbwXsEaNqcKxWjMWcBSmPgjiAOApETlHRHoUWfdWro69i6ZuNcZqy1giMhC4hGh7Xx9gDPCQiGxaZDM9geFFls2DiPRyNK8WqHrGEpGBInKMiBTc/ovhQBEZhclV/RM0MQS4UkSKlTdLMlSLyGYich4m/N8tIqGeqyKyj4jsUUpb7QpVrdoHm1VexrbxP0yQfzjmL6Upn/eBtQJ13ZOg3EJgvyK+qzuwL/AXYI6raxEwDGcNCeRfE9OFfQ3slPW4JPrGrAkoMABjvEEckSD/E0UwlQIPAp0CdY0rUOY94Ecpv+e7wDmYemKpV9dc4KCYcjsAy1zel4AeWY9Noadql0KnJjjaS0qyrM0soqlvgIu1rbvwogLlXlDVp5M0ICKbi8gfMc37COA57MziZ9gsdIyqPhxTRW/MlQfMrWdoknazRNUyFnaipZ/3dxL72pQi2hmtqv8ISR8D3EpbBpsL3ARcVahiEekiIr8C/h84HlgbuBo4GzgOMxsdrqqPFqiqifzDGz8q1HbmyHrKdFN9P+AK4GDckgScTOtS0ULMUuHyD8BkpTRL4HigW0ydPbElzy8zKeE3bQM8HNLmRxijfQ3smbCuDYHPvTrGee92BK7E3KUzH8tVdGVNgOucQcAKx0B/w4T2w7yOfBVzZemGadD3w5STufJ1wB0pmUqBPwN9YujqC0wLlJlcgBm7YzPSnJh25wB7xNRRB+wO7JmjD7jeK3+ry3MRNpMrcHrW41iNjDUAkzdyHfcQthR+49J3BQ7BdoiLgQXAFl75QyluN6jY8rlvORgLGAw8VaC9t4DBBfqjHjNirwCmA+djts5nXR1HuDS/3h9nPY7VyFidgMcCHXUd8CXwK2AUNpv5M1hPV7YuwWAWepZjfljrF8NYmHlnJHaMPqqNZdiss17CPrk2UP5/gb3cj+1692/u3efAhlmPY9UxluvIQzF9Va6zmjE90f0BplLgJK9cnfdLLvV5B/hJGsbClqwXC9T7TyJmxZj+2BaYH6jnCUxGWxFIH5v1+FUzY9Vhuy2/w1aGMNVfQ2aMnzomTMI8twCPxLxvBm7HBOYetBXeJ7s2G4HRtMo4Yc9ibHfZu8g++XXgx5ajz/97IrBO1uNXtYzlOrIBuAyLbRAcpPnYVn2NiLJD3GwSx1QTHLN0Bs4EvojJOw3bmU4PpE/CjuK/UqCtSaRUoEZ810nAxyH1LwXuAzbKetyqnrG8zhwIPIodd/9PN8CDEpTrBVyMCffBgVgI7BLIv02B2StshlhCqxY87FngaFizjP2xPuapcQamef8Y08bXZT1WHYqxXGfejfmH+2l9gPoEZXcHXggM+G8i8tZjZwlnhjBJ2ud5YNcy90OfwN9/wOJsdc96jOKezDXvItJVRDYVkV6BV43YLIOINIrIGGypG1SoTlV9ATPyngLcgP3aL4/I26yqN2Ha7PuK/Iyvgd9iBumXiqyjDdzJ7IdE5EER2dwlL8CW815evi4isomINJar7ZKRNWdjjPIFMBVbQtZ16S9gxuFG4ElaZ4V9KkhLHWZqmU7bTUPYs9LRtn2F6NmA1pn0I+CHwC8xVUN/bLY9HtuVzgdOzno8V9GeOQGwHnZEKjdYrwFbYLqpccCN3rsmYMd2oGlPbBYqxFg3UUE5h1Z3mVx7k4HzMP3eIMxy4NOzd9bjWTWM5TrwtkAHTXSdOJN8NcK7QGMF6RDgSDd7JpmxZmLmm4rJO8BdgTY/wE5TB+2QbwK9sh7LamOsLWi79Q8b2N9WkIZNMB1ZMUL708AOFaLr32m7Cw32zUrM9SbzsawqxnIdeBj5ZorgM74Sv0jMnHQCFmy2lB3hfOB3lFHN4NH4+5h2W3C+/VmPYVUyluvA3TD91Ve0ejvk4ikUpb0u0N73MZNRGgYqpMd6iQq4sGBC+9tev8zHlLDHZj1uVc9YrgMFE+hvwNxLtvLeDSelzS2mneOJ17yvBB4g3+tCMY37AZhXRFTZxZi+KdIlJyGNm2HCeoP7uyemK5uMuTl3zXq8OgxjeZ16NOZ1sJX7+xg3aC8CXUqotx7TZM+KYYz3gMMxVce7gXc5W2EvzFa4KKaeKZSgHnHMqdjOuA7ogrnd/Dnr8enIjLWjmzUOwTxMc/qcr4CNi6xzEOa9eQf5urHcswL4IzDA5e9DYe+Gf8NskFHM1YTZONdNSWsnNzvlZs8D3Q9iAXBO1uNT6Mlc8x6D9zF9zWBs9tjApXfDjNWJ4Q6DboIdQtgDW1I/xfyYcngH8z8/QVVzt0YUPDOoqs8De2MC9ryQLF0wG99TInJQCrIbsNsswGar44GtgTUw5q5qVA1jicgFInK/iNwhIidhS9YEbNAO8LIuJ0UMUBHZC5ud/onNgh+4V3sDf8IUstcDQ1X1wWJoV9VFqnoRZkZ6LiLbVsCDInKziETdeuFjGc6k5bA1ZhX4EJghIgeLyA2uz24RkbWLob1iyHrK9Kb+m8lfQqZiXqXLyZdjJhLjc+7VV4/5hDd5ZRcA97o6VwI7ESMAU5zPewNwLuGuP7nnmoR9crVXZhnmKvMP4JlAfVWlHFWtIhkL+A5mDyu03f8Pl/904ICY+jYk3CzzADYDHkjgkGo5GMsrOwizdUYJ9aEbEGBTzCetB7Aldtwsrj+aKHCC6VvNWK5ThxC9W8sJwfWY2+5SzMbYP6Kubo6J/DqWAMenoKdoxnLlO2OOekGXnOsJP0rf2aP5ZJd2FNGzXxMJToh/6xnLdeTW2BGw3PKXc8293csz2uvc82Lq2pxW78tJwJCUtJTEWF4938PsoVMcU/WNyLcbtkwrpmjt6tKPdWk5U06Te39g1uPVYRjL6+TvY7ds7YVpxz/DnUQhX8Z4ihgPA7ecDKMI43W5GMurL1ahifl05dr5Aosp3wkzZ72Pnac8GNuEFEVDez1VGypSVd/BVACIyCeYxvtyETkOkz9yWBtb9pZE1PM2ZgrJHFo4POV63v+70apm2Bc4SjvQxQNVo26Ig6q+hx3Q/DkW99MPHtsErBCRfk5X1aEgIn29wG/+jWLzsJnpCszrYlx701YKOgRjAajqzZhp4xLyL6ucpHZ34EnARKcP65wFjWkhIsMwE9WNLukV73UdZtKZCpymHezyzKpdCmHVfYH7YUJtTgtdjykbweSuW9z/GzETzGVAXxE5W53gUioZgb/L8mMUkcMxBW13YLGLWPgYJj8OoTVs0xvAVSLSgilzH1HVYsI1tS+yFvJiBNk9sI6M0+Fc7+X3fZZWAAeXgYaGEBqeoYD+K0G9G5Hv//Usrfca/cxLD3N2/AQ7JFISDZV+qnkp3An4QcQ7xQT7E0VkjIh0o/W4F9isdkqSuKVxUNWlWDjvBS5pDnacvdRl6UjyA8lNUFV1G5PrsB3gXMJtlQMwm2dVrzaZc3bMr1owVcM92O0PczGj9HOYS00jtuy1YDbFfTBzT+6X/SmBIB8l0LItFuFlyzLV5/urf4mpEHJhmB7GmGdnTM0y0337LMzmOZwOECqyQ1wr54y2a2JL3Beq2uy92x8TctfHBmkL9+oLYGdVndHO5BaEiDxFa1S+mZgSuDvwX8CNqrrcy7uOe7cMu+S8pZ3JLQrVPZ06qOpc7Fcbhscw887lmItNDp9SXEzS9sA7tDJWf+wH8RssUt9yP6OqftnOtJUF1SxjFYSIHIEtg49gcRh8DALuEJGd2p2wCIjIABG5HIuO46M3ZuqZIiIj3W64Q6NDLIVBiEgX4FIsYEiYgPsoFpztBOAZVT2uHcmLhIiciAXFvRdb2n4dkfVJzItjVnvRVnZkLeQVKfxuQnhonyXAncDaLl9vKnC6pwS6G4B+7v+dsOUvzHNhGTAsa3pLeTrqjCXYzmk3zFjdgDHaBFV9NUva0sKZc3bHPCDqMA/XCcBb2kEE9TB0SMaqofrRoYX3GqoXNcaqoSKoMVYNFUGNsWqoCGqMVUNFUGOsGiqCGmPVUBHUGKuGiqDGWDVUBDXGqqEiyJSxRKRBRC4UkT5Z0lFD+VEvIvuRfwC0WNQB76rqmynK/AKLCFOPBYYNhYishZ1cKecPoQ54XVXfE5Eerv6u3nsBZqvdcpEIIrIxdoGnbzwWzDj+WUj+3pgBejssKMpaWDytJAbcFizM0SzgdeB5jfCWFZEdsSNzwXoFeFNVpyZoL1jnYOzOo+C3LsbcfkLdT4p9xqRwIRmEeU6qIyYyICx2qCJ4R185nnNd/QM9Wvzn6ZRuMadFtPOTkLxH0zYMZSnPbOwMQENIW+Niyr1GStciLCJOMDZr7rkEkDo3YOVCIjcPd6rmSux4PJhP95Uh9+nkoNgtXOVGjt6o+tO2GfX9eenuNM6fsKAl5cK6wAXANSGnk+JOFf0AuwQhEVzduTMGQTwHjFZVLbeMVTC0osNpWEQ9HzsA55Sh7qqGOxzxOyp33uAE7NKBNBghIrsmzHs0Fhc2iLnAWaq6BOI/binG6UkHVLAQPPGZRLbFoqqE4UwReVJVg+EWhcoMREnnDovELsDGMe+bsLgNYX2vLq0nFos0DJ2AH2NReJKiBzBGRPZR1UVRmURkI+xgcNiENFpVX8v9ETVYy7AoJ1NJzlh15AeLDSOsAVsCo2Jw5pbEvVX1Gy99Bhbgtp62AmgzFlPrwhBa78VibYV9Zycs7GI5kUTo3jIivRkLF3AXdnStmXDGqsOE/D2xS9jDdtRbikhdSg/UXYARmJzWBs5r9xLsFHcQ47GDtnl4n3Df8YI3mqZ9sNicSQTR0Snr3Znw4+iRQdkC5b9DeCTBx7GBTPIIdvQ97HsO8dq6OiLP/aS8tgS7rSKsrkl4sbhIfkfQPGC7iLYOJ3wDNRvvkofcEzVjKfECX2q47ekFCbOPcEviMwnzd41ILzXqzGAsrkIStNAaMjwOUcvvY5reT/wZTOUQtiwWI5P2wpbEA9XCC1hFIutjs1UYv1ykqm8FE6MYqzNwhojMTkFgJ+DxEPkIpye6Cu9WUIdm7Nf0U2wZzKEBGCsiQ1V1DtmhEbsgoD3wTeEsbbAUC6kZJW/F4SPshNDgQPoQ7CrjsbBqCRyFXb8SxIPYhQt5EJH6OMY6qQhilxIe5/xMTBEYxAOYknQx9jE+tgFGYsH3awjHCopXFy3AduHjMFWFj/NF5Ak3E+2PxYsIYiYmauS1LyLrAqeWW93QRu/jtL5haoRZwO/UwideSmtgfx+nOMvAtwFpl0Ew2a7YnW1XTBa7NOTd2sClIrIhFk+iS+B9C3Chqk4PKTsKGFpuxsr7SHdUfCwW0COIS1V1GoDaid+RtJXrumJr/jplpjMpVmIbmaRPQXVLDIqRB7sSLV8mLX8rFv8iiAOAv2M77iDGAX8JJorIIdgKtCRqKWzBzA2LSC5jdaZtEI6zsW1sEP9H27X5f7Cg/kcF0rfCFIqnJ6SjnJiM7bySQDFZMekGJYjvFVFmHdrKrZBCRaSqy0XkXMxe6V+bkounH8QM4AL1Iv4AiEh/YAyeSihK3bCd26Qk3W7X4W2XMYZaGFL3V0SoMrA7+D4JKdNE/C0UexKubhhZorphfMrt/4khdQTVDZdF5JlGxFY/oq31iL7E80WgvoC6YSqejRCLI1FIHdEM/CyEljpsBlulponTZrcAaBHHvJ3NbyymIQ7iDxrhAaGqH4rIhcDt5Gt3uwBXiMgUVZ2dlp4SkFZUSGIdeD8ifVPgCRF5HdvMxM069S7/dyPefxycURLgZtpeiBXEncB9IenHYlEK8wiMQim2uXOwUNJBPEtrhOAo3IVdHDAskL4FMEpEflmEvqea8DK2IwuTO3uT3s4XhifTFvCWxO1pu0sEuxx0lAbCZIrIZtgGIE++Lrujn4jsjqkXgpgPnO8r3sLgfmkXYBrdIE7EmK4jYxohgm8ZMQV4qJiCapc2XEzbHeoK7PLNvLBKnomujWI4JxuFpaeesUSkEVsCu4e8Hquqk5LUo+Z4Fmaz6ozZEgcEmyac3jTfENUPaRDV3qp0J1qMBO4moZtRCrwKDFeLgOgjzbfdhsVB9XGLqgbTwOyGQ8LqrscE7AW0cqlgRuhUa7TT0J6KLVnz/UawX9E1aerDtsFDMcL9AdgAOFdEzvCm5WbXptD6HXUkvzCzBeuDhkD5xSlpXo71Z9CrMk+JqKrzRWQ4tm0fhvlE9cG2/12wZaXQct/k6p2HzYLjgfs033ifw5IAXUL+mPu0rRCR8zCNfD8szvwlETS0YO4yuYu0cnUvFiyIWVABpsAMVV1GQjjG2hi7A8bv2DosIO1XSevy6uyNOZQFB2olMD3HWCLSHdvZ+UjcrrvJYiA2I/odtEhVP05BbyPG+EF6P1HVheGlVi0pjbTqpZLMlE0YI8+Lq9vV3x/ziPAZqwkb41CbsIichjn0HaqqoTKbiHTF+s3/IQiwqBYfq4ZQiMiawJFqV82kxr8A/w99EQjAPCMAAAAASUVORK5CYII=" />
        </td>
        <td class="report-type" colspan="3" style="text-align:right;">SmartQuote</td>
    </tr>
    <tr class="report-info">
        <td colspan="3" class="report-info__dest">
            <strong>REMIT TO: Keemera, Inc. | DBA FATHOM</strong>
            <br>3000F Danville Blvd. #141<br>Alamo, CA 94507
            <br><strong>Payment Terms: Credit Card Required</strong>
        </td>
        <td colspan="3" class="order-number">
            <b>Quote #{{ $quote->id }}</b>
        </td>
    </tr>
    <tr class="customer-info">
        <td colspan="6" class="customer-left" valign="top">
            <b>Customer:</b><br>
            {{ $user->first_name }} {{ $user->last_name }}<br>
            {{ $user->email }}
        </td>
    </tr>
    <tr class="item-heading">
        <th class="part-number" align="center">PART NUMBER/DESCRIPTION</th>
        <th class="part-process" align="center">PROCESS</th>
        <th class="part-material">MATERIAL</th>
        <th class="part-price" align="center">PRICE</th>
        <th class="part-quantity" align="center">QTY</th>
        <th class="part-total last" align="center">TOTAL COST</th>
    </tr>
    @foreach($quote->parts as $part)
        <tr class="items">
            <td class="part-number">{{ $part->name }}</td>
            <td class="part-process">{{ $part->process->title }}</td>
            <td class="part-material">{{ $part->material->name }}</td>
            <td class="part-price">${{ $part->price / $part->quantity }}</td>
            <td class="part-quantity">{{ $part->quantity }}</td>
            <td class="part-total">${{ $part->price }}</td>
        </tr>
    @endforeach

    @if($priceAdjustment != '$0.00')
        <tr class="items">
            <td class="part-number">Price Adjustment</td>
            <td class="part-process">Minimum Order</td>
            <td class="part-material"></td>
            <td class="part-price">{{ $priceAdjustment }}</td>
            <td class="part-quantity">1</td>
            <td class="part-total">{{ $priceAdjustment }}</td>
        </tr>
    @endif

    @foreach($quote->fees as $fee)
        <tr class="fees">
            <td class="part-number">{{ $fee->report_type }}</td>
            <td class="part-process">Fees/Discounts</td>
            <td class="part-material">{{ $fee->report_name }}</td>
            <td class="part-price">
                @if($fee->type == 'discount' || $fee->type == 'account')
                    -
                @endif
                ${{ $fee->price }}
            </td>
            <td class="part-quantity">1</td>
            <td class="part-total">
                @if($fee->type == 'discount' || $fee->type == 'account')
                    -
                @endif
                ${{ $fee->price }}
            </td>
        </tr>
    @endforeach
    <tr class="order-total">
        <td colspan="5" >
            <b>Order Total:</b>
        </td>
        <td colspan="1" style="text-align:center;">
            <b>{{ $total }}</b>
        </td>
    </tr>
</table>

<p><b>TERMS AND CONDITIONS (Kemeera, Inc. (dba FATHOM, Inc.)</b></p>
<p><b>PRICING IS CONTINGENT UPON RECEIPT OF P.O. FOR ALL ITEMS AS LISTED ABOVE</b></p>
<p>Prices quoted do not include any applicable sales tax.</p>
<p>Prices quoted do not include any applicable shipping charges.
<p>Payment Terms: Credit Card or Purchase Order on approval—to avoid additional penalties, payment is due upon receipt of the invoice.</p>
<p>A cut-off time of 4:00 PM PST is strictly enforced for all orders specified as Next Day—this ensures that the next day counts as the first business day.</p>
<p>Prices quoted do not include finishing or post processing unless otherwise specified.</p>
<p>Kemeera is not responsible for checking parts to fit in assemblies before shipment.</p>
<p>Upon request, Kemeera will measure up to four dimensions as part of quality control at no additional charge.</p>
<p>Prototypes are not intended for production use.</p>
<p>Kemeera is not responsible for accuracy of CAD data provided by customers. Parts are printed directly from customer files.</p>
<p>Kemeera does not recommend 3D printing a wall thickness less than 1mm—chance of breakage is likely during cleaning process and no guarantee of form to specifications.</p>

<p>Standard tolerances per process as follows:<br>
    Objet +/- 0.2mm ** accuracy applies to rigid materials only**<br>
    FDM +/- 0.2mm or +/- 0.0015 mm/mm, whichever is greater<br>
    SLA +/- 0.2mm or +/- 0.002 mm/mm, whichever is greater<br>
    SLS +/- 0.2mm or +/- 0.003 mm/mm, whichever is greater
</p>
<p>Kemeera is not responsible for fixing/preparing CAD data for printing unless otherwise specified in quotation.</p>
<p>Unless otherwise specified, the finish on all 3D printed parts (matte or glossy) will be chosen by the technician based on what optimizes quality and structural integrity.</p>
<p>Kemeera is not responsible for late shipments due to unforeseen situations with shipping company.</p>
<p>Due to the customization of services performed, the customer cannot cancel this order after Kemeera has started its services.  If work has not begun, a 15% cancellation fee will be charged on the total purchase order amount.</p>
<p>Nonconforming Goods:</p>
<p>Buyer shall inspect all goods upon tender and delivery by Kemeera, and should any of the goods be nonconforming goods, Buyer must notify Kemeera, in writing, within ten (10) days for prototype parts or thirty (30) days for tooled parts of Kemeera’s tender and delivery of the goods describing the nature of any nonconformity.</p>
<p>Kemeera shall have the right and option to repair or replace any nonconforming goods.</p>
<p>The failure of Buyer to notify Kemeera in writing that the goods are nonconforming within 10 days (prototype parts) or 30 days (tooled parts) of Kemeera’s tender and delivery of the goods, shall constitute acceptance of the goods and Buyer shall be liable to Kemeera for the total order price.</p>
<hr style="color:#000;height:1px;">
<p>FATHOM specializes in advanced manufacturing technologies and methods that enhance and accelerate a company's product development process&mdash;from advanced prototype fabrication to bridge-to-production services: 3D printing (FDM, PolyJet, SLS, DMLS, SLA), CNC machining, urethane casting, rapid tooling, 3D printed tools, injection molding, part assembly, finishing, and contract R&amp;D services.</p>
