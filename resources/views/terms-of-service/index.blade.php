@extends('layouts.master')

@section('pageTitle', 'Terms of Service')

@section('content')
    <div class="container content">

        <div class="row">
            <div class="span12 hgray" style="margin-top: 20px; margin-bottom: 0px;">
                <div class="bluehead" style="padding-right: 20px;">TERMS OF SERVICE</div>
            </div>
        </div>

        <div style="margin-top: 20px;">

            <p style="text-align:center;line-height:28pt; text-align: left;">
                <span style="font-family:'Varela Round',sans-serif;font-size:20px;">
                    KEMEERA (DBA FATHOM) CUSTOMER AGREEMENT
                </span>
            </p>

            <p>
                <span style="font-family:Arial;">
                    This Agreement (“<strong>Agreement</strong>”)
                    sets forth the terms and conditions on which
                    FATHOM provides customers digital manufacturing
                    services and rapid prototyping services
                    and issues quotations for such
                    services (“<strong>Services</strong>”).
                    A user (referred to as a “<strong>Customer</strong>”)
                    may obtain a pricing quotation for Services by way of this website
                    (the “<strong>Website</strong>”), and Customers agree and acknowledge
                    that use of this Website is and shall be governed by the terms
                    and conditions of this Agreement The Services a
                    Customer may obtain through this Website are provided by Kemeera,
                    Inc. doing business as “FATHOM” (referred to hereafter as “<strong>FATHOM</strong>”),
                    a California corporation, whose principal place of
                    business is located at 315 Jefferson St, Oakland, CA 94607.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px;line-height:22px;font-weight:bold">
                    1.  DIGITAL MANUFACTURING, RAPID PROTOTYPING AND DESIGN SERVICES.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.1  <strong>FATHOM QUOTATIONS</strong></span><span style="font-family:Arial;">.
                    From time to time the Customer may request price quotations (“FATHOM Quotes”)
                    from FATHOM for the production ofend use parts or a rapid prototype
                    parts (collectively referred to as “<strong>Part</strong>” or “<strong>Parts</strong>”)
                    and/or provision of other manufacturing services or
                    rapid prototyping services (collectively referred
                    to as “<strong>Services</strong>”) by completing the FATHOM Quote
                    form on this Website and initiating an Order (as defined below in Section 1.2).
                    The FATHOM Quote requires the Customer to submit all of its desired specifications,
                    which may include any critical tolerance or requirements (“<strong>Specifications</strong>”)
                    for the Part to FATHOM.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.2  <strong>ORDERS</strong></span><span style="font-family:Arial;">.
                    Once the Customer has obtained a FATHOM Quote,
                    Customer may accept the quote from FATHOM and may submit an online order
                    (an “<strong>Order</strong>”) requesting FATHOM to produce the Part(s) quoted.
                    The total fees for each Order shall include the applicable amount
                    specified in the FATHOM Quote for the Part(s), plus all
                    applicable taxes and all shipping charges (collectively, the “Fees”).
                    To submit an Order, the Customer must either pay the Fees in
                    advance online by providing FATHOM with valid credit card account information,
                    or by issuing a non-cancellable purchase order to FATHOM by e-mail.
                    Once an electronic Order confirmation has been delivered to the Customer,
                    the Order is binding on both parties, and may not be cancelled
                    or changed except by written agreement of the parties.
                    The Order confirmation shall specify the anticipated Services duration,
                    and the date range on which the Part shall be completed.
                    FATHOM reserves the right to accept or reject any Order or Order change
                    for any reason, at its sole discretion. All Services and the Order are
                    expressly governed by the terms and conditions of this Agreement,
                    and the parties acknowledge and agree that no other terms and conditions of any
                    Customer purchase order or other agreements, forms, contracts, or
                    terms and conditions not specifically agreed to in writing by
                    FATHOM shall be binding upon FATHOM under any circumstances,
                    which is an express condition that Customer agrees to in exchange for
                    FATHOM’s performance of the Services in connection with a confirmed Order.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.3  <strong>CANCELLATION/CHANGE ORDERS</strong>
                </span>
                <span style="font-family:Arial">.
                    If FATHOM does agree to a cancellation or change request,
                    Customer may receive the following refunds for cancelled/changed
                    Orders based on the following:
                </span>
            </p>

            <ul style="list-style-type:disc;">
                <li>
                    <span style="font-family:Arial">
                        75% credit on Parts and taxes, 100% credit on
                        shipping for Orders cancelled/changed after Order
                        placement and before the Service begin.
                    </span>
                </li>
                <li>
                    <span style="font-family:Arial">
                        25% credit on Parts and taxes, 100% credit on shipping
                        for Orders cancelled/changed after Services begin
                        but prior to Service completion.
                    </span>
                </li>
                <li>
                    <span style="font-family:Arial">
                        0% credit on Parts and taxes, 100% credit on shipping for
                        Orders cancelled/changed after completion of
                        Service and prior to shipping.
                    </span>
                </li>
                <li>
                    <span style="font-family:Arial">
                        0% credit on Parts, taxes and shipping
                        for Orders cancelled/changed after the
                        completion of Services and after shipping.
                    </span>
                </li>
            </ul>

            <p>
                <span style="font-family:Arial">
                    Orders can only be cancelled/changed by FATHOM personnel.
                    Please contact us by telephone at: (877) 345-4948,
                    or via email at
                </span>
                <a href="mailto:services@studiofathom.com" target="_blank">
                    <span style="font-family:Arial;text-decoration:underline">
                        services@studiofathom.com
                    </span>
                </a>
                <span> if you wish to request changes to your Order.</span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.4  <strong>REPEAT/SIMILAR ORDERS. </strong>
                </span>
                <span style="font-family:Arial">
                    FATHOM processes each Order as a new Order.
                    If the Customer requires an Order to be processed in an
                    identical manner as a previous Order, Customer must request that
                    FATHOM reference in its FATHOM Quote the previous Order number on the new Order.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.5  <strong>DELIVERY TERMS.  </strong>
                </span>
                <span style="font-family:Arial">
                    FATHOM will use commercially reasonable efforts to provide
                    all Parts to the Customer in accordance with the Order and
                    this Agreement, and within the timeframe specified by
                    FATHOM at the time of its Order confirmation.
                    After completion of the Services and once the Part
                    is ready for shipping, FATHOM shall use commercially
                    reasonable efforts to deliver the Part(s) within three (3) to five (5)
                    business days based the delivery method Customer selected in the
                    FATHOM Quote. Shorter lead-times may be available for smaller projects,
                    and overnight delivery may be available for an additional charge.
                    Lead times will be longer for Orders involving large quantities
                    and/or physically large Parts.  If the parties have agreed to partial
                    Parts shipments in the Order, the Quote and Order confirmation may
                    specify additional charges. Please call (877) 345-4948 or email
                </span>
                <a href="mailto:services@studiofathom.com" target="_blank">
                    <span style="font-family:Arial;text-decoration:underline">
                        services@studiofathom.com
                    </span>
                </a>
                <span style="font-family:Arial"> for more information.</span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    1.6  <strong>SHIPPING AND INSURANCE:  </strong>
                </span>
                <span style="font-family:Arial">
                    Parts Shipments are F.O.B. shipping point, and risk of
                    loss passes at the time FATHOM provides the Parts to the
                    designated carrier.  Any damage to Parts during transportation is
                    the responsibility of the carrier and Customer. Customer is responsible
                    for securing appropriate insurance and for submitting any damage
                    claims to the carrier for damages.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;font-weight:bold">
                    2. OWNERSHIP OF INTELLECTUAL PROPERTY.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    2.1  <strong>CUSTOMER OWNERSHIP</strong>
                </span>
                <span style="font-family:Arial">.
                    The entire right, title and interest, including all copyright,
                    patent, trade secret, mask work and trademark rights
                    (“<strong>Customer’s Intellectual Property</strong>”), in and the
                    Part and any Specifications related thereto is and shall be
                    owned solely by the Customer, or its licensors.
                </span>
            </p>


            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    2.2 <strong>FATHOM RIGHTS</strong>
                </span>
                <span style="font-family:Arial">
                    Notwithstanding Section 2.1, FATHOM retains all right, title and
                    interest, in and to all FATHOM Intellectual Property, which includes
                    but is not limited to, all copyright, patent, trade secret,
                    mask work and trademark rights, in and to the FATHOM Website and in
                    the underlying software, technology, methodologies and know-how used by
                    FATHOM in performing its Services and producing Parts, including any
                    new Intellectual Property derived from or developed in course
                    of the performance of Services.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;font-weight:bold">
                    3. LIMITED CUSTOMER WARRANTIES; FATHOM WARRANTY DISCLAIMER.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    3.1 <strong>CUSTOMER WARRANTIES</strong>
                </span>
                <span style="font-family:Arial">
                    . Customer hereby warrants that (a) it has the right to
                    provide the Specifications to FATHOM; (b) the use of the
                    Specifications and such other materials provided by the
                    Customer to produce a Part will not violate or infringe
                    any intellectual property or other legal rights of any third party;
                    and (c) any software or Part files delivered by the Customer to
                    FATHOM will be free of any viruses, time bombs and other
                    harmful programming routines.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    3.2 <strong>FATHOM WARRANTY DISCLAIMER</strong>
                </span>
                <span style="font-family:Arial;">
                    . FATHOM makes no representations, warranties, guarantees
                    or conditions as to materials, strength, tolerances, or
                    other part characteristics; and all parts are delivered
                    and accepted in "as is" condition.  To the maximum extent of applicable law,
                    FATHOM expressly disclaims any and all obligations, duties and
                    liabilities, and customer is and shall be exclusively liable,
                    for any use by a third party of the part manufactured and produced
                    by fathom pursuant to a quote and this agreement.  In addition,
                    FATHOM disclaims all other statutory, express, and implied representations,
                    warranties, guarantees, and conditions of any kind, with respect to the
                    parts or the services provided hereunder, including without limitation
                    any implied warranty of merchantability, fitness for a particular purpose,
                    title, non-infringement, quality of service, or otherwise arising out of a
                    course of performance, dealing, or trade usage.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;font-weight:bold">
                    4. LIMITATION OF LIABILITY.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    4.1 <strong>EXCLUSION OF CONSEQUENTIAL DAMAGES</strong>
                </span>
                <span style="font-family:Arial">
                    . In connection with any services rendered or part produced by
                    FATHOM pursuant to an order placed under this agreement,
                    FATHOM hereby disclaims and excludes any and all liability for punitive,
                    exemplary, incidental, indirect and/or consequential damages,
                    including without limitation late delivery, lost profits,
                    lost opportunity costs or costs of cover, arising in connection
                    with this agreement and its subject matter, whether such damages
                    arise by contract, tort (including negligence), or otherwise.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    4.2 <strong>DIRECT DAMAGE LIMITATION</strong>
                </span>
                <span style="font-family:Arial">
                    . FATHOM’s aggregate and cumulative liability for any damages
                    of any kind incurred in connection with or which arise out
                    of this agreement and its subject matter, whether
                    arising by contract, tort (including negligence) or otherwise,
                    shall not exceed the total service fees paid by customer
                    for the applicable part alleged to have caused such damage.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    4.3 <strong>BASIS OF THE AGREEMENT</strong>
                </span>
                <span style="font-family:Arial">
                    . Customer further agrees and acknowledges that these disclaimers,
                    exclusions and limitations of liability are a material and
                    essential condition of this agreement,
                    and that the website and services would not be made available,
                    or would be made available on materially different terms,
                    in the absence of these conditions, which customer
                    does expressly consent to under the advice of a counsel of its choosing.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    5. <strong>INDEMNITY</strong>
                </span>
                <span style="font-family:Arial">
                    . Customer agrees to defend, indemnify and hold harmless FATHOM,
                    its agents and employees against all claims, losses, liabilities
                    and damages, and to pay all claims, judgments, awards,
                    costs and expenses (including attorney and expert witness fees)
                    arising out of or in connection with Supplier’s acts or omissions under this
                    Agreement, and (i) any claim that any Specifications,
                    Parts or any other materials provided to FATHOM in connection
                    with an Order violate or infringe the Intellectual
                    Property or other legal rights of any third party; (ii) any
                    violation of laws by Customer or any of its affiliates,
                    agents, subcontractor or any third party under Customer’s
                    control or management or (ii) for or in connection with any use of a
                    Part by Customer or its employees, agents or contractors or by any third party
                    including a consumer who may use the Part
                    (including any products liability claims such third party might raise).
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    6. <strong>WEBSITE ACCESS AND USAGE OBLIGATIONS</strong>
                </span>
                <span style="font-family:Arial">
                    . Customer shall use a valid e-mail address and a secure password to
                    log on to the FATHOM Website. Customer must not disclose its
                    password to third parties. Customer is responsible for all
                    access to and use of the FATHOM Website using their e-mail address and password.
                    If an employee, agent or any person uses Customer’s e-mail
                    address and password to access the FATHOM Website,
                    Customer will be deemed to have authorized any access,
                    use or misuse of the FATHOM Website (including without limitation all orders placed)
                    by such employees or agents or any other person using the
                    Customers e-mail address and password. Customer will not
                    use any automatic device or manual process to monitor or
                    copy FATHOM’s web pages or the content contained herein
                    without the express written permission of FATHOM. Customer will not
                    interfere or attempt to interfere with the proper working of the
                    FATHOM Website or any activities conducted on the FATHOM Website.
                    Customer will not take any action that imposes an unreasonable or
                    disproportionately large load on FATHOM’s infrastructure. Customer
                    will not copy, reproduce, alter, modify, create derivative works of,
                    or reverse engineer the FATHOM Website or publicly display any content
                    from the FATHOM Website without the prior express written consent of FATHOM.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    7. <strong>TERMINATION</strong>
                </span>
                <span style="font-family:Arial">
                    . Either Customer or FATHOM may terminate this Agreement at any time
                    upon written notice to the other party, but all Orders previously
                    submitted and confirmed by FATHOM shall remain binding upon the parties.
                    Sections 2-5 and 8 shall survive the termination of this Agreement.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;font-weight:bold">
                    8. MISCELLANEOUS PROVISIONS.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.1 <strong>FORCE MAJEURE</strong>
                </span>
                <span style="font-family:Arial">
                    . Neither party shall be liable for a breach of its obligations
                    hereunder to the extent that such breach is caused by factors outside
                    its reasonable ability to foresee and avoid (provided that such party makes diligent
                    good faith efforts to remedy the breach as soon as possible),
                    including without limitation, war, acts of God, terrorism, natural disaster,
                    or third party communications or encryption failure, and which
                    continue for any 60 day period.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.2 <strong>ENTIRE AGREEMENT</strong>
                </span>
                <span style="font-family:Arial">
                    . This Agreement and any accompanying Orders and
                    Non-Disclosure Agreement embody the entire agreement
                    and understanding between the parties regarding the
                    subject matter hereof, and supersede any prior understanding
                    and agreements between and among them respecting the subject
                    matter hereof. FATHOM may at any time change or modify the
                    terms of this Agreement by publishing such additional or
                    replacement terms on the Website at least five business days
                    prior to implementation. If the Customer does not agree with
                    any change or modification, the Customer may terminate this
                    Agreement on notice to FATHOM.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.3 <strong>INDEPENDENT CONTRACTORS</strong>
                </span>
                <span style="font-family:Arial">
                    . The parties hereto are independent contractors and are not,
                    and shall not represent themselves as, principal and agent,
                    partners or joint ventures. No party shall attempt to act,
                    or represent itself as having the power, to bind another party
                    or create any obligation on behalf of another party. Each party
                    shall be solely responsible for the employment, direction and
                    control of its employees and their acts.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.4 <strong>SEVERABILITY</strong>
                </span>
                <span style="font-family:Arial">
                    . Whenever possible, each provision of this Agreement will be
                    interpreted in such manner as to be effective and valid under
                    applicable law. The parties agree that (a) the provisions of this
                    Agreement shall be severable in the event that any of the provisions
                    hereof are for any reason whatsoever invalid, void or otherwise unenforceable,
                    (b) such invalid, void or otherwise unenforceable provisions
                    shall be automatically replaced by other provisions which are as
                    similar as possible in terms to such invalid, void or otherwise
                    unenforceable provisions but are valid and enforceable, and (c) the
                    remaining provisions shall remain enforceable to the fullest extent
                    permitted by law.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.5 <strong>ASSIGNMENT</strong>
                </span>
                <span style="font-family:Arial">
                    . Customer shall not assign the rights and obligations under
                    this Agreement without the written consent of FATHOM. Any assignment in
                    breach of this provision shall be void.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.6 <strong>GOVERNING LAW</strong>
                </span>
                <span style="font-family:Arial">
                    . This Agreement is made under and shall be construed in
                    accordance with the laws of the State of California, without giving
                    effect to its choice of law rules. The state and federal courts
                    located in Alameda County, California shall have exclusive
                    jurisdiction to adjudicate any dispute arising out of or relating to
                    this Agreement. Each party hereby consents to the jurisdiction of
                    such courts and waives any right it may otherwise have to
                    challenge the appropriateness of such forums, whether on the
                    basis of the doctrine of forum non conveniens or otherwise.
                </span>
            </p>

            <p>
                <span style="font-family:'Varela Round',sans-serif;font-size:16px; line-height:22px;">
                    8.7 <strong>NOTICES</strong>
                </span>
                <span style="font-family:Arial">
                    . All legal notices to FATHOM shall be sent to 3000F Danville Blvd.
                    #141, Alamo, CA 94507, addressed to the attention of “Principal”.
                </span>
            </p>

        </div>

        <div style="text-align: center; width:100%;">
            <a href="/login" style="margin: 0 auto" onclick="window.close()" class="btn btn-custom">Close</a>
        </div>

    </div>
@endsection


@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/terms-of-service.css') }}">
@endsection