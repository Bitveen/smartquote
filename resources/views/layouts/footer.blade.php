<div class="footer-bottom">
    <div class="footer-hr"></div>
    <div class="navbar navbar-static-bottom">
        <div class="navbar-inner">
            <div class="container footer-top">
                <div class="row">
                    <div class="span3">
                        <a href="http://www.studiofathom.com">
                            <img src="{{ asset('images/fathom_footer_logo.gif') }}" />
                        </a>
                    </div>
                    <div class="span3">
                        <ul style="list-style-type: none;">
                            <li><a href="{{ route('home.index') }}">HOME</a></li>
                            <li><a href="{{ route('faq.index') }}">FAQ</a></li>
                            <li><a href="http://studiofathom.com/blog/" target="_blank">BLOG</a></li>
                            <li><a href="{{ route('contact.index') }}">CONTACT</a></li>
                        </ul>
                    </div>
                    <div class="span3">
                        <p class="location-title">LOCATIONS</p>
                        <p class="location">OAKLAND OFFICE<br>
                            (510) 281-9000<br>
                            <a target="_blank" href="http://goo.gl/maps/xvF8c">620 3rd Street<br>
                                Oakland, CA 94607
                            </a>
                        </p>
                        <p class="location">OAKLAND PRODUCTION CENTER<br>
                            <a target="_blank" href="http://goo.gl/maps/FmTQD">
                                315 Jefferson Street<br>
                                Oakland, CA 94607
                            </a>
                        </p>
                    </div>
                    <div class="span3">
                        <p class="location-title">&nbsp;</p>
                        <p class="location">SEATTLE OFFICE + PRODUCTION CENTER<br>
                            (206) 582-1062<br>
                            <a target="_blank" href="http://goo.gl/maps/7JATW">
                                4302 Stone Way North<br>
                                Seattle, WA 98103
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="width-constraint footer-copy clearfix">
            <p style="text-align: center;">
                &copy;{{ $now->year }} SmartQuote, All Rights Reserved.
                Powered By FATHOM – Stratasys 3D Printer Sales + 3D Printing Production Centers + Prototyping
                &amp; Low Volume Production + Additive &amp; Advanced Manufacturing
            </p>
        </div>
    </div>
</div>