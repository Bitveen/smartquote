<div class="navbar" role="navigation">
    <div class="container-fluid log-banner">
        <div class="container">
            <div class="log">
                @if(Auth::check())
                    Welcome, {{ Auth::user()->full_name }} //
                    <a href="{{ route('account.index') }}">My Account</a> //
                    <a href="{{ route('sessions.destroy') }}">Logout</a>
                @else
                    <a href="{{ route('sessions.create') }}">Login</a> //
                    <a href="{{ route('registration.create') }}">Register</a> //
                    <a href="mailto:SQHelp@studiofathom.com">Email Us</a>
                @endif
            </div>
        </div>
    </div>
    <div class="container-fluid nav-banner">
        <div class="container">

            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('images/smartquote_powered.png') }}" />
                </a>
            </div>

            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('/') ? 'active' : null }}">
                        <a href="{{ route('home.index') }}">HOME</a>
                    </li>
                    <li class="{{ Request::is('knowledge*') ? 'active' : null }}">
                        <a href="{{ route('knowledge.index') }}">KNOWLEDGE CENTER</a>
                    </li>
                    <li class="{{ Request::is('faq') ? 'active' : null }}">
                        <a href="{{ route('faq.index') }}">FAQ</a>
                    </li>
                    <li>
                        <a href="http://studiofathom.com/blog/" target="_blank">BLOG</a>
                    </li>
                    <li class="{{ Request::is('contact') ? 'active' : null }}">
                        <a href="{{ route('contact.index') }}">CONTACT</a>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="span12">
                    <span class="assistly-widget">
                        <a id="liveagent_button_online_57380000000PBiu" style="display: none;" href="#">
                            <img src="{{ asset('images/button_live_chat.png') }}" />
                        </a>
                        <div id="liveagent_button_offline_57380000000PBiu" style="display: none;">
                            <a href="mailto:sqhelp@studiofathom.com?Subject=SmartQuote%20Question">
                                <img src="{{ asset('images/button_ask_us.png') }}" />
                            </a>
                        </div>
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>