<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="application-name" content="SmartQuote" />
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('meta')
        <meta name="title" content="Quickly and easily get quotes and place orders for 3D printing projects" />
        <meta name="description" content="Fast, simple, automated quoting and ordering of 3D prints for engineers and designers. Log in or sign up to get started." />
        <meta name="keywords" content="prototyping, 3d printing services, rapid prototyping, quoting and ordering, SLS, PolyJet, FDM, Oakland, Seattle" />
    @show
    <title>SmartQuote - @yield('pageTitle')</title>
    @section('styles')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://d218iqt4mo6adh.cloudfront.net/assets/widget_embed_191.css" />
        <link rel="stylesheet" href="{{ asset('css/common.css') }}">
    @show
</head>
<body>
    @include('layouts.navigation')
    @yield('content')
    @include('layouts.footer')
    @section('scripts')
        <script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '{{ '' }}']);
            _gaq.push(['_setDomainName', 'studiofathom.com']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>
        <script src="https://c.la4-c1-chi.salesforceliveagent.com/content/g/js/39.0/deployment.js"></script>
        <script src="https://d218iqt4mo6adh.cloudfront.net/assets/widget_embed_libraries_191.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/validator/9.4.1/validator.min.js"></script>
        <script src="{{ asset('js/common.js') }}"></script>
    @show
</body>
</html>
