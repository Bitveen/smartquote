@extends('layouts.master')

@section('pageTitle', 'Step 2: Review Quote')

@section('content')
    <main>
        <div class="container content">
            @include('quotes.partials.process-nav')
            <div class="row" style="margin-top: 30px;">
                <div class="span12 well-white">
                    @if(session('manualQuoteRequested'))
                        <h2 style="color: #00c1d5; text-align: center; margin-bottom: 30px">Quote Requested</h2>
                        <div style="text-align: center;">
                            <h2 style="margin-bottom: 30px">Thank You!</h2>
                            <p>Our team will review your request and contact you via email or phone with any questions.</p>
                            <p>
                                <strong>For 3D printing quotes</strong>, please allow <strong>1 hour</strong> for review and quotation.<br>
                                <strong>For CNC machining and urethane casting quotes</strong>, please allow <strong>12-24</strong> hours for review and quotation.<br>
                                Quote requests placed outside of business hours will be returned as soon as possible on the next business day.
                            </p>
                        </div>
                    @else
                        <div>
                            <p style="margin-top: 15px; margin-bottom: 30px">
                                You may leave and come back to your quote at a later time and make modifications. Simply go to the Order & Quote History page.
                            </p>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Preview</th>
                                <th>Dimensions</th>
                                <th>Material</th>
                                <th>Finish</th>
                                <th>Quantity</th>
                                <th>Notes</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($quote->parts)
                                @foreach($quote->parts as $part)
                                    <tr class="item-name">
                                        <td class="item-picture" rowspan="2">
                                            <img src="/storage/orders/{{$part->order_id}}/parts/{{$part->id}}/1.jpg">
                                        </td>
                                        <td colspan="6">
                                            <div>
                                                <b>Part:</b> {{ $part->name }}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="part-info">
                                        <td>
                                            <b>{{ round($part->x, 2) }} {{ $part->units }}</b> &times;
                                            <b>{{ round($part->y, 2) }} {{ $part->units }}</b> &times;
                                            <b>{{ round($part->z, 2) }} {{ $part->units }}</b>
                                        </td>
                                        <td>
                                            {{ $part->material->name }}
                                            @if($part->multi_material)
                                                <br />
                                                This part has multiple materials
                                            @endif
                                        </td>
                                        <td>
                                            {{ ucfirst($part->finish) }}
                                        </td>
                                        <td>
                                            {{ $part->quantity }}
                                        </td>
                                        <td class="form-inline">
                                            {{ $part->process->title }}
                                        </td>
                                        <td class="quote-total">
                                            @if($part->isManual())
                                                Manual Quote Required
                                            @else
                                                ${{ $part->price }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-block alert-error">
                                            <h4 class="alert-heading">Oops, no files were found!</h4>
                                            <p>
                                                Your order currently has no files that can be processed, please
                                                <a href="{{ route('quotes.upload.show', ['quoteId' => $quote->id]) }}">add some new files</a>.
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            @if($adjustment)
                                <tr class="minimum-adjustment">
                                    <td colspan="6">
                                    <span class="pull-left">
                                        <b>Quote #{{ $quote->id }}</b>
                                    </span>
                                        <span class="pull-right">
                                        <i class="icon-info-sign" title="A minimum order of ${{ env('MINIMUM_ORDER') }} is required. Price adjustment is automatically applied if needed."></i>
                                        Minimum Order Adjustment:
                                    </span>
                                    </td>
                                    <td class="quote-total">{{ $adjustment }}</td>
                                </tr>
                            @endif
                            </tbody>
                            <tfoot>
                            @if(count($quote->fees))
                                <tr>
                                    <th colspan="7" style="border-top:3px solid gray;">Adjustments</th>
                                </tr>
                                @foreach($quote->fees as $fee)
                                    @if($fee->type && $fee->price)
                                        <tr>
                                            <td colspan="6">
                                                @switch($fee->type)
                                                    @case('material')
                                                    Material Setup :
                                                    @break

                                                    @case('leadtime')
                                                    Processing :
                                                    @break

                                                    @case('discount')
                                                    Discount :
                                                    @break

                                                    @case('account')
                                                    Standing Discount :
                                                    @break

                                                    @default
                                                    Other :
                                                @endswitch
                                                @if($fee->type == 'leadtime')
                                                    @if($fee->name == 'same_day')
                                                        Same Day
                                                    @else
                                                        Next Day
                                                    @endif
                                                @else
                                                    {{ $fee->name }}
                                                @endif
                                            </td>
                                            <td class="quote-total">
                                                @if($fee->type == 'discount' || $fee->type == 'account') - @endif
                                                ${{ $fee->price }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            <tr>
                                <td colspan="6">
                                    <span class="pull-right">Total:</span>
                                </td>
                                <td class="quote-total" id="overall_total">
                                    @if($manualQuote)
                                        <b>Manual Quote Required</b>
                                    @else
                                        <b>{{ $total }}</b>
                                    @endif
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        @include('quotes.partials.manual-quote-motifications')

                        <div class="alert alert-error" style="height: 20px; margin-top: 20px;">
                            <p>
                                <i>
                                    <strong>Please Note:</strong> Minimum recommended feature size is 1 mm (0.040 in.)
                                    Accuracy of features smaller than 1 mm is not guaranteed.
                                </i>
                            </p>
                        </div>

                        @include('quotes.partials.comments')

                        @if(!$manualQuote)
                            <textarea style="width:100%; height:80px" id="comment"></textarea>
                        @endif

                        <div class="actions">
                            @if(!$manualQuoteApproved)
                                <a class="btn btn-custom" id="modify_parts"
                                   href="{{ route('quotes.details', ['quoteId' => $quote->id]) }}">
                                    <i class="icon-arrow-left icon-white"></i>
                                    Modify Parts
                                </a>
                            @endif
                            <span class="pull-right">
                            @if($manualQuote)
                                    <div id="app">
                                    <button @click="$modal.show('requestManualQuote')" class="btn btn-custom">Request Manual Quote</button>
                                    <request-manual-quote-modal></request-manual-quote-modal>
                                </div>
                                @else
                                    <a href="{{ route('quotes.report.download', ['quoteId' => $quote->id]) }}" class="btn btn-secondary">
                                    Download Quote
                                </a>
                                    <button class="btn btn-custom" id="checkout">
                                    Proceed to Checkout
                                </button>
                                @endif
                        </span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <input type="hidden" id="quoteId" value="{{ $quote->id }}">
    </main>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/quote-review.js') }}"></script>
@endsection

@section('styles')
    @parent
    <style>
        table tbody tr.part-info td, table tbody tr.item-name td {
            vertical-align: middle;
        }
        table tbody tr.part-info td {
            text-align: center;
        }
        table thead tr th {
            text-align: center !important;
        }
    </style>
@endsection