@extends('layouts.master')

@section('pageTitle', 'Step 2: File Details')

@section('content')
    <main>
        <div class="container content">
            @include('quotes.partials.process-nav')
            <div class="row" style="margin-top: 30px;">
                <div class="span12 well-white">
                    <div class="file-details-section">
                        <div id="app">
                            <app></app>
                            <div class="spinner-details" v-cloak>
                                <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/quote-details.js') }}"></script>
@endsection

@section('styles')
    @parent
    <link href="{{ asset('css/quote-details.css') }}" rel="stylesheet" />
@endsection