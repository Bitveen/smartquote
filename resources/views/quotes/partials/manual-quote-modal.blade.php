<div class="modal hide" id="manual_quote_comment">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3>Request Manual Quote</h3>
        <textarea style="width:100%; height:150px" id="comments" name="comments" placeholder="Please use this message box for comments, questions, or special application requirements. Thank you for your order — we will follow-up with you shortly."></textarea>
    </div>
    <div class="modal-body">
        <p>We will review your order and send you a quote. Please enter as many details and any questions you may have, and we will get back to you. Thanks!</p>
    </div>
    <div class="modal-footer">
        <input type="submit" id="order" class="btn btn-custom" value="Request Manual Quote">
        <a href="#" data-dismiss="modal" class="btn">Cancel</a>
    </div>
</div>