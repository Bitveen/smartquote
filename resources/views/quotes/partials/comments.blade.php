@if(count($quote->comments))
    <div>
        <h3>Messages regarding this quote:</h3>
        <dl>
            @foreach($quote->comments as $comment)
                <dt>
                    @if($comment->customer_message)
                        You
                    @else
                        Fathom
                    @endif
                    <span style="font-weight: normal">said at {{ $comment->created_at->format('jS \o\f F, Y g:i:s a') }}:</span>
                </dt>
                <dd>{{ $comment->message }}<br /></dd>
            @endforeach
        </dl>
    </div>
@endif