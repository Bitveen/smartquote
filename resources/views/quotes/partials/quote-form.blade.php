<form id="quote_details" method="POST">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Preview</th>
            <th>Dimensions</th>
            <th>Material</th>
            <th>Finish</th>
            <th>Quantity</th>
        </tr>
        </thead>
        <tbody>
            @include('quotes.partials.file-error')
            @foreach($parts as $part)
                <tr class="item-name">
                    <td class="item-picture" rowspan="2">
                        <img src="{{ $part->preview }}">
                    </td>

                    <td colspan="4">
                        <div>
                            <b>Part:</b> {{ $part->name }}
                        </div>
                        <span class="pull-right item-actions" style="display: inline">
                            <button type="button" id="remove_{{ $part->id }}" class="remove-item btn btn-danger">
                                <i class="icon-trash icon-black"></i> Remove
                            </button>
                        </span>
                    </td>
                </tr>

                <tr>

                    <td class="form-inline"  style="background-color: #FFEC00;">
                        <b>{{ $part->max_x }}</b> &times; <b>{{ $part->max_y }}</b> &times; <b>{{ $part->max_z }}</b>
                        <div class="controls">
                            <label class="radio">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    type="radio"
                                    name="unit_{{ $part->id }}"
                                    @if($part->unit == "mm") checked="checked" @endif
                                    value="mm" />
                                Millimeters
                            </label>
                        </div>
                        <div class="controls">
                            <label class="radio">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    type="radio"
                                    name="unit_{{ $part->id }}"
                                    @if($part->unit == "in") checked="checked" @endif
                                    value="inches" />
                                Inches
                            </label>
                        </div>
                    </td>

                    <td>
                        <b>Process:</b>
                        <div data-popover="#processSelector" class="processSelect input-append">
                            <input
                                type="hidden"
                                data-value="1"
                                value="{{ $part->process }}"
                                name="process_{{ $part->id }}"
                                id="process_{{ $part->id }}">
                            <input
                                class="span2"
                                data-display="1"
                                value="{{ $part->process_display }}" type="text">
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                            </div>
                        </div>
                        <br />
                        <b>Material:</b>
                        <div
                            data-popover="#materialSelector"
                            class="materialSelect input-append">
                            <input
                                type="hidden"
                                data-value="1"
                                value="{{ $part->material }}"
                                name="material_{{ $part->id }}"
                                id="material_{{ $part->id }}" />
                            <input
                                class="span2"
                                data-display="1"
                                value="{{ $part->material_display }}"
                                type="text" />
                            <div class="btn-group">
                                <button class="btn dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                            </div>
                        </div>
                        <label class="{% if item.process !='polyjet' %}hide{% endif %} checkbox">
                            <input
                                @if($part->locked) disabled="disabled" @endif
                                type="checkbox" name="multimaterial_{{ $part->id }}"
                                id="multimaterial_{{ $part->id }}"
                                @if($part->multi_material && $part->process =="polyjet") checked="checked" @endif
                                value="1" />
                            This part has multiple materials
                        </label>
                    </td>

                    <td class="form-inline">
                        <i class="finish-polyjet finish-info icon-info-sign" @if($part->process != "polyjet") style="display: none" @endif></i>

                        <div class="controls finish-polyjet" {% if item.process !='polyjet' %}style="display:none"{% endif %}>
                            <label class="radio">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    type="radio"
                                    name="finish_{{ $part->id }}"
                                    @if($part->finish == "matte") checked="checked" @endif
                                    value="matte">
                                Matte
                            </label>
                        </div>

                        <div class="controls finish-polyjet" @if($part->process !="polyjet") style="display:none" @endif>
                            <label class="radio">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    type="radio"
                                    name="finish_{{ $part->id }}"
                                    @if($part->finish == "glossy") checked="checked" @endif
                                    value="glossy" />
                                Glossy
                            </label>
                        </div>

                        <div class="controls finish-sls finish-fdm" @if($part->process != 'sls' && $part->process != 'fdm') style="display:none" @endif>
                            <label class="radio">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    type="radio"
                                    name="finish_{{ $part->id }}"
                                    @if($part->finish == "standard") checked="checked" @endif
                                    value="standard">
                                Standard
                            </label>
                        </div>
                        <div class="controls finish-cnc finish-uc" @if($part->process =='polyjet' || $part->process =='fdm' || $part->process =='sls') style="display:none" @endif>
                            <label class="radio">
                                Specify in<br>Comments
                            </label>
                            <input
                                @if($part->locked) disabled="disabled" @endif
                                type="radio"
                                name="finish_{{ $part->id }}"
                                @if($part->finish == "specify") checked="checked" @endif
                                value="specify"
                                selected="selected"
                                style="visibility:hidden" />
                        </div>
                    </td>

                    <td class="form-inline">
                        <div class="control-group">
                            <div class="controls">
                                <input
                                    @if($part->locked) disabled="disabled" @endif
                                    style="width:40px"
                                    type="number"
                                    name="quantity_{{ $part->id }}"
                                    min="1"
                                    value="{{ $part->quantity }}"
                                />
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</form>