<div class="row" style="padding-bottom: 10px;">
    <div class="offset1 span10">

        <div class="alert alert-yellow">

            <div class="span3">
                <img class="pull-left" src="{{ asset('images/doc-yellow.png') }}">
                <p class="pull-left" style="width: 170px; padding-left: 10px">
                    Accepted file types:
                    STL, IGES, STEP, Catia (v4 &amp;s v5), SolidWorks (up to v2016), ProE, 3D Studio (Max), Rhino.
                    (Up to 100 MBs.)
                </p>
            </div>

            <div class="span3">
                <img class="pull-left" src="{{ asset('images/arrow-yellow.png') }}">
                <p class="pull-left" style="width: 170px; padding-left: 10px">
                    Once your file(s) is ready, click on the upper right blue button "Upload."
                </p>
            </div>

            <div class="span3">
                <img class="pull-left" src="{{ asset('images/percent-yellow.png') }}">

                <p class="pull-left" style="width: 140px; padding-left: 10px">
                    When your upload is complete click on the upper right blue button "Get Quote."
                </p>
            </div>

            <div style="clear: both; margin: auto; text-align: center">
                <i style="clear: both; border: 1px solid #000; border-left: none; border-right: none; font-size: 15px; padding: 3px;">
                    Did you know we can print multiple materials on a single part?
                    <a target="_blank" href="http://studiofathom.com/knowledge-center/technology/polyjet-3d-printing-technology/">
                        <strong>Click here</strong>
                    </a> to learn more!
                </i>
            </div>

        </div>

    </div>
</div>