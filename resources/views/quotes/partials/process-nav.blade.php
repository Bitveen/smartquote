<div class="quote-process clearfix">
    <h2>
        <span class="{{ Request::is('quotes/*/upload') ? 'active' : null }}">
            1. Upload Files
        </span>
        <span class="{{ (Request::is('quotes/*/details') || Request::is('quotes/*/review')) ? 'active' : null }}">
            2. Get Quote
        </span>
        <span class="{{ Request::is('orders/*/details') ? 'active' : null }}">
            3. Place Order
        </span>
    </h2>
</div>