<div class="modal hide" id="manual_quote_approved_warning">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3>Modifying this quote will invalidate the current offer.</h3>
    </div>
    <div class="modal-body">
        <p>Modifying this quote will invalidate the current offer you have
            been quoted. If you still want to change the details Fathom will
            review your updated quote and quote you a price based on that.</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-custom">Modify Quote</a>
        <a href="#" data-dismiss="modal" class="btn">Cancel</a>
    </div>
</div>