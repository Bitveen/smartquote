@if($manualQuoteMulti)
    <div id="material_warning" style="margin:auto;width:400px" class="alert alert-block alert-info multi-material-text">
        <h4 class="alert-heading">Multi-material parts require a manual quote</h4>
        Please click “Request Manual Quote” below &mdash; a specialist will review and approve your quote.
    </div>
@endif

@if($manualQuoteSize)
    <div style="margin: auto; width: 400px" class="alert alert-block alert-info multi-material-text">
        <h4 class="alert-heading">Parts over 8 inches (203mm) require a manual quote</h4>
        Please click “Request Manual Quote” below &mdash; a specialist will review and approve your quote.
    </div>
@endif

@if($manualQuoteMaterial)
    <div style="margin:auto;width:400px" class="alert alert-block alert-info multi-material-text">
        <h4 class="alert-heading">A selected material requires a manual quote</h4>
        Please click “Request Manual Quote” below &mdash; a specialist will review and approve your quote.
    </div>
@endif
