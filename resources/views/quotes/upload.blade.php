@extends('layouts.master')

@section('pageTitle', 'Step 1: Upload Files')

@section('content')
    <main id="app">
        <div class="container content">

            @include('quotes.partials.process-nav')

            <div class="row">
                <div class="span10 offset1">
                    <div class="spinner-container" v-cloak>
                        <div class="spinner">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                    </div>
                    <div class="uploader" v-cloak>
                        <span class="pull-right upload_top">
                            <span class="pull-right" style="width: 200px; margin-top: 8px; margin-right: 6px;">

                                <label class="btn btn-custom upload-btn">
                                    <input type="file" @change="uploadFiles" multiple style="display: none;" />
                                    <span>
                                        <i class="icon-upload icon-white"></i>
                                        Upload
                                    </span>
                                </label>

                                <button @click="getQuote" id="getQuote" :class="{ 'btn-custom': filesUploaded }" :disabled="!filesUploaded" class="btn get-quote-btn">Get Quote</button>
                            </span>
                        </span>
                        <div class="upload-section">
                            <div v-droppable class="upload-files clearfix" :class="{ 'sq-uploader-upload-destination': !cadFiles.length }">
                                <div class="upload-files-separator"></div>
                                <cad-file @delete-cad="deleteCad" v-for="file in cadFiles" :key="file.id" :file="file"></cad-file>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <get-quote-modal></get-quote-modal>
            @include('quotes.partials.upload-footer')
        </div>
    </main>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/upload.css') }}">
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/upload.js') }}"></script>
@endsection