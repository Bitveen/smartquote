<div class="span12">
    <br />
    <ul id="purchase_type" class="nav nav-pills">
        <li id="CC" @if(!$order->purchase) class="active" @endif>
            <a data-toggle="pill" href="#credit_card_type">
                Credit Card
            </a>
        </li>
        <li id="PO" @if($order->purchase) class="active" @endif>
            <a data-toggle="pill" href="#purchase_order_type">
                Purchase Order
            </a>
        </li>
    </ul>
</div>