<div class="order-details-actions">
    <a href="{{ route('quotes.details', ['quoteId' => $order->id]) }}" class="btn modify-parts-btn">Modify Quote</a>
    <button class="btn btn-custom continue-checkout-btn" id="checkout">
        Continue Checkout
    </button>
</div>