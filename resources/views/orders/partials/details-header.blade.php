<div class="order-details-header">
    <h4>Order Details &mdash; An invoice will be sent to your email address once the order is placed.</h4>
    <div class="row">
        <div class="span2">
            <p>Order Number:</p>
            <strong>{{ $order->id }}</strong>
        </div>
        <div class="span4">
            <p>Total: <i>(Does not include Shipping and Tax)</i></p>
            <strong>{{ $total }}</strong>
        </div>
    </div>
</div>