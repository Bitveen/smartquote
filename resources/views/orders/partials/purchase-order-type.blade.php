<div id="purchase_order_type" class="tab-pane @if($order->purchase) active @endif">
    <div class="span5" style="margin-left:0px;">
        <legend>Purchase Order Contact</legend>

        <div class="control-group">
            <label class="control-label" for="email_address">Email Address:</label>
            <div class="controls">
                <input value="{{ $order->email_address ?? $user->email_address }}" type="text" name="po_email_address" id="email_address" />
                <p class="hide help-inline">Please enter your email address.</p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="phone_number">Phone Number:</label>
            <div class="controls">
                <input value="{{ $order->phone_number }}" type="text" name="po_phone_number" id="phone_number" />
                <p class="hide help-inline">Please enter your phone number in this format 000-000-0000</p>
            </div>
        </div>

        <input type="hidden" name="purchase_order" value="" id="purchase_order" />
    </div>

    <div class="offset1 span5">
        <legend>Promo Code</legend>
        <div class="control-group">
            <label class="control-label" for="promo_code">Promo Code:</label>
            <div class="controls">
                <input value="{{ $order->po_coupon_code }}" type="text" name="po_coupon_code" id="promo_code" />
                <p class="hide help-inline">Sorry, that coupon is not valid.</p>
            </div>
        </div>
    </div>
</div>