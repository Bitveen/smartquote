<div class="payment-details">
    <div class="payment-details__item">
        <h4>Payment Details</h4>
        <div class="control-group">
            <label class="control-label" for="credit_card_number">Credit Card Number:</label>
            <div class="controls">
                <input value="" class="input-small" type="text" name="credit_card_number" id="credit_card_number" />
                <p class="hide help-inline">Please enter a valid credit card number.</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="exp_month_field">Expiration Date:</label>
            <div class="controls">
                <select class="input-mini" name="exp_month">
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select class="input-small" name="exp_year">
                    @foreach($years as $year)
                        <option value="{{ $year }}">{{ $year }}</option>
                    @endforeach
                </select>
                <p class="hide help-inline">Please enter an expiration date</p>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="ccv">Security Code (CCV):</label>
            <div class="controls">
                <input value="" type="text" name="ccv" id="ccv" />
                <p class="hide help-inline">Please enter a valid CCV.</p>
            </div>
        </div>
    </div>
    <div class="payment-details__item">
        <h4>Promo Code</h4>
        <div class="control-group">
            <label class="control-label" for="coupon_code">Promo Code:</label>
            <div class="controls">
                <input value="" type="text" name="coupon_code" id="coupon_code" />
                <p class="hide help-inline">Sorry, that coupon is not valid.</p>
            </div>
        </div>
    </div>
</div>