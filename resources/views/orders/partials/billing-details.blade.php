<div class="billing-details">
    <h4>Billing Details</h4>
    <div class="control-group">
        <label class="control-label" for="first_name">First Name:</label>
        <div class="controls">
            <input value="{{ $order->first_name }}" class="input-large" type="text" name="first_name" id="first_name" />
            <p class="hide help-inline">Please enter your first name</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="last_name">Last Name:</label>
        <div class="controls">
            <input value="{{ $order->last_name }}" class="input-large" type="text" name="last_name" id="last_name" />
            <p class="hide help-inline">Please enter your last name</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="street_address">Street Address:</label>
        <div class="controls">
            <input value="{{ $order->street_address }}" class="input-large" type="text" name="street_address" id="street_address" />
            <p class="hide help-inline">Please enter your street address</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="city">City:</label>
        <div class="controls">
            <input value="{{ $order->city }}" class="input-large" type="text" name="city" id="city" />
            <p class="hide help-inline">Please enter your city</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="state_id">State:</label>
        <div class="controls">
            <select name="state_id" id="state_id">
                <option value="">Select a State</option>
                @foreach($states as $state)
                    <option
                        {{ $order->state_id === $state->id ? 'selected' : null }}
                        value="{{ $state->id }}">
                        {{ $state->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    @if(!$user->reseller)
        <div id="ca_alert" class="alert alert-block alert-info hide" style="width:400px">
            <h4 class="alert-heading">Sales Tax will be added</h4>
            Sales tax will be added to this order.
            You will see the amount and rate on the next page before your order is submitted
        </div>
    @endif
    <div class="control-group">
        <label class="control-label" for="zip_code">Zip Code:</label>
        <div class="controls">
            <input value="{{ $order->zip_code }}" class="input-small" type="text" name="zip_code" id="zip_code" />
            <p class="hide help-inline">Please enter your zip code</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="phone_number">Phone Number:</label>
        <div class="controls">
            <input value="{{ $order->phone_number }}" type="text" name="phone_number" id="phone_number" />
            <p class="hide help-inline">Please enter your phone number in this format 000-000-0000</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="email_address">Email Address:</label>
        <div class="controls">
            <input value="{{ $order->email_address }}" class="input-large" type="text" name="email_address" id="email_address" />
            <p class="hide help-inline">Please enter your email address</p>
        </div>
    </div>
</div>