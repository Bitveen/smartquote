<div class="delivery-details">
    <h4>Delivery Details</h4>

    <div class="delivery-details-pickup">
        <div id="pickup_span">
            <label class="checkbox">
                <input type="checkbox" id="pickup" name="pickup" /> I would like to pick up this order. (No Shipping Cost)
            </label>
        </div>
        <div id="pickup_form" style="display: none;">
            <div class="control-group">
                <label class="control-label" for="pickup_location">Pickup From:</label>
                <div class="controls">
                    <select name="pickup_location" id="pickup_location">
                        <option value="-1" disabled>Please select a pick up location.</option>
                        <option @if($order->pickup_location == "pick_up") selected="selected" @endif value="pick_up">
                            Oakland, CA
                        </option>
                        <option @if($order->pickup_location == "pick_up_seattle_wa") selected="selected" @endif value="pick_up_seattle_wa">
                            Seattle, WA
                        </option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="pickup_name">Pick Up Name:</label>
                <div class="controls">
                    <input value="{{ $order->pickup_name }}" class="input-large" type="text" name="pickup_name" id="pickup_name" />
                    <p class="hide help-inline">Please enter who is picking up this order.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="shipping-information">
        <div id="cc_shipping">
            <label class="checkbox">
                <input type="checkbox" id="same_as_billing" name="same_as_billing" /> Shipping information is the same as billing address.
            </label>
        </div>
        <div class="shipping-information-inputs">
            <div class="control-group">
                <label class="control-label" for="shipping_first_name">First Name:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->first_name }}" class="input-large" type="text" name="shipping_first_name" id="shipping_first_name" />
                    <p class="hide help-inline">Please enter your first name for shipping</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="shipping_last_name">Last Name:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->last_name }}" class="input-large" type="text" name="shipping_last_name" id="shipping_last_name" />
                    <p class="hide help-inline">Please enter your last name for shipping</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="shipping_street_address">Street Address:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->street_address }}" class="input-large" type="text" name="shipping_street_address" id="shipping_street_address" />
                    <p class="hide help-inline">Please enter your street address for shipping</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="shipping_city">City:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->city }}" class="input-large" type="text" name="shipping_city" id="shipping_city" />
                    <p class="hide help-inline">Please enter your city for shipping</p>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="shipping_state_id">State:</label>
                <div class="controls">
                    <select name="shipping_state_id" id="shipping_state_id">
                        <option value="">Select a State</option>
                        @foreach($states as $state)
                            <option
                                {{ $user->shipping->id === $state->id ? 'selected' : null }}
                                value="{{ $state->id }}">
                                {{ $state->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="shipping_zip_code">Zip Code:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->zip_code }}" class="input-small" type="text" name="shipping_zip_code" id="shipping_zip_code" />
                    <p class="hide help-inline">Please enter your zip code for shipping</p>
                </div>
            </div>
            <input type="hidden" name="shipping_company" value="fedex" id="shipping_company" />
        </div>
    </div>

    <div class="shipping-carier">
        <div id="carrier_span">
            <label class="checkbox">
                <input type="checkbox" id="carrier_enable" name="carrier_enable" /> I would like to use my own shipping carrier.
            </label>
        </div>

        <div id="shipping-preference">
            <div class="control-group">
                <label class="control-label" for="shipping_method">Shipping Preference:</label>
                <div class="controls">
                    <select name="shipping_method" id="shipping_method">
                        <option value="" disabled>-- Select a shipping preference --</option>
                    </select>
                </div>
            </div>
        </div>

        <div id="own-shipping-carier" style="display: none;">
            <div class="control-group">
                <label class="control-label" for="shipping_account_carrier">Shipping Carrier:</label>
                <div class="controls">
                    <select id="shipping_account_carrier" name="shipping_account_carrier">
                        <option value="">-- Select a shipping carrier --</option>
                        <option value="ups">UPS</option>
                        <option value="fedex">FedEx</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="shipping_account_number">Account Number:</label>
                <div class="controls">
                    <input value="{{ $user->shipping->account_number }}" class="input-large" type="text" name="shipping_account_number" id="shipping_account_number" />
                    <p class="hide help-inline">Please enter your shipping account number</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="delivery_preference">Delivery Preference:</label>
                <div class="controls">
                    <select name="delivery_preference" id="delivery_preference">
                        <option value="">-- Select a delivery option --</option>

                        <option data-carrier="fedex" value="FEDEX_FIRST_OVERNIGHT">FedEx First Overnight</option>
                        <option data-carrier="fedex" value="FEDEX_PRIORITY_OVERNIGHT">FedEx Priority Overnight</option>
                        <option data-carrier="fedex" value="FEDEX_STANDARD_OVERNIGHT">FedEx Standard Overnight</option>
                        <option data-carrier="fedex" value="FEDEX_2_DAY_AM">FedEx 2 Day AM</option>
                        <option data-carrier="fedex" value="FEDEX_2_DAY">FedEx 2 Day</option>
                        <option data-carrier="fedex" value="FEDEX_EXPRESS_SAVER">FedEx Express Saver</option>
                        <option data-carrier="fedex" value="FEDEX_GROUND">FedEx Ground</option>

                        <option data-carrier="ups" value="UPS_NEXT_DAY_AIR_AM">UPS Next Day Air Early AM</option>
                        <option data-carrier="ups" value="UPS_NEXT_DAY_AIR">UPS Next Day Air</option>
                        <option data-carrier="ups" value="UPS_2_DAY_AIR">UPS 2 Day Air</option>
                        <option data-carrier="ups" value="UPS_GROUND">UPS Ground</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>