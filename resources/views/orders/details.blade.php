@extends('layouts.master')

@section('pageTitle', 'Step 3: Order')

@section('content')
    <main>
        <div class="container content">
            @include('quotes.partials.process-nav')
            <div style="margin-top: 30px;">
                <div class="well-white" style="box-sizing: border-box">
                    @include('orders.partials.details-header')
                    <form action="" class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            @include('orders.partials.billing-details')
                            @include('orders.partials.payment-details')
                        </div>
                        <div class="row">
                            @include('orders.partials.delivery-details')
                        </div>
                        @include('orders.partials.details-actions')
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/order-details.js') }}"></script>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/order-details.css') }}">
@endsection