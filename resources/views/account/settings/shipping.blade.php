@extends('account.settings.layout')

@section('pageTitle', 'Shipping Information')

@section('settingsArea')
    <div class="well-white">
        <form action="{{ route('account.shipping.update') }}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <fieldset>
                <div class="control-group leftspace">
                    <div class="welcome" style="margin-bottom: 30px;">SHIPPING INFORMATION</div>

                    @if(session('shipping-updated'))
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p>
                                Shipping information updated.
                            </p>
                        </div>
                    @endif

                    @include('account.settings.partials.errors')

                    <div class="control-group">
                        <label class="control-label" for="first_name" style="width:170px; padding-right:10px;color:#00c1d5;">First Name*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->first_name }}"
                                type="text" name="first_name"
                                id="first_name" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="last_name" style="width:170px; padding-right:10px;color:#00c1d5;">Last Name*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->last_name }}"
                                type="text" name="last_name"
                                id="last_name" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="address" style="width:170px; padding-right:10px;color:#00c1d5;">Address 1*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->address }}"
                                type="text" name="address"
                                id="address" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="additional_address" style="width:170px; padding-right:10px;color:#00c1d5;">Address 2</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->additional_address }}"
                                type="text" name="additional_address"
                                id="additional_address" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="city" style="width:170px; padding-right:10px;color:#00c1d5;">City*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->city }}"
                                type="text" name="city"
                                id="city" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="state_id" style="width:170px; padding-right:10px;color:#00c1d5;">State</label>
                        <div class="controls">
                            <select name="state_id" id="state_id">
                                <option value="">Select a State</option>
                                @foreach($states as $state)
                                    <option
                                        {{ Auth::user()->shipping->state_id === $state->id ? 'selected' : null }}
                                        value="{{ $state->id }}">
                                        {{ $state->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="zip_code" style="width:170px; padding-right:10px;color:#00c1d5;">Zip*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->shipping->zip_code }}"
                                type="text" name="zip_code"
                                id="zip_code" style="width:350px;" />
                        </div>
                    </div>

                    <button class="btn btn-custom btn-profile" value="Save">Save</button>
                </div>
            </fieldset>
        </form>
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-shipping.css') }}">
@endsection