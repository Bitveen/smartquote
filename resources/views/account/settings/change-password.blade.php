@extends('account.settings.layout')

@section('pageTitle', 'Shipping Information')

@section('settingsArea')
    <div class="well-white">
        <form action="{{ route('account.change-password.update') }}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <fieldset>
                <div class="control-group leftspace">
                    <div class="welcome" style="margin-bottom: 30px;">CHANGE PASSWORD</div>

                    @if(session('password-updated'))
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p>
                                Password has been changed.
                            </p>
                        </div>
                    @endif

                    @if(session('password-mismatch'))
                        <div class="alert alert-block alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p>
                                Your current password is invalid.
                            </p>
                        </div>
                    @endif

                    @include('account.settings.partials.errors')

                    <div class="control-group">
                        <label class="control-label" for="password" style="width:170px; padding-right:10px;color:#00c1d5;">Current Password</label>
                        <div class="controls">
                            <input
                                type="password" name="password"
                                id="password" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="new_password" style="width:170px; padding-right:10px;color:#00c1d5;">New Password</label>
                        <div class="controls">
                            <input
                                type="password" name="new_password"
                                id="new_password" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="new_password_confirmation" style="width:170px; padding-right:10px;color:#00c1d5;">Re-Enter New Password</label>
                        <div class="controls">
                            <input
                                type="password" name="new_password_confirmation"
                                id="new_password_confirmation" style="width:350px;" />
                        </div>
                    </div>

                    <button class="btn btn-custom btn-profile">Save</button>

                </div>
            </fieldset>
        </form>
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-shipping.css') }}">
@endsection