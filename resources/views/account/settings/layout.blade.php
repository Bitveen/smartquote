@extends('layouts.master')

@section('content')
    <main>
        <div class="container content" style="margin-top: 60px;">
            <div class="row">
                @include('account.settings.partials.sidebar')
                <div class="span9">
                    @yield('settingsArea')
                </div>
            </div>
        </div>

    </main>
@endsection