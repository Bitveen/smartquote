<div class="span3">
    <div class="well-white">
        <div class="homeheader">ACCOUNT SETTINGS</div>
        <div class="account-settings-nav">
            <div style="display: table;">
                <a class="{{ Request::is('account/settings/profile') ? 'active' : null }}"
                    href="{{ route('account.profile.edit') }}">
                    Profile Information
                </a>
                <br>
                <a class="{{ Request::is('account/settings/shipping') ? 'active' : null }}"
                    href="{{ route('account.shipping.edit') }}">
                    Shipping Information
                </a>
                <br>
                <a class="{{ Request::is('account/settings/change_password') ? 'active' : null }}"
                    href="{{ route('account.change-password.edit') }}">
                    Change Password
                </a>
            </div>
        </div>
    </div>
</div>