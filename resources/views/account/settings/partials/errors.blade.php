@if($errors->any())
    @foreach($errors->all() as $message)
        <div class="alert alert-block alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p>
                {{ $message }}
            </p>
        </div>
    @endforeach
@endif