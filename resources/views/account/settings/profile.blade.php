@extends('account.settings.layout')

@section('pageTitle', 'Profile Information')

@section('settingsArea')
    <div class="well-white">
        <form action="{{ route('account.profile.update') }}" class="form-horizontal" method="POST">
            <fieldset>
                {{ csrf_field() }}
                <div class="control-group leftspace">
                    <div class="welcome" style="margin-bottom: 30px">PROFILE INFORMATION</div>

                    @if(session('profile-updated'))
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <p>
                                Profile information updated.
                            </p>
                        </div>
                    @endif

                    @include('account.settings.partials.errors')

                    <div class="control-group">
                        <label class="control-label" for="first_name" style="width:170px; padding-right:10px;color:#00c1d5;">First Name*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->first_name }}"
                                type="text" name="first_name"
                                id="first_name" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="last_name" style="width:170px; padding-right:10px;color:#00c1d5;">Last Name*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->last_name }}"
                                type="text" name="last_name"
                                id="last_name" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="phone_number" style="width:170px; padding-right:10px;color:#00c1d5;">Phone*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->phone_number }}"
                                type="text" name="phone_number"
                                id="phone_number" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="company" style="width:170px; padding-right:10px;color:#00c1d5;">Company*</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->company }}"
                                type="text" name="company"
                                id="company" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="physical" style="width:170px; padding-right:10px;color:#00c1d5;">Physical Location</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->physical }}"
                                type="text" name="physical"
                                id="physical" style="width:350px;" placeholder="(City, State)"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="industry" style="width:170px; padding-right:10px;color:#00c1d5;">Industry</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->industry }}"
                                type="text" name="industry"
                                id="industry" style="width:350px;" />
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="title" style="width:170px; padding-right:10px;color:#00c1d5;">Title</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->title }}"
                                type="text" name="title"
                                id="title" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="contact" style="width:170px; padding-right:10px;color:#00c1d5;">Preferred Contact Method</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->contact }}"
                                type="text" name="contact"
                                id="contact" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="software" style="width:170px; padding-right:10px;color:#00c1d5;">Preferred Design Software</label>
                        <div class="controls">
                            <input
                                value="{{ Auth::user()->software }}"
                                type="text" name="software"
                                id="software" style="width:350px;" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" style="width:170px; padding-right:10px;color:#00c1d5;">Other Comments</label>
                        <div class="controls">
                            <textarea id="comment" name="comment" style="width:350px;">{{ Auth::user()->comment }}</textarea>
                        </div>
                    </div>

                    <button class="btn btn-custom btn-profile">Save</button>
                </div>
            </fieldset>
        </form>
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-profile.css') }}">
@endsection