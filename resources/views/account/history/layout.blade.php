@extends('layouts.master')

@section('content')
    <main>
        <div class="container content" style="margin-top: 60px;">
            <div class="row">
                @include('account.history.partials.sidebar')
                <div class="span9">
                    @yield('historyArea')
                </div>
            </div>
        </div>
    </main>
@endsection