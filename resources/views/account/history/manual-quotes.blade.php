@extends('account.history.layout')

@section('pageTitle', 'Manual Quote History')

@section('historyArea')
    <div class="well-white">
        <div class="welcome" style="margin-bottom: 30px;">MANUAL QUOTES</div>

        @if(count($manualQuotes))
            <table class="table manual-quotes overview-table history">
                <thead>
                <tr>
                    <th>#</th>
                    <th>PREVIEW</th>
                    <th>QUOTE PRICE</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($manualQuotes as $quote)
                        <tr>
                            <td class="key">{{ $loop->iteration }}</td>
                            <td></td>
                            <td class="price"></td>
                            <td class="removed"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>You do not have any manual quotes.</p>
        @endif

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-shipping.css') }}">
@endsection