<div class="span3">
    <div class="well-white">
        <div class="homeheader">QUOTE &amp;<br>ORDER HISTORY</div>
        <div class="account-settings-nav">
            <div style="display: table;">
                <a class="{{ Request::is('account/history/quotes') ? 'active' : null }}"
                    href="{{ route('history.quotes') }}">Quotes</a><br>
                <a class="{{ Request::is('account/history/manual_quotes') ? 'active' : null }}"
                    href="{{ route('history.manual-quotes') }}">Manual Quotes</a><br>
                <a class="{{ Request::is('account/history/orders') ? 'active' : null }}"
                    href="{{ route('history.orders') }}">Orders</a>
            </div>
        </div>
    </div>
</div>