@extends('account.history.layout')

@section('pageTitle', 'Order History')

@section('historyArea')
    <div class="well-white">
        <div class="welcome" style="margin-bottom: 30px;">ORDERS</div>

        @if(count($orders))
            <table class="table overview-table history">
                <thead>
                <tr>
                    <th>#</th>
                    <th>PREVIEW</th>
                    <th>ORDER PRICE</th>
                    <th>STATUS</th>
                    <th>PURCHASE DATE</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td class="key">{{ $loop->iteration }}</td>
                            <td>
                                <div class="file-list"></div>
                            </td>
                            <td class="price">${{ $order->amount }}</td>
                            <td class="status">{{ $order->status }}</td>
                            <td class="updated">{{ $order->updated_at }}</td>
                            <td>
                                <a href="#" class="download pull-left"></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>You do not have any orders.</p>
        @endif

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-shipping.css') }}">
@endsection