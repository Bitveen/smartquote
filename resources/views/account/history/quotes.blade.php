@extends('account.history.layout')

@section('pageTitle', 'Quote History')

@section('historyArea')
    <div class="well-white">
        <div class="welcome" style="margin-bottom: 30px;">QUOTES</div>

        @if(count($quotes))
            <table class="table quotes overview-table history">
                <thead>
                <tr>
                    <th>#</th>
                    <th>PREVIEW</th>
                    <th>QUOTE PRICE</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($quotes as $quote)
                        <tr data-order-id="{{ $loop->iteration }}" style="cursor: pointer;">
                            <td class="key">{{ $loop->iteration }}</td>
                            <td></td>
                            <td class="price"></td>
                            <td class="removed"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>You do not have any quotes.</p>
        @endif

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account-shipping.css') }}">
@endsection