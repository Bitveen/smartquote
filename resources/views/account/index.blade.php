@extends('layouts.master')

@section('pageTitle', 'Account')

@section('content')
    <main>
        <div class="container content" style="margin-top: 60px;">
            <div class="row">

                <div class="span6">
                    @include('account.partials.my-account')
                    @include('account.partials.my-quotes')
                    @include('account.partials.my-orders')
                </div>

                <div class="span6">
                    <a href="#" class="btn quote_btn btn-custom">START A NEW QUOTE</a>
                    @include('account.partials.promo')
                    @include('account.partials.resources')
                </div>

            </div>
        </div>
    </main>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/account.css') }}">
@endsection