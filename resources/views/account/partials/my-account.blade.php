<div class="well-white">
    <div class="homeheader">MY ACCOUNT</div>
    <div class="account-block">

        <div class="account-block__avatar">
            <img src="{{ asset('images/smartquote-emblem-avatar.png') }}" />
        </div>

        <div class="account-block__text">
            <div class="welcome">WELCOME, {{ Auth::user()->full_name }}</div>
            <a href="{{ route('account.profile.edit') }}" style="font-size:14px;">My Account Settings</a>
            <br />
        </div>

    </div>
</div>