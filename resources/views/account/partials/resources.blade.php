<div class="well-white approve">
    <div class="homeheader ">RESOURCES</div>
    <a href="{{ route('knowledge.technologies') }}">3D Printing Technologies</a><br>
    <a href="{{ route('knowledge.materials') }}">3D Printing Materials</a><br>
    <a href="{{ route('faq.index') }}">Frequently Asked Questions</a><br>
    <a href="{{ route('knowledge.index') }}">Knowledge Center</a><br>
    <a href="https://www.youtube.com/studiofathom" target="_blank">Featured Videos</a>
</div>