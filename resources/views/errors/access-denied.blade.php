@extends('layouts.master')

@section('pageTitle', 'Access Denied')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">Access Denied</h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <div class="well-white">
                        {{ $reason }}
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection