<div class="row">
    <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 15px;">
        TECHNOLOGIES, MATERIALS &amp; SERVICES
    </div>
    <div class="span12">
        <table class="knowledgecenter">
            <tr class="top">
                <td>3D Printer Sales &amp; Services</td>
                <td>Quick guide on all services offered.</td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/2014/11/FATHOM-Overview-Flyer-2014.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>How 3D Printing Works</td>
                <td>Infographic overview of PolyJet.</td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_infographic_3dprinting.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>3D Printing Material Datasheet</td>
                <td>Chart includes PolyJet, FDM, SLS, and DMLS.</td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_Materials_Datasheet_2017.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>Model Finishing Services</td>
                <td>Sanding, priming, painting, and more.</td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_White_Paper_Model_Finishing.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>Injection Molding Services</td>
                <td>
                    Parts in days, not weeks or months &mdash; achieve better results
                    faster with an advanced additive manufacturing expert.
                </td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_Rapid_Tooling_and_Injection_Mold_Services_2014.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>Matte Vs Glossy (PolyJet)</td>
                <td>Guide on why to choose matte or glossy when ordering a PolyJet part.</td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/2015/02/Matte_vs_Glossy_0420141.pdf"
                       class="download"></a>
                </td>
            </tr>
        </table>
    </div>
</div>