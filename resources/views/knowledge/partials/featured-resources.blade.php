<section>
    <div class="row">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 15px;">
            FEATURED RESOURCES
        </div>
    </div>

    <div class="row">
        <div class="span4">
            <a href="{{ route('knowledge.materials') }}">
                <img src="{{ asset('images/Materials-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                Materials Overview
            </div>
            <span>
                From smooth surfaces to thin walls to complex geometries to rigid to flexible &mdash;
                a variety of material options are available on SmartQuote to meet your application needs.
            </span>
        </div>
        <div class="span4">
            <a href="{{ route('knowledge.technologies') }}">
                <img src="{{ asset('images/Technology-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                Technologies Overview
            </div>
            <span>
                An overview of 3D printing and additive manufacturing
                technologies available at FATHOM’s Oakland and Seattle advanced production centers.
            </span>
        </div>
        <div class="span4">
            <a href="http://studiofathom.com/challenge/">
                <img src="{{ asset('images/MTUC-Contest-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                Make the Unmakeable Contest
            </div>
            <span>
                FATHOM is challenging all designers and engineers to think differently about
                how they design and manufacture products today &mdash; enter to win a professional 3D printer!
            </span>
        </div>
    </div>

    <div class="row" style="margin-top: 40px;">
        <div class="span4">
            <a href="http://studiofathom.com/hashcast/">
                <img src="{{ asset('images/Hash-Cast-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                3D Printing Arm Casts
            </div>
            <span>
                #CAST “Hash Cast” is the new way to customize your broken
                arm cast with personal messages aggregated from your family and friends through social media.
            </span>
        </div>
        <div class="span4">
            <a href="http://studiofathom.com/michelangelo/">
                <img src="{{ asset('images/Michelangelo-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                Transforming Metal Casting
            </div>
            <span>
                San Francisco Bay Area companies FATHOM, Scansite, and Artwork Foundry
                are transforming the ancient art of metal casting using advanced
                3D tech to replicate statues by Michelangelo.
            </span>
        </div>
        <div class="span4">
            <a href="http://studiofathom.com/coolestcooler/">
                <img src="{{ asset('images/Coolest-Cooler-300px.jpg') }}" />
            </a>
            <div style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 15px; margin-top: 20px;">
                55-Part Complex Assembly
            </div>
            <span>
                After the Coolest Cooler campaign closed as the most funded of all-time on Kickstarter,
                the team had less than two weeks to prototype the refined design for TIME Magazine.
            </span>
        </div>
    </div>
</section>