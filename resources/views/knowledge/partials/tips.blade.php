<div class="row" style="margin-top: 0px; margin-bottom: 60px;">
    <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 20px;">
        TIPS &amp; TRICKS FOR USING SMARTQUOTE
    </div>
    <div class="span12" style="margin-left: 35px;">
        <ol class="knowledge_list" style="margin-left: 20px; margin-bottom: 0px;">
            <li>
                <strong>
                    When exporting STLs from Solidworks, Rhino, or other CAD software &mdash;
                </strong>
                <br />
                <span>
                    Adjust STL export settings for high quality to
                    ensure better accuracy and smoother curves.
                </span>
            </li>
            <li>
                <strong>
                    If you want to specify the orientation at which your part should be printed &mdash;
                </strong>
                <br />
                <span>
                    please include clear instructions in the comments box
                    and a SmartQuote specialist will follow-up with you to discuss.
                </span>
            </li>
            <li>
                <strong>
                    For best results, wall thicknesses, pegs, fins,
                    and other features should be greater than 1mm &mdash;
                </strong>
                <br />
                <span>
                    All features are dependent on geometry and subject to review by a SmartQuote specialist.
                </span>
            </li>
            <li>
                <strong>
                    Opting for sparse fill on FDM parts is ideal for reducing overall weight of part &mdash;
                </strong>
                <br />
                <span>
                    Be sure to include “sparse fill” in the comments section
                    and a SmartQuote specialist will follow-up.
                </span>
            </li>
            <li>
                <strong>
                    Any splits, voids or gaps should be larger than 0.6mm &mdash;
                </strong>
                <br />
                <span>
                    This will prevent material from fusing together during the 3D printing process.
                </span>
            </li>
            <li>
                <strong>
                    For best surface finish on small to medium sized parts, PolyJet is ideal &mdash;
                </strong>
                <br />
                <span>
                    For larger parts, or where surface finish is less
                    of a concern versus strength or durability, FDM is ideal.
                </span>
            </li>
            <li>
                <strong>
                    For parts larger than 200mm that require smooth surface finishes &mdash;
                </strong>
                <br />
                <span>
                    Stereolithography and CNC machining should both be considered.
                </span>
            </li>
        </ol>
    </div>
</div>