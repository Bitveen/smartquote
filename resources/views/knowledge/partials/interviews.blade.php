<div class="row">
    <div  class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 15px;">
        WHITE PAPERS &amp; INTERVIEWS
    </div>
    <div class="span12">
        <table class="knowledgecenter">
            <tr class="top">
                <td>From Rhino to STL</td>
                <td>
                    Optimize exporting &mdash; get high quality 3D prints
                    from your Rhino models with these guidelines
                </td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_rhinotostl_F001_2014.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>SolidWorks for Multi-Material</td>
                <td>
                    Guidelines and best practices for using
                    SolidWorks to generate multi-material 3D
                    printed parts &mdash; includes simulated overmolding.
                </td>
                <td>
                    <a href="http://studiofathom.com/wp-content/uploads/FATHOM_SolidWorks_MultiMaterial_F001_2014.pdf"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>Model Finishing for 3D Printing</td>
                <td>
                    A variety of post-processing techniques
                    can be applied to 3D printed parts &mdash; interview with FATHOM experts.
                </td>
                <td>
                    <a href="http://studiofathom.com/blog/3d-printing-computer-mouse-model-finishing/"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>3D Printing Assemblies with Moving Parts</td>
                <td>
                    FATHOM mechanical engineer Alexei Samimi
                    investigates 3D printing moving parts &mdash; comparison between FDM and PolyJet.
                </td>
                <td>
                    <a href="http://studiofathom.com/blog/3d-printing-assemblies-moving-parts/"
                       class="download"></a>
                </td>
            </tr>
            <tr>
                <td>Form, Fit, and Function Testing</td>
                <td>
                    Creators Atom, an innovative bicycle-based charging system,
                    explain how 3D printing helped validate concept, design, and functionality.
                </td>
                <td>
                    <a href="http://studiofathom.com/blog/3d-printing-for-form-fit-and-function/"
                       class="download"></a>
                </td>
            </tr>
        </table>
    </div>
</div>