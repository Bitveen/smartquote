@extends('layouts.master')

@section('pageTitle', 'Knowledge Center')

@section('meta')
    <meta name="title" content="Get Information on 3D Printing Materials and Technologies" />
    <meta name="description" content="Find information on 3D printing materials and technologies — lower costs, decrease time, and achieve the best possible finished product using SmartQuote." />
    <meta name="keywords" content="prototyping, 3d printing, 3d printing services, prototype fabrication, online, quoting and ordering, industrial design, engineering, flexible, high quality 3D prints, model finishing, additive manufacturing, Oakland, Seattle" />
@endsection

@section('content')
    <div class="container content">

        <div class="row">
            <div class="span12 hgray" style="margin-top: 40px; margin-bottom: 40px;">
                <div class="bluehead" style="padding-right: 20px;">KNOWLEDGE CENTER</div>
            </div>

            <div class="span12 text-center" style="margin-bottom: 0px;">
                <img src="{{ asset('images/Knowledge-Center-945px.jpg') }}" />
            </div>

            <div class="span12" style="font-family: 'Arial', sans-serif; font-size: 16px; line-height: 22px; color: black; margin-top: 40px; margin-bottom: 40px;">
                <span style="float: left; font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 22px; color: #00C1D5;">
                    Get Started on SmartQuote: &nbsp;
                </span>
                <span style="display: inline">
                    In the knowledge center, you will find information on 3D printing materials and technologies,
                    tips, tricks, and other how-to’s for additive manufacturing — lower costs, decrease time,
                    and achieve the best possible finished product on SmartQuote.
                </span>
                <p style="padding-top: 30px;">
                    FATHOM is more than just a 3D printing focused company.
                    From advanced prototype fabrication to bridge-to-production services,
                    FATHOM also offers CNC machining, injection molding, urethane casting, and 3D scanning.
                    Speak with a SmartQuote specialist to learn more.
                    Speak with a SmartQuote specialist to learn more about how
                    FATHOM is uniquely blending 3D printing with legacy manufacturing methods
                    so companies can go from concept to prototype to manufacturing
                    in a way that wasn’t previously possible.
                </p>
            </div>
        </div>

        @include('knowledge.partials.services')

        @include('knowledge.partials.interviews')

        <div class="row">
            <div class="hblue" style="margin-top: 40px; margin-bottom: 40px; margin-left: 19px; margin-right: 19px;"></div>
        </div>

        @include('knowledge.partials.featured-resources')

        <div class="row">
            <div class="hblue" style="margin-top: 80px; margin-bottom: 40px; margin-left: 19px; margin-right: 19px;"></div>
        </div>

        @include('knowledge.partials.tips')
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/knowledge.css') }}">
@endsection