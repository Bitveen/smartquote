@extends('layouts.master')

@section('pageTitle', 'Materials')

@section('meta')
    <meta name="title" content="3D Printing Material Options" />
    <meta name="description" content="Get details on properties and applications for 3D printed materials offered on SmartQuote, from rigid to flexible and low to high resolution." />
    <meta name="keywords" content="rapid prototypes, form modeling, aesthetic, resolution, surface finish, flexible parts, material properties, resin, thermoplastic, Shore A, ABS, sparse fill, PolyJet, FDM, SLS, Oakland, Seattle" />
@endsection

@section('content')
    <div class="container content">

        <div class="row">
            <div class="span12 hgray" style="margin-top: 40px; margin-bottom: 40px;">
                <div class="bluehead" style="padding-right: 20px;">ADDITIVE MANUFACTURING MATERIAL OPTIONS</div>
            </div>
        </div>

        @include('knowledge.materials.partials.polyjet')

        <div class="row">
            <div class="hblue" style="margin-top: 40px; margin-bottom: 40px; margin-left: 19px; margin-right: 19px;"></div>
        </div>

        @include('knowledge.materials.partials.fdm')

        <div class="row">
            <div class="hblue" style="margin-top:40px;margin-bottom:40px;margin-left:19px;margin-right:19px;">
            </div>
        </div>

        @include('knowledge.materials.partials.sls')

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/knowledge-materials.css') }}">
@endsection