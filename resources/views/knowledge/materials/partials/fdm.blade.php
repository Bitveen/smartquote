<section>
    <div class="row" style="margin-bottom: 30px;">
        <div class="span12 text-center">
            <img src="{{ asset('images/Material-Page-940x120-FDM.jpg') }}" />
        </div>
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-top: 40px; margin-bottom: 10px;">
            FDM MATERIALS
        </div>
        <div class="span12 bodytext">
            FATHOM’s production centers quickly and efficiently produce functional,
            detailed, accurate parts in production-grade thermoplastics.
            FDM is the technology of choice for functional testing,
            end-use parts, structural parts, jigs, production fixtures,
            large models, moving parts, assemblies, and more.
            <br/><br/>
            Orientation can be specified to optimize either strength or surface finish &mdash;
            numerous post-processing options are available.
            Sparse fill patterns can save time and reduce weight,
            while voids and other complex geometries are made
            possible using support material.
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">

        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">ABS: High durability</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color: </strong>White</li>
                <li>Medium strength</li>
                <li>Popular, common option</li>
                <li>Tough, hard, and rigid</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">ASA: High durability</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> White</li>
                <li>Medium strength</li>
                <li>Improved UV stabilization over ABS</li>
                <li>Ideal for large builds and general purpose functional models</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">PC/ABS: High strength and heat resistance</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black</li>
                <li>Strength and durability</li>
                <li>Very good heat resistance</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">PC: Superior mechanical properties</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> White</li>
                <li>High tensile and flexural strength</li>
                <li>Excellent heat resistance</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">Nylon: Flexibility and toughness</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black</li>
                <li>High impact and fatigue resistance </li>
                <li>Low friction</li>
                <li>Excellent for snap-fits and parts requiring some flexibility</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">Ultem 9085: Highest performance, FST-rated</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black or tan</li>
                <li>Extremely high strength to weight ratio</li>
                <li>Suitable for aerospace and automotive applications</li>
                <li>Heat deflection temperature of 307°F (153°C)</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">Ultem 1010: Ultimate resistance to heat and chemicals</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Tan</li>
                <li>Higher tensile strength, lower impact resistance than Ultem 9085</li>
                <li>Autoclave sterilizable, bio-compatible and food-safe</li>
                <li>Heat deflection temperature of 399°F (204°C)</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top:20px;margin-left:20px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:0px;">ABS-ESD7: Static-dissipative thermoplastic</div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black</li>
                <li>Static-dissipative variant of ABS</li>
                <li>Ideal for electronics assembly fixtures</li>
            </ul>
        </div>
    </div>
</section>