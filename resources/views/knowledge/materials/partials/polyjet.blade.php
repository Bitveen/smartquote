<section>
    <div class="row">
        <div class="span12 text-center">
            <img src="{{ asset('images/Material-Page-940x120-PolyJet.jpg') }}" />
        </div>
    </div>

    <div class="row" style="margin-top: 40px; margin-bottom: 30px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 10px;">
            POLYJET MATERIALS
        </div>
        <div class="span12 bodytext">
            The range of material options offered for PolyJet-based machines,
            combined with the excellent resolution and surface finish,
            makes PolyJet FATHOM’s most popular technology choice.
            For rapid prototyping, form modeling, aesthetic pieces,
            and anything requiring a high level of detail or accuracy,
            PolyJet is an ideal fit. Photopolymer resins are specially
            formulated to simulate common plastics or flexible parts.
            An array of digital materials are available, created through the blending of
            Vero rigid resin with Tango flexible resin, allowing for specifically
            tailored material properties along the spectrum from rigid to rubber-like.
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            Vero: Fully rigid and available in glossy or matte finishes
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black, white, or clear</li>
                <li>Easy to sand, prime, and paint</li>
                <li>Clear option is nearly translucent when sanded and clear-coated</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            TangoBlackPlus: Rubber-like plastic
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black</li>
                <li>Shore 27A durometer &mdash; mid-level elasticity </li>
                <li>Flexible glue can be used to join parts</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            TangoPlus: Flexible translucency
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Clear</li>
                <li>Very similar material properties to TangoBlackPlus</li>
                <li>Special custom setup material</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            DM Shore: Rubber-like digital materials with varying Shore A values
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Black</li>
                <li>Fully opaque</li>
                <li>Choose from 40A, 50A, 60A, 70A, 85A, and 95A Shore A values</li>
                <li>
                    Softest 40A is comparable to TangoBlack, while 95A
                    is the hardest, with the lowest elongation at break (35-45%)
                </li>
                <li>Tensile strength ranges from 1.3-10.0MPa</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            DM Grey: Rigid and durable digital materials for snap fits and parts needing slight flexibility
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Light to medium bluish-grey colors</li>
                <li>Choose from 20A, 25A, 35A, 40A, 50A, and 60A Shore A values</li>
                <li>Flexural strength ranges from 75MPa (Gray 20) to 35MPa (Gray 60)</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            ABS-Like: Closely simulate injection-molded ABS
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> White</li>
                <li>Most functional PolyJet material option with the highest heat deflection</li>
                <li>Features under 1.0mm do not reach optimal material properties</li>
                <li>Closely simulates the material properties of the ABS thermoplastic</li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 20px; margin-left: 20px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 0px;">
            Endur: Polypropylene-like plastic
        </div>
        <div class="span12 bodytext">
            <ul class="material_list">
                <li><strong>Color:</strong> Off-white</li>
                <li>Simulates appearance and functionality of polypropylenes</li>
                <li>Suitable for snap-fits, durable housings, and prototype packaging</li>
            </ul>
        </div>
    </div>
</section>