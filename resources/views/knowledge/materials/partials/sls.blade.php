<div class="row" style="margin-bottom: 30px;">
    <div class="span12 text-center">
        <img src="{{ asset('images/Material-Page-940x120-SLS.jpg') }}" />
    </div>
    <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:24px;line-height:32px; color:#00C1D5;margin-top:40px;margin-bottom:20px;">SLS MATERIAL </div>
    <div class="span12 bodytext">
        Nylon PA2200 is strong, tough, and dense,
        resulting in stiff but durable parts. It is excellent for parts
        as large as 280mm x 280mm, can handle extremely complex geometries,
        has very high accuracy and fine tolerances, and rarely shows layers.
        <br><br>
        SLS parts are bright white and can be dyed, and have an even
        surface finish approximately the texture of fine sandpaper.
        Excess powder used as support material must be physically
        removed in post-production, thus voids or hollows must be accessible.
    </div>
</div>