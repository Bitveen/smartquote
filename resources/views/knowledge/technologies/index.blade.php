@extends('layouts.master')

@section('pageTitle', 'Technologies')

@section('meta')
    <meta name="title" content="Overview of 3D Printing and Additive Manufacturing Technologies" />
    <meta name="description" content="See FATHOM’s guidelines and recommendations to help you choose the right 3D printing technology for your project or design." />
    <meta name="keywords" content="rapid prototypes, aesthetic prototypes, resolution, high accuracy, tolerances, surface finish, concept models, functional prototypes, end-use parts, flexible parts, material properties, resin, thermoplastic, Shore A, ABS, sparse fill, PolyJet, FDM, SLS, designers, engineers, additive manufacturing, wall thickness, minimum feature size, overmolding, model finishing, filament, Oakland, Seattle" />
@endsection

@section('content')
    <div class="container content">

        <div class="row">
            <div class="span12 hgray" style="margin-top: 40px; margin-bottom: 40px;">
                <div class="bluehead" style="padding-right: 20px;">OUR TECHNOLOGIES</div>
            </div>
        </div>

        @include('knowledge.technologies.partials.polyjet')

        <div class="row">
            <div class="hblue" style="margin-bottom: 40px; margin-left: 19px; margin-right: 19px;"></div>
        </div>

        @include('knowledge.technologies.partials.fdm')

        <div class="row">
            <div class="hblue" style="margin-bottom: 40px; margin-left: 19px; margin-right: 19px;"></div>
        </div>

        @include('knowledge.technologies.partials.sls')

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/knowledge-technologies.css') }}">
@endsection