<section>
    <div class="row">
        <div class="span12 text-center">
            <img src="{{ asset('images/Technology-Page-940x120-SLS.jpg') }}" />
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:24px;line-height:32px; color:#00C1D5;margin-bottom:10px;">
            SLS TECHNOLOGY OVERVIEW
        </div>
        <div class="span12 bodytext">
            Selective Laser Sintering is a popular choice for many applications.
            From prototypes to end-use parts, the nylon material is
            chosen by many designers and engineers when creating
            complex geometries that must meet high durability requirements.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>
                    <strong>Maximum Model Size: 280 × 280 × 280 mm (11 x 11 x 11 in.)</strong>
                    <br />
                    <span>Any dimensions exceeding 200mm (8 in.) require a manual quote.</span>
                </li>
                <li>
                    <strong>Minimum Feature Size: 1mm (0.04 in.)</strong>
                    <br />
                    <span>
                        This is the lowest recommended wall thickness or feature size;
                        quality and durability of smaller features is not guaranteed.
                    </span>
                </li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:10px;">
            How Does SLS Additive Manufacturing Work?
        </div>
        <div class="span12 bodytext">
            This additive manufacturing process makes use of a
            high-power optic laser that fuses small powder particles
            layer by layer &mdash; parts are surrounded by loose powder-like
            material during the build, which is removed in post-processing.
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px; margin-bottom: 80px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:20px;line-height:28px; color:#00C1D5;margin-bottom:10px;">
            What are the Benefits of SLS Additive Manufacturing?
        </div>
        <div class="span12 bodytext">
            This additive technology is an ideal option
            for many different types of applications &mdash; for functional parts,
            prototypes, prosthetics, and for manufacturing end-use parts like jigs and fixtures.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>Nylon PA2200 (polyamide white)</li>
                <li>Fine details, complex shapes, and smooth surfaces</li>
                <li>Multipurpose material, well balanced property profile</li>
                <li>High strength/stiffness, chemical resistance</li>
                <li>Ideal for long-term use, handles harsh application environments</li>
                <li>Suitable for model finishing (dyeing, sanding and painting)</li>
                <li>
                    Create
                    <a href="http://studiofathom.com/blog/3d-printing-assemblies-moving-parts/">moving assemblies</a>
                    in a single build
                </li>
                <li>And much more &mdash; speak with a SmartQuote specialist to find out if your project is a fit for SLS</li>
            </ul>
        </div>
    </div>

</section>