<section>
    <div class="row">
        <div class="span12 text-center">
            <img src="{{ asset('images/Technology-Page-940x120-FDM.jpg') }}" />
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:24px;line-height:32px; color:#00C1D5;margin-bottom:10px;">
            FDM TECHNOLOGY OVERVIEW
        </div>
        <div class="span12 bodytext">
            Build concept models, functional prototypes,
            and end-use parts using Stratasys FDM technology &mdash; a powerful,
            patented additive manufacturing method that uses engineering-grade
            thermoplastics with great mechanical, thermal,
            and chemical strength characteristics.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>
                    <strong>Maximum Model Size: 910 × 600 × 910 mm (36 x 24 x 36 in.)</strong>
                    <br />
                    <span>Any dimensions exceeding 200mm (8 in.) require a manual quote.</span>
                </li>
                <li>
                    <strong>Minimum Feature Size: 1mm (0.040 in.)</strong>
                    <br />
                    <span>
                        This is the smallest recommended wall thickness or feature size;
                        smaller features are geometry-dependent and must be manually reviewed.
                    </span>
                </li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12"  style="font-family: 'Varela Round', sans-serif; font-size:24px;line-height:32px; color:#00C1D5;margin-bottom:10px;">
            How Does FDM 3D Printing Work?
        </div>
        <div class="span12 bodytext">
            This additive manufacturing process (Fused Deposition Modeling)
            can be compared to a very tiny, very precise hot glue gun &mdash; these 3D
            printers heat and extrude thermoplastic filament in layers
            onto a build tray in a temperature-controlled chamber.
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px; margin-bottom: 80px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size:24px;line-height:32px; color:#00C1D5;margin-bottom:10px;">
            What are the Benefits of FDM 3D Printing?
        </div>
        <div class="span12 bodytext">
            This technology uses the same thermoplastics found in traditional
            manufacturing processes, with tolerances comparable to
            standard machining and strength up to 80% of injection molded plastics.
            FDM is a good fit for projects that demand dimensional stability,
            durability, and environmental performance.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>Production-grade thermoplastics</li>
                <li>Ideal for testing form, fit, and function</li>
                <li>Tough options for end-use parts and production fixtures</li>
                <li>Achieve complex geometries and cavities</li>
                <li>Black and white standard color options available</li>
                <li>Suitable for model finishing (sanding and painting)</li>
                <li>
                    Ideal for long-term use in harsh application environments,
                    with material options available for FST and biological
                    or chemical compatibility requirements (requires manual quote)
                </li>
                <li>
                    Create
                    <a href="http://studiofathom.com/blog/3d-printing-assemblies-moving-parts/">
                        moving assemblies
                    </a> in a single build
                </li>
                <li>And much more &mdash; speak with a SmartQuote specialist to find out if your project is a fit for FDM</li>
            </ul>
        </div>
    </div>
</section>