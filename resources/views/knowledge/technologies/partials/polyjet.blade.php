<section>
    <div class="row">
        <div class="span12 text-center">
            <img src="{{ asset('images/Technology-Page-940x120-PolyJet.jpg') }}" />
        </div>
    </div>
    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 24px; line-height: 32px; color: #00C1D5; margin-bottom: 10px;">
            POLYJET TECHNOLOGY OVERVIEW
        </div>
        <div class="span12 bodytext">
            PolyJet Technology is a powerful
            additive manufacturing method that produces smooth,
            accurate prototypes, for form, fit and function testing.
            With 16- to 30-micron layer resolution and accuracy as high as 0.1 mm,
            designers and engineers can create 3D printed parts
            with thin walls and complex geometries.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>
                    <strong>Maximum Model Size: 490 × 390 × 200 mm (19.3 x 15.35 x 7.9 in.)</strong>
                    <br />
                    <span>Any dimensions exceeding 200mm (8 in.) require a manual quote.</span>
                </li>
                <li>
                    <strong>Minimum Feature Size: 1 mm (0.040 in.)</strong>
                    <br />
                    <span>
                        The lowest recommended wall thickness or feature size is 1 mm (0.040 in.)
                        Smaller features may not print correctly; accuracy is not guaranteed.
                        Some smaller or thinner features may be possible;
                        <a href="mailto:SQHelp@studiofathom.com">contact us</a>
                        to review manufacturability of a design.
                    </span>
                </li>
            </ul>
        </div>
    </div>

    <div class="row" style="margin-top: 0px; margin-top: 40px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 10px;">
            How Does PolyJet 3D Printing Work?
        </div>
        <div class="span12 bodytext">
            This additive manufacturing process is very similar to inkjet printing &mdash; just
            like ink droplets jetted onto paper by a moving printer head,
            these 3D printers jet UV-curable liquid photopolymer
            drops in layers onto a build tray.
        </div>
    </div>

    <div class="row" style="margin-top:0px;margin-top:40px;margin-bottom:80px;">
        <div class="span12" style="font-family: 'Varela Round', sans-serif; font-size: 20px; line-height: 28px; color: #00C1D5; margin-bottom: 10px;">
            What are the Benefits of PolyJet 3D Printing?
        </div>
        <div class="span12 bodytext">
            This additive technology is a great option for many applications,
            from prototypes to end-use parts and even tooling.
            PolyJet is the technology of choice when high accuracy
            and resolution is needed, perfect for smaller aesthetic
            prototypes as well as any model requiring clear,
            colored, or flexible materials.
        </div>
        <div class="span12 bodytext">
            <ul class="technology_list">
                <li>High-quality models that look like the final product</li>
                <li>Fine details, complex shapes, and smooth surfaces</li>
                <li>Many material options, from flexible to rigid</li>
                <li>
                    Clear, black, white, and grays are standard rigid material options,
                    with more colors available (requires manual quote)
                </li>
                <li>
                    Standard color option for flexible materials is black,
                    with clear and colored flexible materials available to be quoted manually
                </li>
                <li>Choose from different Shore A scale durometers with flexible Shore A Digital Materials</li>
                <li>Create tough snap-fit parts with ABS-Like and Gray Digital Materials</li>
                <li>Great for model finishing (sanding and painting)</li>
                <li>Multi-material printing and simulated overmolding available (requires manual quote)</li>
                <li>
                    Create
                    <a href="http://studiofathom.com/blog/3d-printing-assemblies-moving-parts/">moving assemblies</a>
                    in a single build
                </li>
                <li>And much more &mdash; contact a SmartQuote specialist for more information</li>
            </ul>
        </div>
    </div>

</section>