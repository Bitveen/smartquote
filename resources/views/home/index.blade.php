@extends('layouts.master')

@section('pageTitle', 'Home')

@section('content')
    @include('layouts.promo-banner')

    <div class="container content">

        <div class="row">
            <div class="span12">
                <div class="well-white">
                    <div class="landheader">
                        UPLOAD FILES. INSTANT QUOTES. QUICK TURNAROUND.
                    </div>
                    <div class="landtext">
                        SmartQuote is a specialized online platform that provides
                        users with quick and easy access to 3D printing services
                        for prototyping and low volume production.
                        FATHOM utilizes the most advanced technologies
                        and materials to help companies innovate
                        new products faster and more efficiently.
                    </div>
                    <div class="span11">
                        <fieldset>
                            <div class="clearfix" style="text-align:center;">
                                @if(Auth::check())
                                    <a href="{{ route('quotes.create') }}" id="reg_btn" class="btn btn-custom btn-center">
                                        START A QUOTE
                                    </a>
                                @else
                                    <a href="{{ route('registration.create') }}" id="reg_btn" class="btn btn-custom btn-pull">REGISTER</a>
                                    <a href="{{ route('sessions.create') }}" id="login_btn" class="btn btn-custom btn-pull">LOGIN</a>
                                @endif
                            </div>
                        </fieldset>
                    </div>
                    <div style="clear:both;line-height:1px;">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span12 hgray">
                <div class="bluehead" style="padding-right:20px;">3D PRINTING</div>
            </div>

            <div class="span4">
                <div class="printhead">PolyJet: Rigid or Rubber-Like</div>
                <div class="printsub">Fine Details &amp; Smooth Surfaces</div>
                Current lead time: 2-3 business days<br>
                Parts will ship by {{ $dateAfterFive->format('M d') }} if ordered by:<br>4 pm PST on {{ $now->format('M d') }}
                <br>
            </div>
            <div class="span4">
                <div class="printhead">FDM: ABS &amp; PC Plastics</div>
                <div class="printsub">Engineering Grade Thermoplastics</div>
                Current lead time: 4 business days<br>
                Parts will ship {{ $dateAfterSix->format('M d') }} if ordered by:<br>4 pm PST on {{ $now->format('M d') }}
                <br>
            </div>
            <div class="span4">
                <div class="printhead">SLS: Nylon PA2200</div>
                <div class="printsub">Durable &amp; Complex Geometries</div>
                Current lead time: 2-4 business days<br>
                Parts will ship {{ $dateAfterSix->format('M d') }} if ordered by:<br>4 pm PST on {{ $now->format('M d') }}
                <br>
            </div>
        </div>

        <div class="row">
            <div class="span12 hgray">
                <div class="bluehead" style="padding-right:20px;">ADVANCED SERVICES &amp; EXPEDITE OPTIONS</div>
            </div>
            <div class="span12">
                <div class="bodytext">
                    Upload files to SmartQuote for advanced prototyping
                    or low-volume production services, including
                    <strong>stereolithography</strong>, <strong>CNC machining,</strong>
                    and <strong>urethane casting</strong>.
                    Log in and upload files to request an
                    <strong>expedited lead time</strong> for any quote.
                    FATHOM services also include rapid tooling and injection molding,
                    3D printed injection molding, model finishing, and assembly
                    <a href="http://studiofathom.com/wp-content/uploads/2014/11/FATHOM-Overview-Flyer-2014.pdf"
                       target="_blank">&rarr; Learn More</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="hblue" style="margin-top:40px;margin-bottom:40px;margin-left:19px;margin-right:19px;">
            </div>
        </div>

        <div class="row" style="margin-bottom:20px;">
            <div class="span4">
                <div class="bluehead">
                    VIDEO TUTORIAL
                </div>
                <a href="//www.youtube.com/embed/ueNA2iwLYyE" target="_blank">
                    <div class="your-img">
                        <div class="your-hover"></div>
                    </div>
                </a>
                <div class="bodytext" style="margin-top:20px;">
                    Start getting rapid prototyping quotes in just a few quick steps.
                    Watch the video to learn how to use our SmartQuote online quoting and ordering system.
                </div>
            </div>
            <div class="span4">
                <div class="bluehead">
                    MODEL FINISHING
                </div>
                <a href="http://studiofathom.com/wp-content/uploads/FATHOM_White_Paper_Model_Finishing.pdf"
                   target="_blank">
                    <img width="100%" src="{{ asset('images/SmartQuote_landing_model_finishing.png') }}" />
                </a>
                <div class="bodytext" style="margin-top:20px;">
                    Create truly unique, showroom-quality pieces with finishing options starting at $125,
                    including sanding, painting, polishing and Pantone color matching.
                </div>
            </div>
            <div class="span4">
                <div class="bluehead">
                    CHANGE IS ADDITIVE
                </div>
                <a href="http://studiofathom.com/tag/change-is-additive/" target="_blank">
                    <img width="100%" src="{{ asset('images/SmartQuote_landing_WRD.png') }}" />
                </a>
                <div class="bodytext" style="margin-top:20px;">
                    To cut through the clutter of 3D printing news,
                    check out these FATHOM favorites of the week.
                </div>
            </div>
        </div>

    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endsection