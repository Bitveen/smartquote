<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    <tbody>
    <tr>
        <td>
            <table align="center" width="600" cellpadding="25" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td bgcolor="#FFFFFF">
                        <a href="http://studiofathom.com/" target="_blank">
                            <img src="http://studiofathom.com/wp-content/uploads/2013/06/fathom_logo_150x100.png" width="150" height="100" border="0" alt="studiofathom.com" />
                        </a>
                        <br />
                        <br />
                        <hr style="color:#CCCCCC; height:1px; background:#CCCCCC; border:0;">
                        <p>
                            <br />
                            <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; color:#333333; font-weight: lighter; font-style:normal; line-height:1.5em;">
                                Hi {{ $user->first_name }} {{ $user->last_name }},
                                <br />
                                <br />
                                The password to your SmartQuote account
                                with FATHOM has been reset. In order to login to your account,
                                you will need to set a new password
                                <a href="http://localhost:8000/auth/password/reset/{{ $token }}">here</a>.
                            </span>
                        </p>
                        <p>
                            <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; color:#333333; font-weight: lighter; font-style:normal; line-height:1.5em;">
                                If you received this email in error and did not request a password reset,
                                please contact <a href="mailto:SQHelp@studiofathom.com">SQHelp@studiofathom.com</a>
                            </span>.
                        </p>
                        <p>
                            <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; color:#333333; font-weight: lighter; font-style:normal; line-height:1.5em;">
                                &mdash; The FATHOM Team
                                <br />
                                Oakland, CA &amp; Seattle, WA // 510.281.9000 //
                                <a href="http://studiofathom.com/" target="_blank">
                                    <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; font-style:normal; line-height:1.5em; text-decoration:underline">
                                        studiofathom.com
                                    </span>
                                </a>
                            </span>
                            <br />
                            <br />
                        </p>
                        <hr style="color:#CCCCCC; height:1px; background:#CCCCCC; border:0;">
                        <br />
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size: 10px; color:#999999; font-weight: lighter; font-style:normal; line-height:1.3em;">
                            &copy; {{ $now->year }} FATHOM &ndash; 3D Printer Sales, Rapid Prototyping, Design
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>