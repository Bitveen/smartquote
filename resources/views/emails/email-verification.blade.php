<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    <tbody>
    <tr>
        <td>
            <table align="center" width="600" cellpadding="25" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td bgcolor="#FFFFFF">
                        <a href="http://studiofathom.com/" target="_blank">
                            <img src="http://studiofathom.com/wp-content/uploads/2013/06/fathom_logo_150x100.png" width="150" height="100" border="0" alt="studiofathom.com" />
                        </a>
                        <br />
                        <br />
                        <hr style="color:#CCCCCC; height:1px; background:#CCCCCC; border:0;">
                        <p>
                            <br />
                            <span style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; color:#333333; font-weight: lighter; font-style: normal; line-height: 1.5em;">
                                Hi {{ $user->first_name }} {{ $user->last_name }},
                                <br />
                                <br />
                                Please click on the link below to verify your email.
                            </span>
                        </p>
                        <p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; color: #333333">
                            <a href="http://localhost:8000/auth/email/verify/{{ $token }}">Verify email</a>
                        </p>
                        <p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; color: #333333">
                            If you received this email and did not request an email verification key,
                            please disregard this email.
                        </p>
                        <p style="font-family: Helvetica, Arial, sans-serif; font-size: 16px; color: #333333">
                            <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; color:#333333; font-weight: lighter; font-style:normal; line-height:1.5em;">
                                <br />
                                &mdash; The FATHOM Team
                                <br />
                                Oakland, CA &amp; Seattle, WA // 510.281.9000 //
                                <a href="http://studiofathom.com/" target="_blank">
                                    <span style="font-family:Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; font-style:normal; line-height:1.5em; text-decoration:underline">
                                        studiofathom.com
                                    </span>
                                </a>
                            </span>
                            <br />
                            <br />
                        </p>
                        <hr style="color:#CCCCCC; height:1px; background:#CCCCCC; border:0;">
                        <br />
                        <span style="font-family:Arial, Helvetica, sans-serif; font-size: 10px; color:#999999; font-weight: lighter; font-style:normal; line-height:1.3em;">
                            &copy; {{ $now->year }} FATHOM &ndash; 3D Printer Sales, Rapid Prototyping, Design
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
