@extends('layouts.master')

@section('pageTitle', 'Frequently Asked Questions')

@section('meta')
    <meta name="title" content="Get Help Using SmartQuote" />
    <meta name="description" content="Common questions and answers on 3D printing materials and technologies, preparing and uploading CAD files, lead times and low volume production." />
    <meta name="keywords" content="prototyping, 3d printing, 3d printing services, prototype fabrication, online, quoting and ordering, industrial design, engineering, urethane casting, RTV, silicone molding, CNC machining, Oakland, Seattle" />
@endsection

@section('content')
    <div class="body_container">
        <div class="container glow" style="margin-top: 30px;">
            <h1 class="main-header">FREQUENTLY ASKED QUESTIONS</h1>
        </div>
        <div class="container content" style="min-height:100%; margin-top: 30px">
            <div class="row">
                <div class="span12">
                    <div class="well-white">
                        <div class="text-center">
                            <img src="{{ asset('images/FATHOM-Making-the-Unmakeable.jpg') }}" />
                        </div>
                        <article>
                            <h3 class="first">WHAT IS SMARTQUOTE?</h3>
                            <p>
                                SmartQuote is a specialized online platform that provides
                                users with quick and easy access to 3D printing services
                                for prototyping and low volume production.
                                FATHOM utilizes the most advanced technologies
                                and materials to help companies innovate new
                                products faster and more efficiently.
                                Create an account or log in using username and password:
                            </p>
                            <ul style="list-style: decimal;">
                                <li>Upload CAD files then choose technology and material type</li>
                                <li>Review pricing and lead-time options</li>
                                <li>Choose shipping, enter billing information, and place order</li>
                            </ul>
                            <span>…sit back and relax. The FATHOM team is on it!</span>
                        </article>

                        <article>
                            <h3>WHAT FILES CAN I UPLOAD TO SMARTQUOTE?</h3>
                            <p>
                                SmartQuote accepts STL, 3DM, 3DS, MODEL, EXP, DLV,
                                DLV3, CATPART, IGES, IGS, SLD, SLDPRT, STEP, STP, X_T,
                                PRT, PLY, ZCP, VDA, WRL, VRML, SKP, SAT, JT, DXF, FBX,
                                DAE, ZPR and OBJ files. (Up to 100 MBs.)
                            </p>
                        </article>

                        <article>
                            <h3>WHAT TECHNOLOGIES ARE AVAILABLE ON SMARTQUOTE?</h3>
                            <p>
                                SmartQuote options include Fused Deposition Modeling (FDM),
                                PolyJet, and Selective Laser Sintering (SLS) technologies.
                            </p>
                        </article>

                        <article>
                            <h3>WHAT MATERIALS ARE AVAILABLE ON SMARTQUOTE?</h3>
                            <p>
                                FDM Materials Include:
                            </p>
                            <ul>
                                <li>
                                    ABS, PC/ABS and PC. The only material available for our
                                    SLS technology is PA2200, a Nylon material.
                                </li>
                            </ul>
                            <p>Single PolyJet Materials Include:</p>
                            <ul>
                                <li>
                                    VeroBlack, VeroClear, VeroWhite, TangoBlack+,
                                    ABS-Like, Digital Materials 40A through 95A and
                                    Grey Digital Materials 20 through 60
                                </li>
                            </ul>
                        </article>

                        <article>
                            <h3>CAN'T FIND THE MATERIAL YOU ARE LOOKING FOR?</h3>
                            <p>
                                FATHOM has a wide range of additional materials available through our
                                <a href="http://studiofathom.com/get-a-quote/manual-quote/">
                                    manual quoting system
                                </a>.
                                Review all material options available at FATHOM 3D printing production centers:
                                <a href="http://studiofathom.com/knowledge-center/materials/">
                                    http://studiofathom.com/knowledge-center/materials/
                                </a>
                            </p>
                        </article>

                        <article>
                            <h3>WHAT ARE LEAD-TIMES PER TECHNOLOGIES ON SMARTQUOTE?</h3>
                            <p>
                                Lead times differ depending on which process is selected.
                            </p>
                            <ul>
                                <li>PolyJet: 2 to 3 Business Days + Shipping</li>
                                <li>FDM: 4 Business Days + Shipping</li>
                                <li>SLS: 2 to 4 Business Days + Shipping</li>
                            </ul>
                        </article>

                        <article>
                            <h3>WHAT OTHER SERVICES ARE OFFERED BY FATHOM?</h3>
                            <p>
                                FATHOM offers a wide variety of services to fit
                                your advanced prototype fabrication and low
                                volume manufacturing needs. This complete offering
                                is supported by a dynamic in-house industrial
                                design and mechatronic engineering focused team.
                                We're using our expertise in serious digital
                                manufacturing to help companies put satellites into orbit,
                                electric cars on freeways, and a full spectrum
                                of devices into people’s hands and homes.
                            </p>
                            <ul>
                                <li>
                                    Our product portfolio includes professional 3D printers
                                    and manufacturing systems
                                </li>
                                <li>
                                    Our services include 3D printing, rapid tooling and injection molding,
                                    3D printed injection molding, RTV / silicone molding / urethane casting,
                                    CNC machining, model assembly, and finishing
                                </li>
                            </ul>
                        </article>

                        <article>
                            <h3>HOW CAN I GET IN TOUCH WITH AN ACCOUNT MANAGER?</h3>
                            <p>
                                FATHOM account managers are extremely knowledgeable
                                about our advanced prototype fabrication and low
                                volume manufacturing needs for a variety of industries,
                                from electronics and consumer products to medical and aerospace.
                            </p>
                            <ul>
                                <li>
                                    I’d like to work with an account manager directly &mdash;
                                    <a href="http://studiofathom.com/get-a-quote/manual-quote/">
                                        Get a Manual Quote
                                    </a>
                                </li>
                                <li>
                                    Call us directly — Oakland 510.281.9000 or Seattle 206.582.1062
                                </li>
                            </ul>
                        </article>

                        <article>
                            <h3>DOES FATHOM SELL 3D PRINTERS?</h3>
                            <p>
                                FATHOM sells a wide variety of professional
                                3D printers and manufacturing systems from Stratasys.
                            </p>
                            <ul>
                                <li>
                                    I’d like to purchase a Stratasys 3D Printing System &mdash;
                                    <a href="http://studiofathom.com/get-a-quote/manual-quote/">
                                        Get a Manual Quote
                                    </a>
                                </li>
                            </ul>
                            <i>
                                “Our Objet 3D printer has been invaluable in
                                the development of Beam and other products.
                                It allows us to quickly and conveniently create
                                functional prototypes and mock-ups in-house at
                                a far lower cost than if we used outside vendors.”
                                &mdash; Stephanie Lee, Mechanical Engineer at Suitable Technologies
                            </i>
                        </article>

                        <article>
                            <h3>I NEED HELP USING SMARTQUOTE &mdash; WHO CAN I CONTACT?</h3>
                            <p>
                                If you would like some assistance with SmartQuote, please contact
                                <a href="mailto:SQhelp@studiofathom.com">
                                    SQhelp@studiofathom.com
                                </a>
                                or call (510) 281-9000, ext.111.
                            </p>
                        </article>

                        <article>
                            <h3>I DID NOT GET AN INVOICE COPY OF MY ORDER &mdash; WHO DO I CONTACT?</h3>
                            <p>
                                If you need copy of your invoice sent to you, please contact
                                <a href="mailto:SQhelp@studiofathom.com">
                                    SQhelp@studiofathom.com
                                </a>
                                or call (510) 281-9000, ext.111.
                            </p>
                        </article>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/faq.css') }}">
@endsection