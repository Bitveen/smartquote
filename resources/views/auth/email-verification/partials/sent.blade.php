<div class="well-white">
    <h1>Your email verification has been sent!</h1>
    <p>
        If you do not receive an email,
        please check your spam folder or contact us at
        <a href="emailto:SQHelp@studiofathom.com">SQHelp@studiofathom.com</a>
    </p>
</div>