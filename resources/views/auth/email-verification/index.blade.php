@extends('layouts.master')

@section('pageTitle', 'Resend Email Verification')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">Resend Email Verification</h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <br />
                    @if(session('email-sent'))
                        @include('auth.email-verification.partials.sent')
                    @else
                        @include('auth.email-verification.partials.form')
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection