@extends('layouts.master')

@section('pageTitle', 'Email Verification')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">Email Verification</h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <div class="well-white">
                        @if(session('verification-failed'))
                            <h2>This is an invalid key!</h2>
                            <p>
                                Click
                                <a href="{{ route('email-verification.create') }}">here</a>
                                to resend your email verification.
                            </p>
                        @else
                            <h2>Your email has been verified!</h2>
                            <p>
                                Please
                                <a href="{{ route('sessions.create') }}">log in</a>
                                and make your first quote!
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection