<div class="well-white">
    <h3>Fast, Simple &amp; Automated</h3>
    <p>
        SmartQuote by FATHOM is a specialized online platform
        that provides users with quick and easy access to 3D printing
        services for prototyping and low volume production.
        FATHOM utilizes the most advanced technologies and materials to help
        companies innovate new products faster and more efficiently.
    </p>
    <h3>Always Working &mdash;<br>So You Don&rsquo;t Have To</h3>
    <p>
        FATHOM works 24/7 to ensure that your parts are ready when you need them.
        Sit back, relax, and be confident deadlines will
        be met.
    </p>
    <h3>About FATHOM</h3>
    <p>
        FATHOM’s product portfolio includes professional
        3D printers and manufacturing systems. Services focus on advanced
        prototype fabrication and low volume production
        by way of advanced manufacturing techniques &mdash; supported by a dynamic
        in-house industrial design and mechatronic engineering focused team.
    </p>
</div>