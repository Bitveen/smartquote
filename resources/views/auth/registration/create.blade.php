@extends('layouts.master')

@section('pageTitle', 'Register')

@section('meta')
    <meta name="title" content="Join SmartQuote to Quote and Order 3D Prints from Your 3D CAD Files" />
    <meta name="description" content="Join SmartQuote for access to fast, simple, automated quoting and ordering of 3D prints for engineers and designers. " />
    <meta name="keywords" content=" prototyping, 3d printing services, rapid prototyping, online, quoting and ordering, industrial design, engineering, Oakland, Seattle" />
@endsection

@section('content')
    <main>
        @include('layouts.promo-banner')
        <div class="container content">
            <div class="row">

                <div class="span6">
                    <br />
                    @include('auth.partials.info')
                    <br />
                    @include('auth.partials.promo')
                </div>

                <div class="span6">
                    <div class="login-form-toggle">
                        <a href="{{ route('sessions.create') }}">Already an existing user? Login here!</a>
                    </div>
                    @include('auth.registration.partials.form')
                </div>

            </div>
        </div>
    </main>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/registration.js') }}"></script>
@endsection