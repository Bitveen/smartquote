<form id="register-form" action="{{ route('registration.store') }}" class="registration-form well-white form-horizontal" method="POST">

    {{ csrf_field() }}

    <fieldset>
        <legend>User Account Information:</legend>

        @if($errors->any())
            @foreach($errors->all() as $message)
                <div class="alert alert-block alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <p>
                        {{ $message }}
                    </p>
                </div>
            @endforeach
        @endif

        <div class="control-group">
            <label class="control-label" for="first_name">First Name*</label>
            <div class="controls">
                <input autocomplete="off" type="text" id="first_name" name="first_name">
                <span class="help-inline" style="display: none">Please enter a first name</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="last_name">Last Name*</label>
            <div class="controls">
                <input autocomplete="off" type="text" name="last_name" id="last_name">
                <span class="help-inline" style="display: none">Please enter a last name</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="email">Email Address*</label>
            <div class="controls">
                <input autocomplete="off" type="text" name="email" id="email">
                <span class="help-inline" style="display: none">Please enter a valid email address</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="email_confirmation">Confirm Email Address*</label>
            <div class="controls">
                <input autocomplete="off" type="text" name="email_confirmation" id="email_confirmation">
                <span class="help-inline" style="display: none">Your email address must match</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password">Password*</label>
            <div class="controls">
                <input autocomplete="off" type="password" name="password" id="password">
                <span class="help-inline" style="display: none">Please enter a password</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password_confirmation">Confirm Password*</label>
            <div class="controls">
                <input autocomplete="off" type="password" name="password_confirmation" id="password_confirmation">
                <span class="help-inline" style="display: none">Your passwords must match</span>
            </div>
        </div>

    </fieldset>

    <div class="control-group">
        <label class="control-label" for="company">Company*</label>
        <div class="controls">
            <input autocomplete="off" type="text" name="company" id="company">
            <span class="help-inline" style="display: none">Please enter your Company name</span>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="phone_number">Phone Number*</label>
        <div class="controls">
            <input autocomplete="off" type="text" name="phone_number" id="phone_number">
            <span class="help-inline" style="display: none">Please enter a valid phone number</span>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="zip_code">Zip Code</label>
        <div class="controls">
            <input autocomplete="off" type="text" name="zip_code" id="zip_code">
            <span class="help-inline" style="display: none">Please enter a valid zip code</span>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="know_from">How did you hear about us:</label>
        <div class="controls">
            <select name="know_from" id="know_from">
                <option value="">Choose...</option>
                <option value="fathom website">FATHOM Website</option>
                <option value="fathom employee">FATHOM Employee</option>
                <option value="tradeshow">Trade Show</option>
                <option value="newsletter">Newsletter</option>
                <option value="online search">Online Search</option>
                <option value="google advertisement">Google Advertisement</option>
                <option value="website banner">Website Banner</option>
                <option value="social media">Social Media</option>
                <option value="referral">Referral</option>
                <option value="other">Other</option>
            </select>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <label class="checkbox">
                <input type="checkbox" name="is_subscribed" checked="checked">Email me updates from FATHOM
            </label>
        </div>
    </div>

    <fieldset>
        <div class="control-group">
            <div class="controls">
                <label class="checkbox">
                    <input type="checkbox" name="is_terms_agree" id="is_terms_agree">
                    I agree to the
                    <a target="_blank" href="{{ route('terms-of-service.index') }}">
                        Terms of Service
                    </a>
                </label>
                <span class="help-inline" style="display: none">
                    You must agree to the Terms of Service to continue
                </span>
            </div>
        </div>
    </fieldset>

    <div class="form-actions">
        <button id="register_btn" class="btn btn-large btn-custom">
            Create Account
        </button>
    </div>
</form>