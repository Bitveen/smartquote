@extends('layouts.master')

@section('pageTitle', 'Account has been registered')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">Account Registered!</h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <br />
                    <div class="well-white">
                        <h2>Please verify your email!</h2>
                        <p>
                            A verification email has been sent to your email.
                            Please verify your email using the link included.
                        </p>
                        <p>
                            If you do not receive an email, please check your spam
                            folder or contact us at
                            <a href="emailto:SQHelp@studiofathom.com">SQHelp@studiofathom.com</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection