@extends('layouts.master')

@section('pageTitle', 'Login')

@section('content')
    <main>
        @include('layouts.promo-banner')

        <div class="container content">
            <div class="row">

                <div class="span6">
                    <br />
                    @include('auth.partials.info')
                </div>

                <div class="span6">
                    <div style="text-align: right">
                        <a href="{{ route('registration.create') }}">Don't have an account? Sign up now!</a>
                    </div>
                    <form action="{{ route('sessions.store') }}" class="login-form well-white form-horizontal" method="POST">

                        {{ csrf_field() }}

                        <fieldset>
                            <h4>Please Login:</h4>

                            @if(session('verify-email-needed'))
                                <div class="alert alert-error">
                                    Sorry, but you need to verify your email before you can use SmartQuote.
                                    If you need the email verification resent, please click
                                    <a href="{{ route('email-verification.create') }}">here</a>
                                </div>
                            @endif

                            @if(session('password-reset-success'))
                                <div class="alert alert-success">
                                    Your password has been reset.
                                </div>
                            @endif

                            @if(session('invalid-credentials'))
                                <div class="alert alert-block alert-error">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <p>
                                        Invalid credentials.
                                    </p>
                                </div>
                            @endif

                            @if($errors->any())
                                @foreach($errors->all() as $message)
                                    <div class="alert alert-block alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <p>
                                            {{ $message }}
                                        </p>
                                    </div>
                                @endforeach
                            @endif

                            <div class="control-group">
                                <label class="control-label">Email:</label>
                                <div class="controls">
                                    <input type="text" name="email" value="" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Password:</label>
                                <div class="controls">
                                    <input type="password" name="password" value="">
                                    <span class="help-block">
                                        <a href="{{ route('password-reset.create') }}" class="small">
                                            Forgot your password?
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </fieldset>

                        <div class="clearfix">
                            <button id="login_btn" class="btn btn-custom pull-right">Sign In</button>
                        </div>
                    </form>
                    @include('auth.partials.promo')
                </div>

            </div>
        </div>
    </main>
@endsection