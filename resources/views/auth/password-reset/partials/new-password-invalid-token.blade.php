<div class="well-white">
    <h2>This is an invalid key!</h2>
    <p>
        Click
        <a href="{{ route('password-reset.create') }}">here</a>
        to resend your password reset email.
    </p>
</div>