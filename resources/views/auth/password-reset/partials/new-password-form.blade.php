<div class="well-white">
    <h2>Please enter your new password.</h2>

    @if(session('password-mismatch'))
        <p style="margin: 15px 0">
            <span class="alert alert-error">Passwords must match</span>
        </p>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $message)
            <div class="alert alert-block alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <p>
                    {{ $message }}
                </p>
            </div>
        @endforeach
    @endif

    <form action="{{ route('password-reset.update', [ 'token' => $token ]) }}" method="POST">

        {{ csrf_field() }}

        <div class="control-group">
            <label class="control-label" for="password">Password</label>
            <div class="controls">
                <input type="password" id="password" name="password">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password_confirmation">Re-enter Password</label>
            <div class="controls">
                <input type="password" id="password_confirmation" name="password_confirmation">
            </div>
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-custom">Reset password</button>
        </div>

    </form>
</div>