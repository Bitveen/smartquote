<div class="well-white">
    <h1>Your password has been reset!</h1>
    <p>
        Your password has been reset,
        please check your email for instructions
        on how to reactivate your account.
    </p>
    <p>
        If you do not receive an email, please check your
        spam folder or contact us at
        <a href="emailto:SQHelp@studiofathom.com">SQHelp@studiofathom.com</a>
    </p>
</div>