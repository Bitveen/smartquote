<div class="well-white">
    <h2>Please enter your email address &mdash; Thank you!</h2>

    @if(session('user-not-found'))
        <p style="margin: 15px 0">
            <span class="alert alert-error">
                Sorry, that email was not found, you may register an account
                <a href="{{ route('registration.create') }}">here</a>
            </span>
        </p>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $message)
            <div class="alert alert-block alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <p>
                    {{ $message }}
                </p>
            </div>
        @endforeach
    @endif

    <form action="{{ route('password-reset.store') }}" method="POST">

        {{ csrf_field() }}

        <div class="control-group">
            <label class="control-label" for="email">Email</label>
            <div class="controls">
                <input type="text" id="email" name="email" />
            </div>
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-custom">Reset password</button>
        </div>

    </form>
</div>