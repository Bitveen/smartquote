@extends('layouts.master')

@section('pageTitle', 'Reset your password')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">
                @if(session('email-sent'))
                    Password Reset
                @else
                    Reset Your Password
                @endif
            </h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <br />
                    @if(session('email-sent'))
                        @include('auth.password-reset.partials.sent')
                    @else
                        @include('auth.password-reset.partials.form')
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection