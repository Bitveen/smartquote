@extends('layouts.master')

@section('pageTitle', 'Reset your password')

@section('content')
    <main>
        <div class="container glow" style="margin-top: 30px; text-transform: uppercase">
            <h1 class="main-header">Reset Your Password</h1>
        </div>
        <div class="container content">
            <div class="row">
                <div class="span12">
                    <br />
                    @if(session('invalid-reset-token'))
                        @include('auth.password-reset.partials.new-password-invalid-token')
                    @else
                        @include('auth.password-reset.partials.new-password-form')
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection