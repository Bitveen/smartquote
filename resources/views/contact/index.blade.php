@extends('layouts.master')

@section('pageTitle', 'Start a Conversation')

@section('meta')
    <meta name="title" content="Start a Conversation with an Additive Manufacturing Expert" />
    <meta name="description" content="Contact FATHOM SmartQuote Representatives via email, phone or in person." />
    <meta name="keywords" content="prototyping, low volume production, additive, advanced manufacturing, Oakland, Seattle, production" />
@endsection

@section('content')
    <div class="body_container">
        <div class="container glow" style="margin-top: 30px;">
            <h1 class="main-header">START A CONVERSATION</h1>
        </div>
        <div class="container content" style="min-height: 100%; margin-top: 30px;">
            <div class="row">
                <div class="span8">
                    <div class="well-white">
                        <h3>Thanks for choosing SmartQuote!</h3>
                        <p>
                            <a href="mailto:SQhelp@studiofathom.com">SQhelp@studiofathom.com</a>
                            <span class="contact-info">(510) 281-9000, ext.128</span>
                        </p>
                        <p>
                            <strong>Oakland Office</strong>
                            <a target="_blank" href="http://goo.gl/maps/xvF8c">
                                <span class="contact-info">620 3rd Street</span>
                                <span class="contact-info">Oakland, CA 94607</span>
                            </a>
                        </p>
                        <p>
                            <strong>Oakland Production Center</strong>
                            <a target="_blank" href="http://goo.gl/maps/FmTQD">
                                <span class="contact-info">315 Jefferson Street</span>
                                <span class="contact-info">Oakland, CA 94607</span>
                            </a>
                        </p>
                        <p>
                            <strong>Seattle Office &amp; Production Center</strong>
                            <a target="_blank" href="http://goo.gl/maps/7JATW">
                                <span class="contact-info">4302 Stone Way</span>
                                <span class="contact-info">North Seattle, WA 98103</span>
                            </a>
                        </p>
                    </div>
                </div>
                <div class="span4">
                    <img src="{{ asset('images/homepage-trio-collage-01.jpg') }}" style="height: 360px;" >
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endsection