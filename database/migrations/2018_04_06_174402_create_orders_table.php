<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', [
                'quote',
                'confirmed',
                'billed',
                'processing',
                'processed',
                'shipping',
                'shipped',
                'manual_quote_requested',
                'manual_quote_approved',
                'building',
                'post_processing',
                'refunded'
            ])->default('quote');
            $table->integer('user_id');
            $table->enum('process_schedule', ['standard', 'next_day', '2_day'])->default('standard');
            $table->float('amount')->default(0);
            $table->string('invoice_number')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email_address')->nullable();
            $table->integer('state_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
