<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company');
            $table->string('phone_number');
            $table->integer('shipping_id');
            $table->string('zip_code')->nullable();
            $table->string('know_from')->nullable();
            $table->string('physical')->nullable();
            $table->string('industry')->nullable();
            $table->string('title')->nullable();
            $table->string('contact')->nullable();
            $table->string('software')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('is_terms_agree')->default(false);
            $table->boolean('is_subscribed')->default(false);
            $table->boolean('email_verified')->default(false);

            $table->boolean('account_discount_active')->default(false);
            $table->string('account_discount_name')->nullable();
            $table->float('account_discount_rate')->default(0);

            $table->boolean('purchase_order')->default(false);
            $table->boolean('reseller')->default(false);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
