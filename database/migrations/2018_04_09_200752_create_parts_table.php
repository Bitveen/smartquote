<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->bigInteger('size');
            $table->integer('order_id');
            $table->integer('process_id')->nullable();
            $table->integer('material_id')->nullable();
            $table->enum('units', ['mm', 'in'])->default('mm');
            $table->string('finish')->default('standard');
            $table->boolean('multi_material')->default(false);
            $table->integer('quantity')->default(1);
            $table->float('price')->default(0);
            $table->boolean('locked')->default(false);

            // Streamics
            $table->double('x')->nullable();
            $table->double('y')->nullable();
            $table->double('z')->nullable();
            $table->double('volume')->nullable();
            $table->double('support_volume')->nullable();
            $table->double('surface_area')->nullable();
            $table->double('projected_area')->nullable();
            $table->double('height_estimated')->nullable();
            $table->enum('streamics_status', ['waiting', 'processed', 'error'])->default('waiting');


            $table->float('build_time')->nullable();
            $table->enum('mem_status', ['waiting', 'processed', 'error'])->default('waiting');
            $table->enum('pcm_status', ['waiting', 'processed', 'error'])->default('waiting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
