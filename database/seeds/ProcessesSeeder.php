<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProcessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('processes')->insert([
            [
                'name' => 'polyjet',
                'title' => 'PolyJet',
                'description' => 'Highest resolution and widest range of materials. Ideal for form, fit, and presentation models.<br><strong>PolyJet orders take up to (3) business days plus shipping &mdash; First Available Vero orders ship next business day.</strong>'
            ],
            [
                'name' => 'sls',
                'title' => 'SLS',
                'description' => 'Ideal for medium to large models. SLS has a standard layer thickness of .15mm. It allows for excellent surface finish while not sacrificing material strength.<br><strong>SLS orders take up to (4) business days plus shipping</strong>'
            ],
            [
                'name' => 'fdm',
                'title' => 'FDM',
                'description' => 'A cost effective solution for large models. It has a lower resolution, but allows for a number of engineering grade thermoplastics. A favorite for jigs, fixtures and production parts.<br><strong>FDM orders take up to (4) business days plus shipping</strong>'
            ],
            [
                'name' => 'sla',
                'title' => 'SLA',
                'description' => 'Excellent for larger aesthetic models that require smoother surface finish than FDM. The white thermoset resin simulates ABS and can be sanded or painted. <br><strong>SLA orders take up to (4) business days plus shipping</strong>'
            ],
            [
                'name' => 'cnc',
                'title' => 'CNC Machining',
                'description' => 'Milling and turning operations for rigid materials including plastics and metals, with many finish options and secondary operations available. Ideal for prototypes or low-volume production runs in end-use materials. Please allow 12-24 hours for review and quotation.<br><strong>CNC orders take 1-2 weeks plus shipping.</strong>'
            ],
            [
                'name' => 'uc',
                'title' => 'Urethane Casting',
                'description' => 'A silicone mold is used to cast rigid or flexible models in thermoset resins that mimic common thermoplastics. Ideal for small production runs of 5-50 units, or when multiple aesthetic models are needed. Please allow 12-24 hours for quotation.<br><strong>Casting orders take 1-2 weeks plus shipping.</strong>'
            ]
        ]);
    }
}
