<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('material_groups')->insert([
            [
                'name' => 'Vero',
                'description' => 'Standard rigid plastics; First Available Vero orders ship next business day.'
            ],
            [
                'name' => 'DM Shore',
                'description' => 'Digital Materials Shore plastics are blends of other resins that result in rubber-like materials. Available in different shore A hardness values (40A the softest of the material, while 95A is the hardest).'
            ],
            [
                'name' => 'DM Grey',
                'description' => 'Rigid plastic, a blend of TangoBlack+ and VeroWhite, resulting in different shades of Gray.'
            ],
            [
                'name' => 'Nylon 12',
                'description' => 'Strong, durable nylon 12 thermoplastic, suitable for production components, excellent for mild flexibility and snap-fit features, and especially economical for large quantities of small parts. SLS prints in white and can be dyed in standard colors; MJF prints in gray and can be dyed black.'
            ],
            [
                'name' => 'ASA',
                'description' => 'General purpose material, ideal for 3D printing and a suitable substitute for any ABS part.  Best choice for large builds and functional models, with good UV resistance.'
            ],
            [
                'name' => 'Polycarbonate',
                'description' => ''
            ]
        ]);
    }
}
