<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shippingId = DB::table('shippings')->insertGetId([
            'first_name' => 'Test name',
            'last_name' => 'Test name',
            'address' => 'New York address',
            'city' => 'New York',
            'zip_code' => 12345
        ]);

        DB::table('users')->insert([
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'test@smartquote.com',
            'password' => bcrypt(env('TEST_USER_PASSWORD')),
            'company' => 'Test company',
            'phone_number' => 1234567890,
            'shipping_id' => $shippingId,
            'is_terms_agree' => 1,
            'email_verified' => 1
        ]);
    }
}
