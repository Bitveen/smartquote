<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert([
            [
                'name' => 'Vero White Plus',
                'title' => 'White',
                'multiplier' => 1,
                'group_id' => 1,
                'process_id' => 1,
                'description' => 'The most commonly used material. Works for many applications from finishing the model to form and function tests.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Vero Gray',
                'title' => 'Gray',
                'multiplier' => 1,
                'group_id' => 1,
                'process_id' => 1,
                'description' => 'Empty',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Vero Blue',
                'title' => 'Blue',
                'multiplier' => 0.93,
                'group_id' => 1,
                'process_id' => 1,
                'description' => 'Empty',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Vero Black',
                'title' => 'Black',
                'multiplier' => 1,
                'group_id' => 1,
                'process_id' => 1,
                'description' => 'Great for form studies and fit tests. It shows off small features well.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Vero Clear',
                'title' => 'Clear',
                'multiplier' => 1.19,
                'group_id' => 1,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'First Available Vero',
                'title' => 'First Available Vero',
                'multiplier' => 1,
                'group_id' => 1,
                'process_id' => 1,
                'description' => 'Choosing First Available Vero helps our technicians process your order as quickly as possible. Select this option for rigid parts when color is not a concern.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ABS-Like',
                'title' => 'ABS-Like',
                'multiplier' => 1.35,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'Most functional PolyJet material with highest heat deflection and impact resistance. Best PolyJet material for snap-fits and flexural features. Ideal for functional models.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Full Cure 720',
                'title' => 'Full Cure 720',
                'multiplier' => 0.83,
                'group_id' => null,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Agilus30',
                'title' => 'Agilus30+',
                'multiplier' => 1.19,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'Flexible, translucent rubber-like plastic with a Shore A value of 27A.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Tango Black Plus',
                'title' => 'TangoBlack+',
                'multiplier' => 1.19,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'Flexible rubber-like plastic, with a Shore A value of 27A.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 27A',
                'title' => '27A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 40A',
                'title' => '40A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Softest, most comparable to pencil eraser.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 50A',
                'title' => '50A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Comparable to inner tube.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 60A',
                'title' => '60A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Comparable to running shoe sole.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 70A',
                'title' => '70A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Comparable to tire thread.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 85A',
                'title' => '85A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Comparable to shoe heel.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Shore 95A',
                'title' => '95A',
                'multiplier' => 1.25,
                'group_id' => 2,
                'process_id' => 1,
                'description' => 'Hardest, comparable to shopping cart wheel.',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],

            [
                'name' => 'Digital Material Grey 20',
                'title' => '20',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Grey 25',
                'title' => '25',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Grey 35',
                'title' => '35',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Grey 40',
                'title' => '40',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Grey 50',
                'title' => '50',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Digital Material Grey 60',
                'title' => '60',
                'multiplier' => 1.25,
                'group_id' => 3,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 1,
                'enabled' => 1
            ],
            [
                'name' => 'Endur',
                'title' => 'Endur',
                'multiplier' => 1.07,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'Off-white in color. Simulates the appearance and functionality of polypropylene.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Connex3 Colors',
                'title' => 'Connex3',
                'multiplier' => 1.07,
                'group_id' => null,
                'process_id' => 1,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Durus White',
                'title' => 'Durus',
                'multiplier' => 1.22,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'This is the most flexible hard material. It is designed to work like polypropylene.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'MED610',
                'title' => 'MED610',
                'multiplier' => 1.4,
                'group_id' => null,
                'process_id' => 1,
                'description' => 'Rigid and translucent bio-compatible material. Approved for limited contact with skin (up to 30 days) and mucosal membrane (up to 24 hours)',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'SLS White',
                'title' => 'SLS White',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'SLS Red',
                'title' => 'SLS Red',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'SLS Blue',
                'title' => 'SLS Blue',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'SLS Yellow',
                'title' => 'SLS Yellow',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'SLS Black',
                'title' => 'SLS Black',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'MJF Gray',
                'title' => 'MJF Gray',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'MJF Black',
                'title' => 'MJF Black',
                'multiplier' => 1,
                'group_id' => 4,
                'process_id' => 2,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ABS',
                'title' => 'ABS',
                'multiplier' => 1.35,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'White in color. General purpose material. Good choice for functional models.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PC/ABS',
                'title' => 'PC/ABS',
                'multiplier' => 1.35,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Black in color. Good for snap-fits and functional models.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PC',
                'title' => 'PC',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Off-white in color. Ideal for high strength and applications requiring heat resistance.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ULTEM 9085',
                'title' => 'ULTEM 9085',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Tan or black in color. Highest performance plastic, FST-rated.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PPSF',
                'title' => 'PPSF',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Excellent material for chemical and heat resistance. Great for most applications, especially industrial transportation, machining, and medical.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ASA (White)',
                'title' => 'White',
                'multiplier' => 1,
                'group_id' => 5,
                'process_id' => 3,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ASA (Black)',
                'title' => 'Black',
                'multiplier' => 1,
                'group_id' => 5,
                'process_id' => 3,
                'description' => '',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Nylon',
                'title' => 'Nylon',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Black in color. Ideal for applications requiring flexibility, impact resistance, low friction, and fatigue resistance. Excellent for snap-fits and functional models.',
                'manual_quotation_required' => 0,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ULTEM 1010',
                'title' => 'ULTEM 1010',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Tan in color. Highest performance plastic, food-safe. Less impact resistance than ULTEM 9085.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ABS-ESD7',
                'title' => 'ABS-ESD7',
                'multiplier' => 1.35,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Black in color. Static dissipative variant of ABS, ideal for electronics assembly fixtures.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Nylon 12CF',
                'title' => 'Nylon 12CF',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 3,
                'description' => 'Black in color. Nylon 12 filled with 30% carbon fiber by volume. Highest strength- and stiffness-to-weight ratio of any Stratasys FDM materials.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ABS',
                'title' => 'ABS',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => 'Natural/Off-White.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Acrylic',
                'title' => 'Acrylic',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => 'Clear.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Aluminum 6061',
                'title' => 'Aluminum',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => '6061 Alloy.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Delrin',
                'title' => 'Delrin',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => 'Natural/Off-White.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PC (White)',
                'title' => 'White',
                'multiplier' => 1,
                'group_id' => 6,
                'process_id' => 5,
                'description' => '',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PC (Clear)',
                'title' => 'Clear',
                'multiplier' => 1,
                'group_id' => 6,
                'process_id' => 5,
                'description' => '',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Stainless Steel 303',
                'title' => 'Steel',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => '303 Stainless.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Other',
                'title' => 'Other',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 5,
                'description' => 'Please specify in comments.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'ABS-Sim',
                'title' => 'ABS-Sim',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'ABS-like. Specify color in comments.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'PP-Sim',
                'title' => 'PP-Sim',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'Polypropylene-like. Specify color in comments.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Flame Retardant',
                'title' => 'Flame Retardant',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'UL94V0-rated. Off-White.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Rigid Clear',
                'title' => 'Rigid Clear',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'Acrylic-like. Clear.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'High Temp',
                'title' => 'High Temp',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'High-temperature applications (to 120 C). Black.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Elastomer 1',
                'title' => 'Elastomer 1',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'Rubber-like. Specify color in comments.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Elastomer 2',
                'title' => 'Elastomer 2',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'Rubber-like. Colorless translucent.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'Silicone',
                'title' => 'Silicone',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 6,
                'description' => 'Silicone',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ],
            [
                'name' => 'White',
                'title' => 'White',
                'multiplier' => 1,
                'group_id' => null,
                'process_id' => 4,
                'description' => 'Bright white thermoset resin simulates ABS with smoother surface finish than FDM. Rigid with mild flexibility.',
                'manual_quotation_required' => 1,
                'expedited' => 0,
                'enabled' => 1
            ]
        ]);
    }
}
