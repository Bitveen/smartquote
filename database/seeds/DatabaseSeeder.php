<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ShippingStatesTableSeeder::class,
            UsersTableSeeder::class,
            ProcessesSeeder::class,
            MaterialGroupsSeeder::class,
            MaterialsTableSeeder::class
        ]);
    }
}
