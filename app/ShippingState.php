<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingState extends Model
{
    protected $fillable = [
        'name',
        'code'
    ];
}
