<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset as Email;

class PasswordReset extends Model
{
    protected $fillable = [
        'email', 'token',
    ];

    public static function send(User $user)
    {
        $token = Str::uuid();

        self::create([
            'email' => $user->email,
            'token' => $token
        ]);

        Mail::to($user)->send(new Email($user, $token));
    }
}
