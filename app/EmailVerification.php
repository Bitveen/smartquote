<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\EmailVerification as Email;

class EmailVerification extends Model
{
    protected $fillable = [
        'email', 'token',
    ];

    public static function send(User $user)
    {
        self::where('email', $user->email)->delete();

        $verification = self::create([
            'email' => $user->email,
            'token' => Str::uuid()
        ]);

        Mail::to($user)->send(new Email($user, $verification->token));
    }

    public static function verify($token)
    {
        $verification = self::where('token', $token)->first();

        if (!$verification) {
            return false;
        }

        // TODO: expired links

        User::where('email', $verification->email)->update(['email_verified' => true]);

        self::where('email', $verification->email)->delete();

        return true;
    }

}
