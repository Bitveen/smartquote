<?php

namespace App\Listeners;

use App\Events\PartProcessed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class PartProcessedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PartProcessed  $event
     * @return void
     */
    public function handle(PartProcessed $event)
    {
        // TODO: Notify client
        Log::info('PART: '.$event->part->id);
        Log::info('PART PROCESSED LISTENER');
    }
}
