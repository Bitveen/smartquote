<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use App\Part;

class PartProcessed
{
    use Dispatchable, SerializesModels;

    public $part;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Part $part)
    {
        $this->part = $part;
    }
}
