<?php

namespace App\Utils;

class Schedule
{
    public static function format($slug, $priceData)
    {
        $schedules = [
            'standard' => 'Standard Delivery',
            'next_day' => 'Next Day Delivery (+$'.number_format($priceData['expeditedCosts']['next_day'], 2).')',
            '2_day'    => 'Second Day Delivery (+$'.number_format($priceData['expeditedCosts']['2_day'], 2).')',
        ];

        return $schedules[$slug];
    }
}