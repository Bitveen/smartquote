<?php

namespace App\Utils;

class Quantity
{


    private $value;
    private $units;
    private $operation;

    public function __construct($value, $units, $operation)
    {
        $this->value = $value;
        $this->units = $units;
        $this->operation = $operation;
    }

    public function to($units)
    {
        switch ($this->operation) {
            case 'volume':
                return $this->translateVolume($units);
                break;
            case 'area':
                return $this->translateArea($units);
                break;
            case 'length':
                return $this->translateLength($units);
                break;
            default:
                return 0;
        }
    }

    public static function volume($value, $units)
    {
        return new self($value, $units, 'volume');
    }

    public static function area($value, $units)
    {
        return new self($value, $units, 'area');
    }

    public static function length($value, $units)
    {
        return new self($value, $units, 'length');
    }

    private function translateVolume($units)
    {
        if ($this->units === $units) return $this->value;

        switch ($this->units) {
            case 'in':
                if ($units === 'mm') {
                    return ((25.4 * 25.4 * 25.4) * $this->value);
                }
                break;
            case 'mm':
                if ($units === 'in') {
                    return ((0.03937007874 * 0.03937007874 * 0.03937007874) * $this->value);
                }
                break;
            default:
                return 0;
        }
        return 0;
    }

    private function translateArea($units)
    {
        if ($this->units === $units) return $this->value;

        switch ($this->units) {
            case 'in':
                if ($units === 'mm') {
                    return ((25.4 * 25.4) * $this->value);
                }
                break;
            case 'mm':
                if ($units === 'in') {
                    return ((0.03937007874 * 0.03937007874) * $this->value);
                }
                break;
            default:
                return 0;
        }
        return 0;
    }

    private function translateLength($units)
    {
        if ($this->units === $units) return $this->value;

        switch ($this->units) {
            case 'in':
                if ($units === 'mm') {
                    return (25.4 * $this->value);
                }
                break;
            case 'mm':
                if ($units === 'in') {
                    return (0.03937007874 * $this->value);
                }
                break;
            default:
                return 0;
        }
        return 0;
    }
}