<?php

namespace App\Utils;

use App\Fee;
use App\Material;
use App\Order;
use App\Part;
use App\Process;
use App\User;

class Calculator
{
    public static function calculateAccountDiscount(Order $order)
    {
        $fees = $order->fees;

        foreach ($fees as $fee) {
            if ($fee->type === 'account') {
                $fee->delete();
            }
        }

        $baseOrderPrice   = self::getBaseOrderPrice($order);
        $nonDiscountPrice = self::getBaseOrderNonDiscountPrice($order);

        $user = User::find($order->user_id);

        $minimumOrder = env('MINIMUM_ORDER');

        if ($nonDiscountPrice > 0) {
            $printingBase = $baseOrderPrice - $nonDiscountPrice;
            $accountDiscount = round($printingBase * ($user->account_discount_rate / 100.0),2);
        } else if ($baseOrderPrice <= $minimumOrder) {
            $accountDiscount = 0;
        } else {
            $max = $baseOrderPrice - $minimumOrder;
            $accountDiscount = min($max, round($baseOrderPrice * ($user->account_discount_rate / 100.0),2));
        }

        if ($user->account_discount_active && $accountDiscount > 0) {
            Fee::create([
                'order_id' => $order->id,
                'type' => 'account',
                'name' => $user->account_discount_name,
                'price' => $accountDiscount
            ]);
        }
    }

    public static function priceQuote(Order $order)
    {

        $baseOrderPrice = self::getBaseOrderPrice($order);

        list($baseSubtotal, $hasPriceAdjustment, $priceAdjustment) = self::getOrderPriceAdjustment($baseOrderPrice);
        list($baseSubtotal, $expeditedCosts, $expediteAvailable)   = self::getExpeditedCosts($order, $baseSubtotal);
        list($baseTotal, $feesCost)                                = self::getFees($order, $baseSubtotal);

        $discounts = self::getDiscounts($order, $baseOrderPrice);

        $baseTotal -= $discounts;

        return [
            'baseOrderPrice' => $baseOrderPrice,
            'expeditedCosts' => $expeditedCosts,
            'baseSubtotal' => $baseSubtotal,
            'baseTotal' => $baseTotal,
            'hasPriceAdjustment' => $hasPriceAdjustment,
            'priceAdjustment' => $priceAdjustment,
            'hasExpediteAvailable' => $expediteAvailable,
        ];
    }

    private static function getBaseOrderPrice(Order $order)
    {
        return Part::where(['order_id' => $order->id])->sum('price');
    }

    private static function getBaseOrderNonDiscountPrice(Order $order)
    {
        $totalPrice = 0;

        foreach ($order->parts as $part) {
            $process = Process::find($part->process_id);

            if ($process->name === 'cnc' || $process->name === 'ua') {
                $totalPrice += $part->price;
            }
        }

        return $totalPrice;
    }

    private static function getOrderPriceAdjustment($baseOrderPrice)
    {
        $minimumOrder = env('MINIMUM_ORDER');

        if ($baseOrderPrice < $minimumOrder) {
            return [$minimumOrder, true, $minimumOrder - $baseOrderPrice];
        } else {
            return [$baseOrderPrice, false, 0];
        }
    }

    private static function getExpeditedCosts(Order $order, $baseSubtotal)
    {
        $expeditionAvailable = true;

        foreach ($order->parts as $part) {
            $process = Process::find($part->process_id);

            if ($process->name !== 'polyjet') {
                $expeditionAvailable = false;
                break;
            }

            $material = Material::find($part->material_id);

            if (!$material->expedited) {
                $expeditionAvailable = false;
                break;
            }
        }

        if ($baseSubtotal < 1000) {
            $costs = [
                'next_day' => 250,
                '2_day' => 250,
            ];
        } else {
            $costs = [
                'next_day' => $baseSubtotal * .25,
                '2_day' => $baseSubtotal * .25,
            ];
        }

        if ($expeditionAvailable) {
            if ($order->process_schedule == 'next_day') {
                $baseSubtotal += $costs['next_day'];
            } elseif ($order->process_schedule == '2_day') {
                $baseSubtotal += $costs['2_day'];
            }
        } else {
            if ($order->process_schedule !== 'standard') {
                $order->process_schedule = 'standard';
                $order->save();
            }
        }

        return [$baseSubtotal, $costs, $expeditionAvailable];
    }

    private static function getFees(Order $order, $baseSubtotal)
    {
        $fees = $order->fees;

        $feesCost = 0;

        if (count($fees)) {
            foreach ($fees as $fee) {
                if ($fee->type != 'discount' && $fee->type != 'account') {
                    $name = $fee->name;
                    $type = $fee->type;
                    $price = $fee->price;
                    if ($name && $type && $price) {
                        $feesCost += $price;
                    }
                }
            }
        }

        $feesTotal = $baseSubtotal + $feesCost;

        return [$feesTotal, $feesCost];
    }

    private static function getDiscounts(Order $order, $baseSubtotal)
    {
        $fees = $order->fees;
        $discountAmount = 0;

        if ($fees) {
            foreach ($fees as $fee) {
                if ($fee->type == 'discount' || $fee->type == 'account') {
                    $name = $fee->name;
                    $type = $fee->type;
                    $price = $fee->price;
                    if ($name && $type && $price) {
                        $discountAmount += $price;
                    }
                }
            }
        }

        $minimumOrder = env('MINIMUM_ORDER');

        if ($baseSubtotal < $minimumOrder) {
            $discountAmount = 0;
        }

        if(($baseSubtotal >= $minimumOrder) && ($baseSubtotal - $discountAmount < $minimumOrder)) {
            $discountAmount = round($baseSubtotal, 2) - $minimumOrder;
        }

        return $discountAmount;
    }

}