<?php

namespace App\Utils;

class Materials
{
    public static function translate($process, $material)
    {
        $materials = [
            'polyjet' => [
                'ABS-Like' => 'Digital ABS (ivory)',
                'Vero White Plus' => 'VeroWhitePlus',
                'Vero Gray' => 'Other PJ Non-Standard Material',
                'Vero Blue' => 'Other PJ Non-Standard Material',
                'Vero Black' => 'VeroBlackPlus',
                'Vero Clear' => 'VeroClear',
                'Agilus30' => 'Agilus30 (translucent)',
                'Tango Black Plus' => 'TangoBlackPlus',
                'Digital Material Shore 40A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Shore 50A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Shore 60A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Shore 70A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Shore 85A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Shore 95A' => 'Digital Shore A (TangoBlackPlus)',
                'Digital Material Grey 20' => 'Digital Gray',
                'Digital Material Grey 25' => 'Digital Gray',
                'Digital Material Grey 35' => 'Digital Gray',
                'Digital Material Grey 40' => 'Digital Gray',
                'Digital Material Grey 50' => 'Digital Gray',
                'Digital Material Grey 60' => 'Digital Gray',
                'MED610' => 'MED610 (translucent)',
                'First Available Vero' => 'First Available Rigid',
                'Endur' => 'Rigur (ivory)'
            ],
            'fdm' => [
                'ABS' => 'ABS (white)',
                'PC/ABS' => 'PC-ABS (black)',
                'PC' => 'PC (white)',
                'ULTEM 9085' => 'Ultem 9085 (natural)',
                'ASA (Black)' => 'ASA (black)',
                'ASA (White)' => 'ASA (white)',
                'Nylon' => 'Nylon 12 (black, FDM)',
                'ULTEM 1010' => 'Ultem 1010 (natural)',
                'ABS-ESD7' => 'Other FDM Non-Standard Material'
            ],
            'sls' => [
                'PA2200(Nylon)' => 'Nylon 12 (white, SLS)'
            ],
            'cnc' => [
                'ABS' => 'None',
                'Acrylic' => 'None',
                'Aluminum 6061' => 'None',
                'Delrin' => 'None',
                'PC(White)' => 'None',
                'PC(Clear)' => 'None',
                'Stainless Steel 303' => 'None',
                'Other' => 'None'
            ],
            'uc' => [
                'ABS-Sim' => 'None',
                'PP-Sim' => 'None',
                'Flame Retardant' => 'None',
                'Rigid Clear' => 'None',
                'High Temp' => 'None',
                'Elastomer 1' => 'None',
                'Elastomer 2' => 'None',
                'Silicone' => 'None'
            ],
            'sla' => [
                'White' => 'Accura Xtreme White 200'
            ]
        ];

        if (array_key_exists($material, $materials[$process])) {
            return $materials[$process][$material];
        }

        return $material;
    }
}