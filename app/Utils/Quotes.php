<?php

namespace App\Utils;

use App\Material;
use App\Order;

class Quotes
{
    public static function isManual(Order $quote)
    {
        $isManualQuote = false;
        $reason = '';

        foreach ($quote->parts as $part) {
            if ($part->multi_material) {
                $isManualQuote = true;
                $reason = 'multi';
                break;
            }

            $maxSide = max($part->x, $part->y, $part->z);

            if ($part->units == 'mm' && $maxSide > 203) {
                $isManualQuote = true;
                $reason = 'size';
                break;
            }

            if ($part->units == 'in' && $maxSide > 8) {
                $isManualQuote = true;
                $reason = 'size';
                break;
            }

            $material = Material::where(['id' => $part->material_id])->first();

            if ($material->manual_quotation_required) {
                $isManualQuote = true;
                $reason = 'mat';
                break;
            }
        }

        return [
            $isManualQuote,
            $reason
        ];
    }
}