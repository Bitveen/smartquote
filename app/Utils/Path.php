<?php

namespace App\Utils;

class Path
{
    /**
     * Make full file path
     *
     * @param $quoteId
     * @param $partId
     * @return string
     */
    public static function buildFullFilePath($quoteId, $partId)
    {
        return 'public/orders/'.$quoteId.'/parts/'.$partId;
    }


    /**
     * Normalize file name
     *
     * @param $fullFileName - Full file name with extension
     * @return null|string|string[]
     */
    public static function normalizeFileName($fullFileName)
    {
        $fileInfo = pathinfo($fullFileName);

        $fileName = $fileInfo['filename'];
        $extension = $fileInfo['extension'];

        $fileName = str_replace([' ', '-', '.'], ['_', '_', ''], $fileName).'.'.$extension;
        $fileName = preg_replace('/[^._a-zA-Z0-9s]/', '', $fileName);

        return $fileName;
    }
}