<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $fillable = [
        'order_id',
        'type',
        'name',
        'price'
    ];

    protected $appends = ['report_type', 'report_name'];

    public function getReportTypeAttribute()
    {
        $type = 'Other';

        switch ($this->type) {
            case 'material':
                $type = 'Material Setup';
                break;
            case 'leadtime':
                $type = 'Expedite';
                break;
            case 'discount':
                $type = 'Discount';
                break;
            case 'account':
                $type = 'Standing Discount';
                break;
        }

        return $type;
    }

    public function getReportNameAttribute()
    {
        $name = $this->name;

        if ($this->type === 'leadtime') {
            if ($this->name === 'same_day') {
                $name = 'Same Day';
            } else {
                $name = 'Next Day';
            }
        }

        return $name;
    }

}
