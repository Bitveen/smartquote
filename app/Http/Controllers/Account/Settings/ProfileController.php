<?php

namespace App\Http\Controllers\Account\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller
{
    public function edit()
    {
        return view('account.settings.profile');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'company' => 'required',
        ]);

        $data = $request->all();

        User::where('id', auth()->id())->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone_number' => $data['phone_number'],
            'company' => $data['company'],
            'physical' => $data['physical'] ?? null,
            'industry' => $data['industry'] ?? null,
            'title' => $data['title'] ?? null,
            'contact' => $data['contact'] ?? null,
            'software' => $data['software'] ?? null,
            'comment' => $data['comment'] ?? null
        ]);

        session()->flash('profile-updated');

        return redirect()->back();
    }
}
