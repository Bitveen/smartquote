<?php

namespace App\Http\Controllers\Account\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;

class ChangePasswordController extends Controller
{
    public function edit()
    {
        return view('account.settings.change-password');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
           'password' => 'required',
           'new_password' => 'required|confirmed'
        ]);

        $newPassword = $request->get('new_password');
        $oldPassword = $request->get('password');

        if (!Hash::check($oldPassword, auth()->user()->makeVisible('password')->password)) {
            session()->flash('password-mismatch');
            return redirect()->back();
        }

        User::where('id', auth()->id())->update([
            'password' => Hash::make($newPassword)
        ]);

        session()->flash('password-updated');

        return redirect()->back();
    }
}
