<?php

namespace App\Http\Controllers\Account\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ShippingState;
use App\Shipping;

class ShippingController extends Controller
{
    public function edit()
    {
        $states = ShippingState::all();
        return view('account.settings.shipping', compact('states'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zip_code' => 'required'
        ]);

        $data = $request->all();

        Shipping::where('id', auth()->user()->shipping_id)->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'address' => $data['address'],
            'city' => $data['city'],
            'zip_code' => $data['zip_code'],
            'additional_address' => $data['additional_address'] ?? null,
            'state_id' => $data['state_id'] ?? null
        ]);

        session()->flash('shipping-updated');

        return redirect()->back();
    }
}
