<?php

namespace App\Http\Controllers\Account\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = [];
        return view('account.history.orders', compact('orders'));
    }
}
