<?php

namespace App\Http\Controllers\Account\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManualQuotesController extends Controller
{
    public function index()
    {
        $manualQuotes = [];
        return view('account.history.manual-quotes', compact('manualQuotes'));
    }
}
