<?php

namespace App\Http\Controllers\Account\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuotesController extends Controller
{
    public function index()
    {
        $quotes = [];
        return view('account.history.quotes', compact('quotes'));
    }
}
