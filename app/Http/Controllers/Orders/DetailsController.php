<?php

namespace App\Http\Controllers\Orders;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Utils\Calculator;
use App\ShippingState;

class DetailsController extends Controller
{
    public function show($id)
    {

        $order = Order::with(['parts', 'comments', 'fees'])->findOrFail($id);
        $user = User::with('shipping')->find($order->user_id);
        $states = ShippingState::all();

        $priceData = Calculator::priceQuote($order);

        $grandTotal = $priceData['baseTotal'];
        $total = '$'.number_format($grandTotal,2);

        $years = [];
        $yearsCount = 0;
        $currentYear = Carbon::now()->year;
        while ($yearsCount < 7) {
            $years[] = $currentYear++;
            $yearsCount++;
        }

        return view('orders.details', compact('order', 'user', 'total', 'states', 'years'));
    }
}
