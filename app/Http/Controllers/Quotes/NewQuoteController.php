<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class NewQuoteController extends Controller
{
    public function store()
    {
        $id = Order::create([
            'user_id' => auth()->id()
        ]);

        return redirect()->route('quotes.upload.show', [ 'id' => $id ]);
    }
}
