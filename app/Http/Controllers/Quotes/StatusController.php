<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Part;
use App\Order;

class StatusController extends Controller
{
    public function checkStreamicsStatus($quoteId)
    {
        $quote = Order::where(['id' => $quoteId])->with('parts')->first();

        $processedParts = 0;

        foreach ($quote->parts as $part) {
            if ($part->streamics_status === 'processed') {
                $processedParts++;
            }
        }

        return [
            'processed' => $processedParts,
            'total' => count($quote->parts)
        ];
    }

    public function checkSpreadsheetStatus($quoteId)
    {
        $quote = Order::where(['id' => $quoteId])->with('parts')->first();

        $processedParts = 0;

        foreach ($quote->parts as $part) {
            if ($part->mem_status === 'processed' && $part->pcm_status === 'processed') {
                $processedParts++;
            }
        }

        return [
            'processed' => $processedParts,
            'total' => count($quote->parts)
        ];
    }
}
