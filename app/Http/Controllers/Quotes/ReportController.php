<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Utils\Calculator;
use App\Utils\Schedule;

class ReportController extends Controller
{
    /**
     * Make PDF for quote
     *
     * @param $quoteId - Quote ID
     * @return mixed - PDF file
     */
    public function download($quoteId)
    {

        $order = Order::with(['fees', 'parts', 'comments'])->find($quoteId);

        $user = User::find($order->user_id);

        $priceData = Calculator::priceQuote($order);

        $schedule = Schedule::format($order->process_schedule, $priceData);

        $priceAdjustment = '$'.number_format($priceData['priceAdjustment'],2);

        $total = '$'.number_format($priceData['baseTotal'],2);

        $pdf = \PDF::loadView('reports.quote', [
            'quote' => $order,
            'user' => $user,
            'priceAdjustment' => $priceAdjustment,
            'schedule' => $schedule,
            'total' => $total
        ]);

        return $pdf->download('order_'.$quoteId.'.pdf');
    }
}
