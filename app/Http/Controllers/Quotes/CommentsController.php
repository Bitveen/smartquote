<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrderComment;

class CommentsController extends Controller
{
    public function save(Request $request, $quoteId)
    {
        $message = $request->get('message');

        if (!$message) {
            return [
                'saved' => false
            ];
        }

        OrderComment::create([
            'message' => $message,
            'order_id' => $quoteId,
            'customer_message' => 1
        ]);

        return [
            'saved' => true
        ];
    }
}
