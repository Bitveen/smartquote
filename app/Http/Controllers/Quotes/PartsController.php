<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Part;

class PartsController extends Controller
{
    public function fetch($quoteId)
    {
        return Part::where(['order_id' => $quoteId])->with(['material', 'process'])->get();
    }

    public function save(Request $request, $quoteId)
    {
        $parts = $request->get('parts');

        if (!count($parts)) return abort(400, 'Parts empty.');

        foreach ($parts as $part) {
            Part::where(['id' => $part['id']])->update([
                'quantity' => $part['quantity'],
                'units' => $part['units'],
                'finish' => $part['finish'],
                'multi_material' => $part['multi_material'],
                'process_id' => $part['process_id'],
                'material_id' => $part['material_id'],
                'mem_status' => 'waiting',
                'pcm_status' => 'waiting'
            ]);
        }

        return [
            'updated' => true
        ];
    }
}
