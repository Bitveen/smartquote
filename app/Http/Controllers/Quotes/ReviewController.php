<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Part;
use App\Order;
use App\Jobs\HandleMem;
use App\Jobs\HandlePcm;
use App\Utils\Calculator;
use App\Utils\Schedule;
use App\Utils\Quotes;

class ReviewController extends Controller
{
    public function show($quoteId)
    {
        $order = Order::with(['parts', 'comments', 'fees'])->find($quoteId);

        Calculator::calculateAccountDiscount($order);

        $manualQuote = false;
        $manualQuoteSize = false;
        $manualQuoteMulti = false;
        $manualQuoteMaterial = false;

        $manualQuoteApproved = false;
        if ($order->status === 'manual_quote_approved') {
            $manualQuoteApproved = true;
        }

        list($manualQuote, $reason) = Quotes::isManual($order);

        switch ($reason) {
            case 'size':
                $manualQuoteSize = true;
                break;
            case 'multi':
                $manualQuoteMulti = true;
                break;
            case 'mat':
                $manualQuoteMaterial = true;
                break;
        }

        $priceData = Calculator::priceQuote($order);

        $schedule = Schedule::format($order->process_schedule, $priceData);

        $total = 0;
        $adjustment = 0;
        $subtotal = 0;

        if (!$manualQuote) {
            if ($priceData['hasPriceAdjustment']) {
                $adjustment = '$'.number_format($priceData['priceAdjustment'], 2);
            }
            $total = '$'.number_format($priceData['baseTotal'], 2);
            $subtotal = '$'.number_format($priceData['baseSubtotal'], 2);
        }

        return view('quotes.review', [
            'quote' => $order,
            'total' => $total,
            'manualQuoteMulti' => $manualQuoteMulti,
            'manualQuoteSize' => $manualQuoteSize,
            'manualQuoteMaterial' => $manualQuoteMaterial,
            'manualQuote' => $manualQuote,
            'manualQuoteApproved' => $manualQuoteApproved,
            'schedule' => $schedule,
            'adjustment' => $adjustment
        ]);
    }

    public function beginReview($quoteId)
    {
        $predicates = [
            'order_id' => $quoteId,
            'streamics_status' => 'processed',
            'mem_status' => 'waiting',
            'pcm_status' => 'waiting'
        ];

        $parts = Part::where($predicates)->with(['material', 'process'])->get();

        foreach ($parts as $part) {
            HandleMem::withChain([
                new HandlePcm($part)
            ])->dispatch($part)->allOnQueue('spreadsheet');
        }

        return [
            'total' => count($parts)
        ];
    }
}
