<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class ManualController extends Controller
{
    public function save($quoteId)
    {
        $order = Order::find($quoteId);

        $order->status = 'manual_quote_requested';
        $order->save();

        session()->flash('manualQuoteRequested', true);

        return [
            'updated' => true
        ];
    }
}
