<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessPart;
use App\Utils\Path;
use App\Order;
use App\Part;

class UploadController extends Controller
{
    private $allowedExtensions = [
        '.stl',
        '.3dm',
        '.3ds',
        '.model',
        '.exp',
        '.dlv',
        '.dlv3',
        '.catpart',
        '.iges',
        '.igs',
        '.obj',
        '.x_t',
        '.x_b',
        '.prt',
        '.stp',
        '.step',
        '.ply',
        '.zcp',
        '.vda',
        '.wrl',
        '.vrml',
        '.skp',
        '.sldprt',
        '.sat',
        '.jt',
        '.dxf',
        '.fbx',
        '.dae',
        '.zpr',
    ];

    /**
     * Show upload files view
     *
     * @param $quoteId - Quote id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function index($quoteId)
    {
        $quote = Order::find($quoteId);

        if (auth()->user()->cant('view', $quote)) {
            $reason = 'You don\'t have enough permissions to upload files for this quote.';
            return view('errors.access-denied', compact('reason'));
        }

        return view('quotes.upload');
    }

    /**
     * Upload files for current quote
     *
     * @param Request $request
     * @param $quoteId - Quote id
     * @return array|void
     */
    public function upload(Request $request, $quoteId)
    {
        $quote = Order::find($quoteId);

        if (auth()->user()->cant('uploadFiles', $quote)) {
            return abort(403, 'You don\'t have permission to upload files for this quote.');
        }

        if (!$request->hasFile('cad')) {
            return abort(400, 'Bad request. File not provided');
        }

        $file = $request->file('cad');
        $extension = $file->getClientOriginalExtension();

        if ($extension === 'zip') {
            return $this->handleZip($file, $quoteId);
        } else {
            return $this->handleCad($file, $quoteId);
        }
    }

    /**
     * @param Request $request
     * @param $quoteId
     * @param $partId
     * @return array|void
     */
    public function drop(Request $request, $quoteId, $partId)
    {
        $quote = Order::find($quoteId);

        if (auth()->user()->cant('deleteFiles', $quote)) {
            return abort(403, 'You don\'t have permission to delete files from this quote.');
        }

        $part = Part::where(['id' =>  $partId, 'order_id' => $quoteId ])->firstOrFail();

        $filePath = 'parts/'.$quoteId.'/'.$part->name;

        Storage::delete($filePath);

        $part->delete();

        return [
            'status' => 'deleted'
        ];
    }

    /**
     * Handle ZIP extracting
     *
     * @param UploadedFile $file - Zip file
     * @param $quoteId - Quote id
     * @return array
     */
    private function handleZip(UploadedFile $file, $quoteId)
    {
        $filePath = $file->path();
        $zip = new \ZipArchive();

        $uploadedFiles = [];
        $errorsCount = 0;

        $defaultMaterial = Part::getDefaultMaterial();
        $defaultProcess = Part::getDefaultProcess();

        if ($zip->open($filePath)) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $entry = $zip->getNameIndex($i);

                /* MAC OS X Specific */
                if (starts_with($entry, '__MACOSX')) {
                    continue;
                }

                if (!ends_with($entry, $this->allowedExtensions)) {
                    $errorsCount++;
                    continue;
                }

                $fp = $zip->getStream($entry);

                if ($fp) {
                    $fullFileName = basename($entry);

                    $stats = $zip->statIndex($i);

                    $part = Part::create([
                        'name' => $fullFileName,
                        'size' =>  $stats['size'],
                        'order_id' => intval($quoteId),
                        'material_id' => $defaultMaterial ? $defaultMaterial->id : null,
                        'process_id' => $defaultProcess ? $defaultProcess->id: null,
                        'finish' => 'glossy'
                    ]);

                    $partPath = Path::buildFullFilePath($quoteId, $part->id);
                    $destinationPath = storage_path('app/'.$partPath);

                    if (!Storage::exists($partPath)) {
                        Storage::makeDirectory($partPath);
                    }

                    $this->dispatch(new ProcessPart($part));

                    $uploadedFiles[] = $part;

                    file_put_contents($destinationPath.'/'.$fullFileName, $fp);
                }
            }
            $zip->close();
        }

        return $uploadedFiles;
    }

    /**
     * Handle files upload
     *
     * @param UploadedFile $file - File from client
     * @param $quoteId - Quote id
     * @return array
     */
    private function handleCad(UploadedFile $file, $quoteId)
    {
        $fileName = Path::normalizeFileName($file->getClientOriginalName());

        if (!ends_with($fileName, $this->allowedExtensions)) {
            return abort(400, 'Not allowed file format.');
        }

        $defaultMaterial = Part::getDefaultMaterial();
        $defaultProcess = Part::getDefaultProcess();

        $part = Part::create([
            'name' => $fileName,
            'size' =>  $file->getClientSize(),
            'order_id' => intval($quoteId),
            'material_id' => $defaultMaterial ? $defaultMaterial->id : null,
            'process_id' => $defaultProcess ? $defaultProcess->id: null,
            'finish' => 'glossy'
        ]);

        $fullFilePath = Path::buildFullFilePath($quoteId, $part->id);
        $path = $file->storeAs($fullFilePath,  $fileName);

        ProcessPart::dispatch($part)->onQueue('streamics');

        return [
            'id' => $part->id
        ];
    }
}
