<?php

namespace App\Http\Controllers\Manufacturing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Material;
use App\MaterialGroup;

class MaterialsController extends Controller
{
    public function fetch($processId)
    {

        return Material::where([ 'process_id' => $processId, 'enabled' => 1 ])->with('group')->get();
    }
}
