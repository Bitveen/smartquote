<?php

namespace App\Http\Controllers\Manufacturing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Process;

class ProcessesController extends Controller
{
    public function fetch()
    {
        return Process::all()->load('materials');
    }
}
