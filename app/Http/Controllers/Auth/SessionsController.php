<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{

    public function create()
    {
        return view('auth.sessions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'email' => 'required|email',
           'password' => 'required'
        ]);

        $email = $request->get('email');
        $password = $request->get('password');

        $user = User::where('email', $email)->first();

        if (!$user) {
            session()->flash('invalid-credentials');
            return redirect()->back();
        }

        if (!$user->email_verified) {
            session()->flash('verify-email-needed');
            return redirect()->back();
        }

        if (auth()->attempt([ 'email' => $email, 'password' => $password ], true)) {
            return redirect()->route('home.index'); // TODO: redirect to account
        } else {
            session()->flash('invalid-credentials');
            return redirect()->back();
        }
    }

    public function destroy()
    {
        auth()->logout();
        return redirect()->route('home.index');
    }

}
