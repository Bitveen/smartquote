<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\User;

class PasswordResetController extends Controller
{
    public function create()
    {
        return view('auth.password-reset.index');
    }

    public function store(Request $request)
    {
        $validatedRequest = $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = User::where('email', $validatedRequest['email'])->first();

        if (!$user) {
            session()->flash('user-not-found');
            return redirect()->back();
        }

        PasswordReset::send($user);

        session()->flash('email-sent');

        return redirect()->back();
    }

    public function edit($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset) {
            session()->flash('invalid-reset-token');
        }

        return view('auth.password-reset.new-password', compact('token'));
    }

    public function update(Request $request, $token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset) {
            session()->flash('invalid-reset-token');
            return redirect()->back();
        }

        $this->validate($request, [
           'password' => 'required|confirmed|min:2'
        ]);

        User::where('email', $passwordReset->email)->update([
            'password' => bcrypt($request->get('password'))
        ]);

        PasswordReset::where('token', $token)->delete();

        session()->flash('password-reset-success');

        return redirect()->route('sessions.create');
    }

}
