<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\EmailVerification;
use App\Shipping;

class RegistrationController extends Controller
{

    public function create()
    {
        if (session('registration-success')) {
            return view('auth.registration.success');
        }
        return view('auth.registration.create');
    }

    public function store(Request $request)
    {

        $validatedRequest = $this->validate($request, [
            'email' => 'required|unique:users|email|confirmed',
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'password' => 'required|confirmed|min:2|max:255',
            'is_terms_agree' => 'required|accepted',
            'company' => 'required',
            'phone_number' => 'required',
            'zip_code' => 'present',
            'know_from' => 'present',
            'is_subscribed' => 'present'
        ]);

        $shipping = Shipping::create([]);

        $user = User::create([
            'email' => $validatedRequest['email'],
            'first_name' => $validatedRequest['first_name'],
            'last_name' => $validatedRequest['last_name'],
            'password' => bcrypt($validatedRequest['password']),
            'company' => $validatedRequest['company'],
            'phone_number' => $validatedRequest['phone_number'],
            'zip_code' => $validatedRequest['zip_code'],
            'know_from' => $validatedRequest['know_from'],
            'is_terms_agree' => $validatedRequest['is_terms_agree'] === 'on',
            'is_subscribed' => $validatedRequest['is_subscribed'] === 'on',
            'shipping_id' => $shipping->id
        ]);

        EmailVerification::send($user);

        session()->flash('registration-success');

        return redirect()->back();
    }

}
