<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmailVerification;
use App\User;

class EmailVerificationController extends Controller
{
    public function create()
    {
        return view('auth.email-verification.index');
    }

    public function store(Request $request)
    {
        $validatedRequest = $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = User::where('email', $validatedRequest['email'])->first();

        if (!$user) {
            session()->flash('email-not-found');
            return redirect()->back();
        }

        if ($user->email_verified) {
            session()->flash('email-already-verified');
            return redirect()->back();
        }

        EmailVerification::send($user);

        session()->flash('email-sent');

        return redirect()->back();
    }

    public function verify($token)
    {
        if (!EmailVerification::verify($token)) {
           session()->flash('verification-failed');
        }

        return view('auth.email-verification.result');
    }

}
