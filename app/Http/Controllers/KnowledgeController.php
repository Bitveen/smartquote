<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KnowledgeController extends Controller
{

    public function index()
    {
        return view('knowledge.index');
    }

    public function materials()
    {
        return view('knowledge.materials.index');
    }

    public function technologies()
    {
        return view('knowledge.technologies.index');
    }

}
