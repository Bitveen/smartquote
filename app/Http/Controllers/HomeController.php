<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index()
    {
        $dateAfterFive = Carbon::now()->addDays(5);
        $dateAfterSix = Carbon::now()->addDays(6);
        return view('home.index', compact('dateAfterFive', 'dateAfterSix'));
    }

}
