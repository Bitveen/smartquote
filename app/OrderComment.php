<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $fillable = [
        'customer_message',
        'message',
        'order_id'
    ];
}
