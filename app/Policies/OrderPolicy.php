<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function process(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function delete(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function view(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function uploadFiles(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function deleteFiles(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }

    public function checkPartsStatus(User $user, Order $order)
    {
        return $user->id === $order->user_id;
    }
}
