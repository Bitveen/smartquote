<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Material;

class Process extends Model
{
    protected $fillable = [
        'name',
        'title',
        'description'
    ];

    public function materials()
    {
        return $this->hasMany(Material::class);
    }
}
