<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Material;
use App\Process;
use App\Order;

class Part extends Model
{
    protected $fillable = [
        'name',
        'size',
        'order_id',
        'material_id',
        'process_id',
        'finish'
    ];

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function process()
    {
        return $this->belongsTo(Process::class, 'process_id');
    }

    public function isManual()
    {
        if ($this->multi_material) {
            return true;
        }

        $maxSide = max($this->x, $this->y, $this->z);

        if ($this->units == 'mm' && $maxSide > 203) {
            return true;
        }

        if ($this->units == 'in' && $maxSide > 8) {
            return true;
        }

        $material = Material::where(['id' => $this->material_id])->first();



        if ($material->manual_quotation_required) {
            return true;
        }

        return false;
    }

    public static function getDefaultMaterial()
    {
        return Material::where([ 'name' => 'First Available Vero' ])->first();
    }

    public static function getDefaultProcess()
    {
        return Process::where([ 'name' => 'polyjet' ])->first();
    }

}
