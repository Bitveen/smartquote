<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ShippingState;
use App\User;

class Shipping extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'additional_address',
        'city',
        'zip_code'
    ];

    public function state()
    {
        return $this->belongsTo(ShippingState::class, 'state_id');
    }
}
