<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Part;
use App\OrderComment;
use App\Fee;

class Order extends Model
{
    protected $fillable = [
        'user_id'
    ];

    public function parts()
    {
        return $this->hasMany(Part::class);
    }

    public function comments()
    {
        return $this->hasMany(OrderComment::class);
    }

    public function fees()
    {
        return $this->hasMany(Fee::class);
    }
}
