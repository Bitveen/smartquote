<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Part;
use App\Utils\Path;
use App\Events\PartProcessed;
use Requests as HttpRequest;

class ProcessPart implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $part;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Part $part)
    {
        $this->part = $part;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filePath = Path::buildFullFilePath($this->part->order_id, $this->part->id);
        $fullFilePath = storage_path('app/'.$filePath.'/'.$this->part->name);
        $zipPath = storage_path('app/'.$filePath.'/streamics_result.zip');

        $cfile = new \CURLFile($fullFilePath,'application/octet-stream', $this->part->name);
        $ch = curl_init();
        curl_setopt($ch, \CURLOPT_URL, 'http://198.20.94.219:81/process_file.php');
        curl_setopt($ch, \CURLOPT_POST,1);
        curl_setopt($ch, \CURLOPT_SAFE_UPLOAD, 1);
        curl_setopt($ch, \CURLOPT_POSTFIELDS, [ 'uploadfile' => $cfile ]);
        curl_setopt($ch, \CURLOPT_FILE, fopen(storage_path('app/'.$filePath.'/streamics_result.zip'), 'w'));
        curl_exec($ch);
        curl_close($ch);

        $zip = new \ZipArchive();

        if ($zip->open($zipPath)) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $entry = $zip->getNameIndex($i);

                $fp = $zip->getStream($entry);

                if ($fp) {
                    $fullFileName = basename($entry);

                    $destinationPath = storage_path('app/'.$filePath);

                    file_put_contents($destinationPath.'/'.$fullFileName, $fp);
                }
            }
            $zip->close();
            Storage::delete($filePath.'/streamics_result.zip');

            $contents = json_decode(Storage::get($filePath.'/data.json'));
            $data = json_decode($contents->json);

            $this->part->x = floatval($data->ExtractParameters->Part->Delta->X);
            $this->part->y = floatval($data->ExtractParameters->Part->Delta->Y);
            $this->part->z = floatval($data->ExtractParameters->Part->Delta->Z);
            $this->part->volume = floatval($data->ExtractParameters->Part->Volume);
            $this->part->surface_area = floatval($data->ExtractParameters->Part->SurfaceArea);
            $this->part->support_volume = floatval($data->ExtractParameters->Support->Volume);
            $this->part->projected_area = floatval($data->ExtractParameters->Support->ProjectedArea);
            $this->part->height_estimated = floatval($data->ExtractParameters->Support->HeightEstimated);
            $this->part->streamics_status = 'processed';
            $this->part->save();

            event(new PartProcessed($this->part));
        }
    }
}
