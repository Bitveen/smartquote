<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Part;
use Requests as HttpRequest;
use App\Utils\Quantity;

class HandleMem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $part;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Part $part)
    {
        $this->part = $part;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $buildTime = 1;
        $technology = $this->part->process->title;

        $modelVolume = 0;
        $supportVolume = 0;
        $supportZ = 0;
        $surfaceArea = 0;

        switch ($this->part->units) {
            case 'in':
                $modelVolume = $this->part->volume;
                $supportVolume = $this->part->support_volume;
                $supportZ = Quantity::length($this->part->height_estimated, 'in')->to('mm');
                $surfaceArea = $this->part->surface_area;
                break;
            case 'mm':
                $modelVolume = Quantity::volume($this->part->volume, 'mm')->to('in');
                $supportVolume = Quantity::volume($this->part->support_volume, 'mm')->to('in');
                $supportZ = $this->part->height_estimated;
                $surfaceArea = Quantity::area($this->part->surface_area, 'mm')->to('in');
                break;
        }

        $payload = [
            'env' => 'mem-staging',
            'data' => [
                'buildTime' => $buildTime,
                'modelVolume' => $modelVolume,
                'supportVolume' => $supportVolume,
                'supportZ' => $supportZ,
                'surfaceArea' => $surfaceArea,
                'technology' => $technology
            ],
            'result' => ['buildTime', 'modelVolume', 'supportVolume']
        ];

        $response = HttpRequest::post(env('MEM_URI'), [], $payload);

        $result = json_decode($response->body)->result;

        $convertedModelVolume = Quantity::volume($result->modelVolume, 'in')->to($this->part->units);
        $convertedSupportVolume = Quantity::volume($result->supportVolume, 'in')->to($this->part->units);

        $this->part->build_time = round($result->buildTime, 2);
        $this->part->volume = round($convertedModelVolume, 2);
        $this->part->support_volume = round($convertedSupportVolume, 2);
        $this->part->mem_status = 'processed';
        $this->part->save();
    }
}
