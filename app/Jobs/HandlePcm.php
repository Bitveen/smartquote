<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Part;
use Requests as HttpRequest;
use App\Utils\Quantity;
use App\Utils\Materials;

class HandlePcm implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $part;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Part $part)
    {
        $this->part = $part;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $buildTime = 1;
        if ($this->part->process->name === 'fdm') {
            $buildTime = $this->part->build_time;
        }
        $cleaningTime = 0.25;
        $discount = 0;
        $material = Materials::translate($this->part->process->name, $this->part->material->name);
        $quantity = $this->part->quantity;
        $finish = $this->part->finish;
        if ($finish === 'manual') {
            $finish = 'Standard';
        }
        $modelVolume = 0;
        $supportVolume = 0;
        $surfaceArea = 0;
        $partdimensionX = 0;
        $partdimensionY = 0;
        $partdimensionZ = 0;

        switch ($this->part->units) {
            case 'in':
                $modelVolume = $this->part->volume;
                $supportVolume = $this->part->support_volume;
                $surfaceArea = Quantity::area($this->part->surface_area, 'in')->to('mm');
                $partdimensionX = Quantity::length($this->part->x, 'in')->to('mm');
                $partdimensionY = Quantity::length($this->part->y, 'in')->to('mm');
                $partdimensionZ = Quantity::length($this->part->z, 'in')->to('mm');
                break;
            case 'mm':
                $modelVolume = Quantity::volume($this->part->volume, 'mm')->to('in');
                $supportVolume = Quantity::volume($this->part->support_volume, 'mm')->to('in');
                $surfaceArea = $this->part->surface_area;
                $partdimensionX = $this->part->x;
                $partdimensionY = $this->part->y;
                $partdimensionZ = $this->part->z;
                break;
        }

        $payload = [
            'env' => 'pcm-smartquote',
            'data' => [
                'buildTime' => $buildTime,
                'cleaningTime' => $cleaningTime,
                'discount' => $discount,
                'material' => $material,
                'quantity' => $quantity,
                'modelVolume' => $modelVolume,
                'partdimensionX' => $partdimensionX,
                'partdimensionY' => $partdimensionY,
                'partdimensionZ' => $partdimensionZ,
                'supportVolume' => $supportVolume,
                'surfaceArea' => $surfaceArea
            ],
            'result' => ['totalPrice']
        ];

        $response = HttpRequest::post(env('PCM_URI'), [], $payload);

        $result = json_decode($response->body)->result;

        $this->part->price = $result->totalPrice;
        $this->part->pcm_status = 'processed';
        $this->part->save();
    }
}
