<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MaterialGroup;

class Material extends Model
{
    /**
     * Get material multipliers as array
     * [ material_id => multiplier ]
     *
     * @return array
     */
    public static function getMultipliers()
    {
        $materials = self::all();

        $multipliers = [];

        foreach ($materials as $material) {
            $multipliers[$material->id] = $material->multiplier;
        }

        return $multipliers;
    }

    public function group()
    {
        return $this->belongsTo(MaterialGroup::class, 'group_id');
    }
}
